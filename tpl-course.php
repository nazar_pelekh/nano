<?php get_header(); /*Template Name: Course*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section class="flex_text_video">
    <div class="container flex__mob">
        <?php
        $video = get_field('video_course');
        if ($video['type'] == 'library') { ?>
            <div class="half_video def_video_popup" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                <a href="#video" data-fancybox class="play_icon">
                    <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                </a>
                <video id="video" src="<?php echo $video['file']; ?>" controls muted style="display: none"></video>
            </div>
        <?php } elseif ($video['type'] == 'link') { ?>
            <div class="half_video def_video_popup" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                    <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                </a>
            </div>
        <?php } ?>
        <div class="half_text">
            <?php the_content(); ?>
        </div>
    </div>
</section>

<?php if($goal = get_field('levels')){ ?>
    <section class="goals">
        <div class="container is_smaller">
            <div class="goals_text">
                <?php the_field('text_course') ?>
            </div>
            <div class="goals_items flex_center__rwd">
                <?php foreach ($goal as $g) { ?>
                    <div class="goal_item flip-card course__item">
                        <div class="flip-card-inner">
                            <div class="goal__side flip-card-front">
                                <div class="goal_item__img flex_center">
                                    <img src="<?php echo $g['image'] ?>" alt="">
                                </div>
                                <div class="goal_item__text">
                                    <?php echo $g['text'] ?>
                                </div>
                            </div>
                            <!--<div class="goal__side flip-card-back">
                                <h4><?php /*echo $g['title'] */?></h4>
                                <?php /*echo $g['text_hover']; */?>
                            </div>-->
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_template_part('tpl-parts/how-we-teach') ?>

<?php if($steps_content = get_field('text_skype')){ ?>
    <div class="container is_smaller">
        <div class="steps_content flex_start__rwd">
            <div class="expand_text">
                <?php the_field('text_skype'); ?>
                <!--                        <button class="not_active">--><?php //echo __('[:ua]Розгорнути[:ru]Развернуть') ?><!--</button>-->
                <!--                        <button class="is_active hide">--><?php //echo __('[:ua]Згорнути[:ru]Свернуть') ?><!--</button>-->
            </div>
            <?php echo get_field('picture_skype') ? '<img src="' . get_field('picture_skype')['url'] . '" class="rwd_hide"/ alt="'. get_field('picture_skype')['alt'].'">' : 'empty' ?>
        </div>
        <?php the_field('text_full__skype') ?>
    </div>
<?php } ?>

<?php get_template_part('tpl-parts/trial-lesson-row') ?>

<?php get_template_part('tpl-parts/reviews-slider') ?>

<?php get_template_part('tpl-parts/facebook-widget') ?>

<?php get_footer(); ?>
