<?php get_header(); ?>

<?php if($bg = get_field('background_top')) { ?>
    <section class="home_top" style="background-image: url(<?php echo $bg ?>);">
        <div class="container flex_center__rwd">
            <div class="item">
                <?php the_field('text_top') ?>
                <?php
                $video = get_field('video_top');
                if($video){ ?>
                    <div class="play_icon rwd_show">
                        <a href="#entryVideo_" data-fancybox="entryVideo">
                            <img src="<?php echo theme('images/play.svg') ?>" alt="">
                        </a>
                        <video id="entryVideo" src="<?php echo $video['video']['url']; ?>" controls style="display: none;"></video>
                        <p><?php echo $video['text'];  ?></p>
                    </div>
                <?php } ?>
            </div>
            <?php
            $video = get_field('video_top');
            if($video){ ?>
                <div class="item play_icon rwd_hide">
                    <a href="#entry_video" data-fancybox="entry_video">
                        <img src="<?php echo theme('images/play.svg') ?>" alt="">
                    </a>
                    <video id="entry_video" src="<?php echo $video['video']['url']; ?>" controls style="display: none;"></video>
                    <p><?php echo $video['text'];  ?></p>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>

<?php if($goal = get_field('goals')){ ?>
    <section class="goals">
        <div class="container is_smaller">
            <div class="goals_text">
                <?php the_field('text_goal') ?>
            </div>
            <div class="goals_items flex__rwd">
                <?php foreach ($goal as $g) { ?>
                    <div class="goal_item flip-card">
                        <div class="flip-card-inner">
                            <div class="goal__side flip-card-front">
                                <div class="goal_item__img flex_center">
                                    <img src="<?php echo $g['image'] ?>" alt="">
                                </div>
                                <div class="goal_item__text flex_center">
                                    <?php echo $g['text'] ?>
                                </div>
                            </div>
                            <div class="goal__side flip-card-back">
                                <h4><?php echo $g['text'] ?></h4>
                                <?php echo $g['text_hover']; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>



<?php if($trial = get_field('form_trial')) { ?>
    <section class="trial_lesson">
        <div class="container is_smaller flex__rwd">
            <?php if($quote = get_field('quote_trial')) { ?>
                <div class="item qoute_bg">
                    <?php
                    echo '<div class="quote_text">' . $quote['text'] . '</div>';
                    echo '<div class="quote_author">– ' . $quote['author'] . '</div>';
                    ?>
                </div>
            <?php } ?>
            <div class="item form_item">
                <?php echo $trial ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_template_part('tpl-parts/how-we-teach') ?>

<?php if($steps_content = get_field('text_skype')){ ?>
    <div class="container is_smaller">
        <div class="steps_content flex_start__rwd">
            <div class="expand_text">
                <?php the_field('text_skype'); ?>
                <!--                        <button class="not_active">--><?php //echo __('[:ua]Розгорнути[:ru]Развернуть') ?><!--</button>-->
                <!--                        <button class="is_active hide">--><?php //echo __('[:ua]Згорнути[:ru]Свернуть') ?><!--</button>-->
            </div>
            <?php echo get_field('picture_skype') ? '<img src="' . get_field('picture_skype')['url'] . '" class="rwd_hide"/ alt="'. get_field('picture_skype')['alt'].'">' : 'empty' ?>
        </div>
    </div>
<?php } ?>

<?php
$text_tests = get_field('text_tests');
$button_tests = get_field('button_tests');
if($text_tests || $button_tests) { ?>
    <section class="tests">
        <div class="container is_smaller flex__rwd">
            <?php echo $text_tests ? '<div class="item">'.$text_tests.'</div>' : "" ; ?>
            <?php echo $button_tests ? '<div class="item">'.$button_tests.'</div>' : "" ; ?>
        </div>
    </section>
<?php } ?>

<?php if($text_text = get_field('text_text')){ ?>
    <section class="home_text">
        <div class="container">
            <?php the_field('text_title') ?>
            <div class="home_text__inner">
                <?php echo $text_text ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_template_part('tpl-parts/reviews-slider') ?>

<?php get_template_part('tpl-parts/facebook-widget') ?>

<?php get_footer(); ?>
