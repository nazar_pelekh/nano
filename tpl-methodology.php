<?php get_header(); /*Template Name: Methodology*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) : ?>
        <section class="flex_text_video">
            <div class="container flex__mob">
                <?php
                $video = get_field('video');
                if ($video['type'] == 'library') { ?>
                    <div class="half_video def_video_popup"
                         style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                        <a href="#video" data-fancybox class="play_icon">
                            <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                        </a>
                        <video id="video" src="<?php echo $video['file']; ?>" controls muted
                               style="display: none"></video>
                    </div>
                <?php } elseif ($video['type'] == 'link') { ?>
                    <div class="half_video def_video_popup"
                         style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                        <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                            <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                        </a>
                    </div>
                <?php } ?>
                <div class="half_text">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
    <?php endif; endwhile; endif; ?>

<div class="methodology_sbs">
    <div class="container is_smaller">
        <?php if (get_field('text_sbs')) {
            the_field('text_sbs');
        } ?>
        <?php if ($steps = get_field('steps')) { ?>
            <div class="m_steps flex__mob">
                <?php foreach ($steps as $step) { ?>
                    <div class="m_step">
                        <figure><img src="<?php echo $step['image']['url']; ?>"
                                     alt="<?php echo $step['image']['alt']; ?>"></figure>
                        <?php echo $step['text']; ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if (get_field('bottom_text')) { echo '<div class="m_steps_bottom">'.
            get_field('bottom_text') .'</div>';
        } ?>
    </div>
</div>

<?php if($more = get_field('more_information')){ ?>
    <div class="more__information container is_smaller">
        <?php echo $more; ?>
    </div>
    <br>
<?php } ?>

<?php if ($trial = get_field('form_trial', 2)) { ?>
    <section class="trial_lesson">
        <div class="container is_smaller flex__rwd">
            <div class="item form_item">
                <?php echo $trial ?>
            </div>
            <?php if ($quote = get_field('quote_trial', 2)) { ?>
                <div class="item qoute_bg">
                    <?php
                    echo '<div class="quote_text">' . $quote['text'] . '</div>';
                    echo '<div class="quote_author">– ' . $quote['author'] . '</div>';
                    ?>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>
<?php get_footer(); ?>
