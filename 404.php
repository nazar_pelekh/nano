<?php get_header(); ?>

<section class="page404 content container">
    <h1><?php echo __('[:ua]Помилка 404[:ru]Ошибка 404') ?></h1>
    <p><?php echo __('[:ua]Вибачте, цієї сторінки не існує або її видалили[:ru]Простите, этой страницы не существует или её удалили') ?></p>
    <a href="<?php echo site_url(); ?>" class="button"><?php echo __('[:ua]Повернутись на головну[:ru]Вернуться на главную') ?></a>
</section>

<?php get_footer(); ?>
