<?php get_header(); /*Template Name: Why We*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) : ?>
        <section class="flex_text_video">
            <div class="container flex__mob">
                <?php
                $video = get_field('video');
                if ($video['type'] == 'library') { ?>
                    <div class="half_video def_video_popup" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                        <a href="#video" data-fancybox class="play_icon">
                            <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                        </a>
                        <video id="video" src="<?php echo $video['file']; ?>" controls muted style="display: none"></video>
                    </div>
                <?php } elseif ($video['type'] == 'link') { ?>
                    <div class="half_video def_video_popup" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                        <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                            <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                        </a>
                    </div>
                <?php } ?>
                <div class="half_text">
                    <?php the_content(); ?>
                </div>
            </div>
        </section>
    <?php endif; endwhile; endif; ?>
<?php if (get_field('more_information')) { ?>
    <div class="why_we__more_info">
        <div class="container">
            <?php the_field('more_information'); ?>
        </div>
    </div>
<?php } ?>
<?php if($steps_content = get_field('text_skype')){ ?>
    <br>
    <div class="container is_smaller">
        <div class="steps_content flex_start__rwd">
            <div class="expand_text">
                <?php the_field('text_skype'); ?>
                <!--                        <button class="not_active">--><?php //echo __('[:ua]Розгорнути[:ru]Развернуть') ?><!--</button>-->
                <!--                        <button class="is_active hide">--><?php //echo __('[:ua]Згорнути[:ru]Свернуть') ?><!--</button>-->
            </div>
            <?php echo get_field('picture_skype') ? '<img src="' . get_field('picture_skype')['url'] . '" class="rwd_hide"/ alt="'. get_field('picture_skype')['alt'].'">' : 'empty' ?>
        </div>
        <?php the_field('text_full_skype') ?>
    </div>
<?php } ?>
<?php if (get_field('trial_lesson', 'option')) { ?>
    <div id="t_lesson" class="trial_lesson_row ww_trial_lesson">
        <div class="container">
            <?php the_field('trial_lesson', 'option'); ?>
        </div>
    </div>
<?php } ?>
<?php get_template_part('tpl-parts/reviews-slider') ?>
<?php get_footer(); ?>
