<?php get_header(); /*Template Name: FAQ*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section class="faq_content">
    <div class="container ">
        <h1><?php the_title(); ?></h1>
        <div class="flex_start__rwd">
            <article>
                <?php if ($faqs = get_field('faq')) { ?>
                    <?php foreach ($faqs as $faq) { ?>
                        <div class="faq_group">
                            <div class="faq_group_title flex">
                                <h3><?php echo $faq['title']; ?></h3>
                                <a href="#" class="open_all_questions">
                                    <span class="show_all_q"><?php echo __('[:ua]Розгорнути всі[:ru]Развернуть все') ?></span>
                                    <span class="hide_all_q"><?php echo __('[:ua]Згорнути всі[:ru]Свернуть все') ?></span>
                                </a>
                            </div>
                            <?php foreach ($faq['questions_group'] as $question) { ?>
                                <div class="item">
                                    <div class="faq_question flex">
                                        <?php echo $question['question']; ?>
                                        <span></span>
                                    </div>
                                    <div class="faq_answer">
                                        <?php echo $question['answer']; ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                <?php } ?>
            </article>
            <aside>
                <?php
                if ( 'ua' === $GLOBALS['q_config']['language']) {
                    echo do_shortcode('[contact-form-7 id="569" title="FAQ form"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="974" title="FAQ form (ru)"]');
                }
                ?>
            </aside>
        </div>
    </div>
</section>
<?php get_template_part('tpl-parts/trial-lesson-row') ?>
<?php get_footer(); ?>
