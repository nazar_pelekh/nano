<?php get_header('nanotest'); ?>

    <div id="exit_test" class="popup" style="display: none;">
        <h4><?php echo __('[:ua]Вийти з тесту?[:ru]Выйти из теста?') ?></h4>
        <p><?php echo __('[:ua]Весь прогрес буде загублено а при наступній спробі ви почнете тест з самого початку.[:ru]Весь прогресс будет потерян а при следующей попытке вы начнете тест с самого начала.') ?></p>
        <button class="button active close_popup"><?php echo __('[:ua]Продовжити тестування[:ru]Продолжить тестирование') ?></button>
        <div class="exit_link_wrapper">
            <a href="<?php echo site_url(); ?>/tests/" class="exit link"><?php echo __('[:ua]Вийти та втратити прогрес[:ru]Выйти и потерять прогресс') ?></a>
        </div>
    </div>

    <div id="thanks_nano" class="popup" style="display: none;">
        <h4><?php echo __('[:ua]Дякуємо![:ru]Спасибо!') ?></h4>
        <p>
            <?php echo __('[:ua]Ваші результати успішно відправлено на вказану вами електронну скриньку[:ru]Ваши результаты успешно отправлена на указанный вами электронный адрес') ?>
            <strong id="nano_user_mail"></strong>
        </p>
        <img src="<?php echo theme() ?>/images/check_success_nano.svg" alt="">
        <a href="<?php echo site_url(); ?>/tests/" class="button transparent"><?php echo __('[:ua]На головну[:ru]На главную') ?></a>
    </div>

    <div class="container progress_container">
        <div id="regulartest_progress" class="flex">
            <span class="nanotest_progress_text active">Start</span>
            <div id="regulartest_progress-lines"></div>
            <span class="nanotest_progress_text">Finish</span>
        </div>
    </div>

    <div id="regular-test__category-eng" class="hide"><?php $terms = wp_get_post_terms( get_the_ID(), 'type_test');
        foreach ($terms as $t) {
            echo $t->slug, '|';
        }
        ?></div>
    <div id="regular-test__category-trans" class="hide"><?php $terms = wp_get_post_terms( get_the_ID(), 'type_test');
        foreach ($terms as $t) {
            echo $t->name, '|';
        }
        ?></div>

    <section class="nanotest_content" >
        <div class="regular-test__slider swiper-container">
            <div class="swiper-wrapper">
                <?php while (have_posts()) : the_post(); ?>
                    <?php if( have_rows('test_of_type') ) { ?>
                        <?php $slide = 1; while( have_rows('test_of_type') ): the_row(); ?>

                            <?php
                            if( get_row_layout() == 'step_1_translation' ) { ?>

                                <div class="swiper-slide vocabulary_type vocabulary_type_1" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test nanotest_type1">
                                        <div class="container is_tests">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_col nanotest_question">
                                                <div class="nanotest_question_img swiper-lazy" style="background-image: url(<?php the_sub_field('picture') ?>);"></div>
                                            </div>
                                            <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_2_vocabulary' ) { ?>

                                <div class="swiper-slide vocabulary_type regular-test__vocabulary-2" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_3_vocabulary' ) { ?>

                                <div class="swiper-slide vocabulary_type regular-test__vocabulary-3 regular-test__create-row-with-variants" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test">
                                        <div class="container is_tests">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question">
                                                <div class="regular-test__sentence">

                                                </div>
                                            </div>
                                            <div class="nanotest_answers " data-slide="<?php echo $slide++; ?>">
                                                <div class="nanotest_question_row">
                                                    <?php foreach (get_sub_field('answers') as $answer) { ?>

                                                        <div class="regular-test__vocabulary-row" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>">
                                                            <div class="regular-test__vocabulary-choice regular-test__vocabulary-row-correct hide"><?php echo $answer['text']['correct_variants'] ?></div>
                                                            <div class="regular-test__vocabulary-choice regular-test__vocabulary-buttons regular-test__vocabulary-row1" data-count="1"><button><?php echo $answer['text']['row_1'] ?></button></div>
                                                            <div class="regular-test__vocabulary-choice regular-test__vocabulary-buttons regular-test__vocabulary-row2 hide" data-count="2">
                                                                <?php
                                                                $arr2 = explode('/', $answer['text']['row_2']);
                                                                foreach ($arr2 as $ar) {
                                                                    echo '<button>'.$ar.'</button>';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="regular-test__vocabulary-choice regular-test__vocabulary-buttons regular-test__vocabulary-row3 hide" data-count="3">
                                                                <?php
                                                                $arr3 = explode('/', $answer['text']['row_3']);
                                                                foreach ($arr3 as $ar) {
                                                                    echo '<button>'.$ar.'</button>';
                                                                }
                                                                ?>
                                                            </div>

                                                        </div>

                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_4_vocabulary' ) { ?>

                                <div class="swiper-slide vocabulary_type vocabulary_type_2" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <div class="nanotest_question_col" data-col="<?php echo $col++; ?>">
                                                        <div class="nanotest_question_col_picture" data-correct="<?php echo $answer['correct_answer'] ?>" style="background-image: url(<?php echo $answer['picture'] ?>);">
                                                            <img src="<?php echo $answer['picture'] ?>" class="swiper-lazy" alt="">
                                                        </div>
                                                        <select class="nanotest_question_select" name="nanotest_question_select">
                                                            <option value="false">Select Word</option>
                                                            <?php foreach (get_sub_field('answers') as $an) { ?>
                                                                <option value="<?php echo $an['correct_answer'] ?>"><?php echo $an['correct_answer'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_6_grammar_new' ) { ?>

                                <div class="swiper-slide vocabulary_type grammar_type_6 regular-test__grammar-6" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_block nanotest_answers">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_4_grammar' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_4 regular-test__vocabulary-5" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test nanotest_type4">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_5_grammar' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_5" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test nanotest_type4">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_col nanotest_question">
                                                <img src="<?php the_sub_field('picture') ?>" class="swiper-lazy" alt="">
                                                <div class="nanotest_question_text">
                                                    <?php the_sub_field('text'); ?>
                                                </div>
                                            </div>
                                            <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="nanotest_exit">
                                            <div class="container flex">
                                                <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                                <div class="question_next flex">
                                                    <div class="total_count">
                                                        <span class="current_index"></span>
                                                        /
                                                        <span class="total_index"></span>
                                                    </div>
                                                    <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'step_6_grammar' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_6" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test nanotest_type6">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_col nanotest_question">
                                                <div class="nanotest_question_img" style="background-image: url(<?php the_sub_field('picture') ?>);"></div>
                                            </div>
                                            <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="nanotest_exit">
                                            <div class="container flex">
                                                <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                                <div class="question_next flex">
                                                    <div class="total_count">
                                                        <span class="current_index"></span>
                                                        /
                                                        <span class="total_index"></span>
                                                    </div>
                                                    <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_7_grammar' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_4 regular-test__vocabulary-5 regular-test__vocabulary-7" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test nanotest_type4">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'type_8_grammar' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_input " data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers hide" data-slide="<?php echo $slide++; ?>" data-point="<?php the_sub_field('points_for_the_correct_answer') ?>">
                                                <?php echo get_sub_field('answer') ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'create_a_sentence_with_the_words_one_row' ) { ?>

                                <div class="swiper-slide vocabulary_type regular-test__create-row" data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test">
                                        <div class="container is_tests">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question">
                                                <div class="regular-test__sentence">

                                                </div>
                                            </div>
                                            <div class="nanotest_answers " data-slide="<?php echo $slide++; ?>">
                                                <div class="nanotest_question_row">
                                                    <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                        <button data-order="<?php echo $answer['order']  ?>" data-point="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }
                            if( get_row_layout() == 'find_the_mistake_in_the_sentence' ) { ?>

                                <div class="swiper-slide grammar_type grammar_type_mistake " data-nanotest-slide="<?php echo $slide; ?>">
                                    <div class="nanotest_test">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers hide" data-slide="<?php echo $slide++; ?>" data-point="<?php the_sub_field('points_for_the_correct_answer') ?>">
                                                <?php echo get_sub_field('answer') ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>

                            <?php }

                            if( get_row_layout() == 'just_a_text' ) { ?>
                                <?php $slide = 0; ?>
                                <div class="swiper-slide regular-test__text">
                                    <div class="nanotest_test">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers hide" data-slide="<?php echo $slide++; ?>" data-point="<?php the_sub_field('points_for_the_correct_answer') ?>">
                                                <?php echo get_sub_field('answer') ?>
                                            </div>
                                        </div>
                                        <div class="nanotest_exit">
                                            <div class="container flex">
                                                <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                                <div class="question_next flex">
                                                    <div class="total_count">
                                                        <?php echo __('[:ua]Уважно прочитайте текст перед тим як почати тест[:ru]Внимательно прочитайте текст перед тем как начать тест') ?>
                                                    </div>
                                                    <button class="button next_slide"><?php echo __('[:ua]Почати тест[:ru]Hачать тест') ?><i class="fas fa-chevron-right"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Subscribe popup -->
                                <div id="regular-test__text" class="popup" style="display: none;">
                                    <div class="regular-test__text-inner">
                                        <?php the_sub_field('text'); ?>
                                    </div>
                                </div>
                            <?php }
                            if( get_row_layout() == 'select_an_option_from_text' ) { ?>

                                <div class="swiper-slide vocabulary_type grammar_type_6 regular-test__grammar-6 regular-test__text-block" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text flex">
                                                <div class="nanotest_question_text__inner">
                                                    <?php the_sub_field('text'); ?>
                                                </div>
                                                <button class="regular-test__show-text" data-fancybox data-src="#regular-test__text">
                                                    <i></i>
                                                    <b><?php echo __('[:ua]Показати текст[:ru]Показать текст') ?></b>
                                                </button>
                                            </div>
                                            <div class="nanotest_question_block nanotest_answers">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>
                            <?php }
                            if( get_row_layout() == 'select_an_option_from_text_row' ) { ?>

                                <div class="swiper-slide vocabulary_type grammar_type_6 regular-test__grammar-6 regular-test__text-row-boolean" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text flex">
                                                <div class="nanotest_question_text__inner">
                                                    <?php the_sub_field('text'); ?>
                                                </div>
                                                <button class="regular-test__show-text" data-fancybox data-src="#regular-test__text">
                                                    <i></i>
                                                    <b><?php echo __('[:ua]Показати текст[:ru]Показать текст') ?></b>
                                                </button>
                                            </div>
                                            <div class="nanotest_question_row nanotest_answers">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>
                            <?php }
                            if( get_row_layout() == 'select_an_option_from_textmultiply_row' ) { ?>

                                <div class="swiper-slide vocabulary_type grammar_type_6 regular-test__grammar-6 regular-test__text-row-multiply" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test nanotest_type2">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text flex">
                                                <div class="nanotest_question_text__inner">
                                                    <?php the_sub_field('text'); ?>
                                                </div>
                                                <button class="regular-test__show-text" data-fancybox data-src="#regular-test__text">
                                                    <i></i>
                                                    <b><?php echo __('[:ua]Показати текст[:ru]Показать текст') ?></b>
                                                </button>
                                            </div>
                                            <div class="nanotest_question_block">
                                                <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                    <button class="button" <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-order="<?php echo $col++; ?>" data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>
                            <?php }
                            if( get_row_layout() == 'fill_a_gap_by_dropdown' ) { ?>

                                <div class="swiper-slide vocabulary_type grammar_type_6 regular-test__grammar-6 regular-test__text-selects" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                    <div class="nanotest_test">
                                        <div class="container is_smaller">
                                            <div class="nanotest_question_title">
                                                <?php the_sub_field('title'); ?>
                                            </div>
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                                <div class="hide regular-test_reading_select_parent">
                                                    <select class="regular-test_reading_select" name="regular-test_reading_select">
                                                        <option value="false">select</option>
                                                        <?php
                                                        $i = 0;
                                                        $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',];
                                                        foreach (get_sub_field('answers') as $answer) { ?>
                                                            <option data-select-order="<?php echo $answer['order'] ?>" data-letter="<?php echo $letters[$i++] ?>" data-point="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <?php get_template_part('tpl-parts/regular-test/test-exit') ?>
                                    </div>
                                </div>
                            <?php }
                            ?>

                        <?php endwhile; ?>
                    <?php } ?>

                <?php endwhile;?>

            </div>
        </div>
    </section>


<?php get_footer('nanotest'); ?>