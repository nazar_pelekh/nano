<?php get_header('nanotest'); /*Template Name: nanoTest*/ ?>

<div id="exit_test" class="popup" style="display: none;">
    <h4><?php echo __('[:ua]Вийти з тесту?[:ru]Выйти из теста?') ?></h4>
    <p><?php echo __('[:ua]Весь прогрес буде загублено а при наступній спробі ви почнете тест з самого початку.[:ru]Весь прогресс будет потерян а при следующей попытке вы начнете тест с самого начала.') ?></p>
    <button class="button active close_popup"><?php echo __('[:ua]Продовжити тестування[:ru]Продолжить тестирование') ?></button>
    <div class="exit_link_wrapper">
        <a href="<?php echo site_url(); ?>" class="exit link"><?php echo __('[:ua]Вийти та втратити прогрес[:ru]Выйти и потерять прогресс') ?></a>
    </div>
</div>

<div id="thanks_nano" class="popup" style="display: none;">
    <h4><?php echo __('[:ua]Дякуємо![:ru]Спасибо!') ?></h4>
    <p>
        <?php echo __('[:ua]Ваші результати успішно відправлено на вказану вами електронну скриньку[:ru]Ваши результаты успешно отправлена на указанный вами электронный адрес') ?>
        <strong id="nano_user_mail"></strong>
    </p>
    <img src="<?php echo theme() ?>/images/check_success_nano.svg" alt="">
    <a href="<?php echo site_url(); ?>" class="button transparent"><?php echo __('[:ua]На головну[:ru]На главную') ?></a>
</div>

<div class="container progress_container">
    <div id="nanotest_progress" class="flex">
        <div id="vocabulaty" class="flex">
            <span class="nanotest_progress_text active">Vocabulary</span>
        </div>
        <div id="grammar" class="flex">
            <span class="nanotest_progress_text">Grammar</span>
        </div>
        <div id="listening" class="flex">
            <span class="nanotest_progress_text">Listening</span>
        </div>
    </div>
</div>

<div id="result" class="hide" data-points="" data-goal="">
    <div id="voc_res">0</div>
    <div id="gram_res">0</div>
    <div id="lis_res">0</div>
    <p id="test_goal"></p>
    <div id="test_result"></div>
</div>

<section class="nanotest_content" >
    <div class="nanotest_slider swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <div class="nanotest_slide_main">
                    <div class="container">
                        <div class="nanotest_col">
                            <?php echo get_field('text_nano') ? get_field('text_nano') : ''; ?>
                            <?php echo get_field('picture_nano') ? '<img src="'.get_field('picture_nano').'" class="hide_mob">' : ''; ?>
                        </div>
                        <?php if($goals = get_field('goals')) { ?>
                            <div class="nanotest_col">
                                <?php the_field('title_goals') ?>
                                <div class="nanotest_goals">
                                    <?php $i = 0; foreach ($goals as $goal) { ?>
                                        <div class="nanotest_goal flex_start" data-for="radio<?php echo $i; ?>" data-goal="<?php echo $i; ?>">
                                            <div class="nanotest_goal_radio">
                                                <input id="radio<?php echo $i++; ?>" value="" type="radio" name="goal">
                                                <span></span>
                                            </div>
                                            <div class="nanotest_goal_text">
                                                <?php echo $goal['text']; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <button class="button next_slide is_disabled hide_mob"><?php echo __('[:ua]Розпочати тест[:ru]Начать тест') ?><i class="fas fa-chevron-right"></i></button>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="nanotest_exit">
                        <div class="container">
                            <div class="hide_desk flex">
                                <a href="<?php echo site_url(); ?>"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Розпочати тест[:ru]Начать тест') ?><i class="fas fa-chevron-right"></i></button>
                            </div>
                            <div class="hide_mob">
                                <a href="<?php echo site_url(); ?>"><?php echo __('[:ua]Вийти з тесту[:ru]Выйти из теста') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            query_posts(array(
                'post_type' => 'nanotest',
                'showposts' => -1,
                'order' => 'ASC',
                'orderby' => 'title'
            ) );
            ?>
            <?php $slide = 1; while (have_posts()) : the_post(); ?>
                <?php if( have_rows('test_of_type') ) { ?>
                    <?php  while( have_rows('test_of_type') ): the_row(); ?>

                        <?php
                        if( get_row_layout() == 'step_1_translation' ) { ?>

                            <div class="swiper-slide vocabulary_type vocabulary_type_1" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type1">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <div class="nanotest_question_img swiper-lazy" style="background-image: url(<?php the_sub_field('picture') ?>);"></div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                                <button class="button next_slide is_disabled hide"><?php echo __('[:ua]Завершити тест[:ru]Завершить тест') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_2_translation' ) { ?>

                            <div class="swiper-slide vocabulary_type vocabulary_type_2" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0" data-total="0" data-points="<?php the_sub_field('points_for_the_correct_answer') ?>" data-answer1="" data-answer2="" data-answer3="">
                                <div class="nanotest_test nanotest_type2">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_question_row">
                                            <?php $col = 1; foreach (get_sub_field('answers') as $answer) { ?>
                                                <div class="nanotest_question_col" data-col="<?php echo $col++; ?>">
                                                    <div class="nanotest_question_col_picture" data-correct="<?php echo $answer['correct_answer'] ?>" style="background-image: url(<?php echo $answer['picture'] ?>);">
                                                        <img src="<?php echo $answer['picture'] ?>" class="swiper-lazy" alt="">
                                                    </div>
                                                    <select class="nanotest_question_select" name="nanotest_question_select">
                                                        <option value="false">Select Word</option>
                                                        <?php foreach (get_sub_field('answers') as $an) { ?>
                                                            <option value="<?php echo $an['correct_answer'] ?>"><?php echo $an['correct_answer'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_3_translation' ) { ?>

                            <div class="swiper-slide vocabulary_type vocabulary_type_3" data-nanotest-slide="<?php echo $slide++; ?>" data-answers="0">
                                <div class="nanotest_test nanotest_type3">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_title">
                                            <?php the_sub_field('title'); ?>
                                        </div>
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                            <div class="hide">
                                                <?php foreach (get_sub_field('dropdown') as $dropdown) { ?>
                                                    <select class="nanotest_question_select_3" name="nanotest_question_select_3">
                                                        <option value="false">select</option>
                                                        <?php foreach ($dropdown['variant'] as $variant) { ?>
                                                            <option <?php echo $variant['correct'] ? 'data-select-correct="1"' : ''; ?> value="<?php echo $variant['text'] ?>"><?php echo $variant['text'] ?></option>
                                                        <?php } ?>
                                                    </select>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="nanotest_question_row">
                                            <div class="nanotest_question_row_bg"></div>
                                            <div class="nanotest_question_row_title">
                                                <?php the_sub_field('title_final') ?>
                                            </div>
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <div class="nanotest_question_item" <?php echo $answer['correct'] ? 'data-select-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer'); ?>">
                                                    <?php echo $answer['text'] ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_4_grammar' ) { ?>

                            <div class="swiper-slide grammar_type grammar_type_4" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type4">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_title">
                                            <?php the_sub_field('title'); ?>
                                        </div>
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <div class="nanotest_question_img swiper-lazy" style="background-image: url(<?php the_sub_field('picture') ?>);"></div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_5_grammar' ) { ?>

                            <div class="swiper-slide grammar_type grammar_type_5" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type4">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_title">
                                            <?php the_sub_field('title'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <img src="<?php the_sub_field('picture') ?>" class="swiper-lazy" alt="">
                                            <div class="nanotest_question_text">
                                                <?php the_sub_field('text'); ?>
                                            </div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'step_6_grammar' ) { ?>

                            <div class="swiper-slide grammar_type grammar_type_6" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type6">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <div class="nanotest_question_img" style="background-image: url(<?php the_sub_field('picture') ?>);"></div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_7_listening' ) { ?>

                            <div class="swiper-slide listening_type listening_type_7 " data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type7">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_question_row">
                                            <div class="nanotest_col nanotest_question">
                                                <div class="nanotest_question_img listening_video">
                                                    <?php $video = get_sub_field('video')?>
                                                    <video height="304" class="swiper-lazy">
                                                        <source src="<?php echo $video['url'] ?>#t=0.1" type="<?php echo $video['mime_type'] ?>">
                                                    </video>
                                                    <div class="play_video">
                                                        <img src="<?php echo theme('images/play.svg') ?>" class="img_svg" alt="play video">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                                <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                    <div class="button_wrap is_disabled">
                                                        <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>">
                                                            <?php echo $answer['word'] ?>
                                                        </button>
                                                        <span class="button_tooltip">
                                                            <?php echo __('[:ua]Ви зможете обрати відповідь лише після перегляду відео[:ru]Вы можете выбрать ответ только после просмотра видео') ?>
                                                        </span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_8_listening' ) { ?>

                            <div class="swiper-slide listening_type listening_type_8" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type4">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_title">
                                            <?php the_sub_field('title'); ?>
                                        </div>
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <div class="nanotest_question_img listening_video">
                                                <?php $video = get_sub_field('video')?>
                                                <video height="304" class="swiper-lazy">
                                                    <source src="<?php echo $video['url'] ?>#t=0.1" type="<?php echo $video['mime_type'] ?>">
                                                </video>
                                                <div class="play_video">
                                                    <img src="<?php echo theme('images/play.svg') ?>" class="img_svg" alt="play video">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <div class="button_wrap is_disabled">
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_the_correct_answer') ?>"><?php echo $answer['text'] ?></button>
                                                    <span class="button_tooltip">
                                                        <?php echo __('[:ua]Ви зможете обрати відповідь лише після перегляду відео[:ru]Вы можете выбрать ответ только после просмотра видео') ?>
                                                    </span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        elseif( get_row_layout() == 'type_9_listening' ) { ?>

                            <div class="swiper-slide listening_type listening_type_9" data-nanotest-slide="<?php echo $slide; ?>">
                                <div class="nanotest_test nanotest_type9">
                                    <div class="container is_smaller">
                                        <div class="nanotest_question_text">
                                            <?php the_sub_field('text'); ?>
                                        </div>
                                        <div class="nanotest_col nanotest_question">
                                            <div class="nanotest_question_img listening_video">
                                                <?php $video = get_sub_field('video')?>
                                                <video height="304" class="swiper-lazy">
                                                    <source src="<?php echo $video['url'] ?>#t=0.1" type="<?php echo $video['mime_type'] ?>">
                                                </video>
                                                <div class="play_video">
                                                    <img src="<?php echo theme('images/play.svg') ?>" class="img_svg" alt="play video">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="nanotest_col nanotest_answers" data-slide="<?php echo $slide++; ?>">
                                            <?php foreach (get_sub_field('answers') as $answer) { ?>
                                                <div class="button_wrap is_disabled">
                                                    <button <?php echo $answer['correct'] ? 'data-correct="1"' : ''; ?> data-val="<?php the_sub_field('points_for_correct_answer') ?>"><?php echo $answer['word'] ?></button>
                                                    <span class="button_tooltip">
                                                        <?php echo __('[:ua]Ви зможете обрати відповідь лише після перегляду відео[:ru]Вы можете выбрать ответ только после просмотра видео') ?>
                                                    </span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="nanotest_exit">
                                        <div class="container flex">
                                            <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
                                            <div class="question_next flex">
                                                <div class="total_count">
                                                    <span class="current_index"></span>
                                                    /
                                                    <span class="total_index"></span>
                                                </div>
                                                <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
                                                <button class="button next_slide is_disabled hide"><?php echo __('[:ua]Завершити тест[:ru]Завершить тест') ?><i class="fas fa-chevron-right"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        ?>

                    <?php endwhile; ?>
                <?php } ?>

            <?php endwhile;?>
            <?php wp_reset_query(); ?>

            <div class="swiper-slide">
                <div class="nanotest_slide_last">
                    <div class="container flex_start__rwd">
                        <div class="nanotest_col">
                            <?php if ( $result_text = get_field('result_text') ) { ?>
                                <div id="result_text" class="hide">
                                    <?php $r = 0; foreach($result_text as $res){ ?>
                                        <div id="resTE" data-point="<?php echo $r; ?>"><?php echo $res['title'] ?></div>
                                        <div id="resTX" data-point="<?php echo $r++; ?>"><?php echo $res['text'] ?></div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php if ( $recomm_text = get_field('recommendation_text') ) { ?>
                                <div id="recomm_text" class="hide">
                                    <?php $re = 0; foreach($recomm_text as $rec){ ?>
                                        <div id="rec" data-point="<?php echo $re++; ?>"><?php echo $rec['text'] ?></div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php the_field('text_nano_last') ?>
                        </div>
                        <div class="nanotest_col">
                            <img src="<?php the_field('picture_nano_last') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer('nanotest'); ?>