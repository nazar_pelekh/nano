<?php
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
    die ('Будь ласка, не завантажуйте цю сторінку безпосередньо. Дякую!');
if ( post_password_required() ) { ?>
    <p class="nocomments"><?php _e('Цей пост захищений паролем. Введіть пароль для перегляду коментарів'); ?></p>
    <?php
    return;
}
?>

<?php if ( have_comments() ) : ?>
    <div class="navigation">
        <div class="alignleft"><?php previous_comments_link() ?></div>
        <div class="alignright"><?php next_comments_link() ?></div>
    </div>
    <?php function comment($comment, $args, $depth){
        $GLOBALS['comment'] = $comment;
        $date = get_comment_date('%B');
        $m = '';
        if ($date = 'December') {
            $m = __('[:ua]Грудня[:ru]Декабря');
        } elseif ($date = 'January') {
            $m = __('[:ua]Січня[:ru]Января');
        } elseif($date = 'February') {
            $m = __('[:ua]Лютого[:ru]Февраля');
        } elseif($date = 'March') {
            $m = __('[:ua]Березня[:ru]Марта');
        } elseif($date = 'April') {
            $m = __('[:ua]Квітня[:ru]Апреля');
        } elseif($date = 'May') {
            $m = __('[:ua]Травня[:ru]Мая');
        } elseif($date = 'June') {
            $m = __('[:ua]Червня[:ru]Июня');
        } elseif($date = 'July') {
            $m = __('[:ua]Липня[:ru]Июля');
        } elseif($date = 'August') {
            $m = __('[:ua]Серпня[:ru]Августа');
        } elseif($date = 'September') {
            $m = __('[:ua]Вересня[:ru]Сентября');
        } elseif($date = 'October') {
            $m = __('[:ua]Жовтня[:ru]Октября');
        } elseif($date = 'November') {
            $m = __('[:ua]Листопада[:ru]Ноября');
        }

        ?>
        <ul <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>" class="comment-post">
                <div class="comment-avatar">
                    <?php echo get_avatar( $comment, $size='56' ); ?>
                </div>
                <div class="comment-inner">
                    <div class="comment-author vcard">
                        <b class="comment-author-name"><?php echo get_comment_author_link() ?></b>
                        <span class="comment-post-time"><?php echo get_comment_date('d') ?> <?php echo $m; ?> <?php echo get_comment_date('Y') ?></span>
                    </div>
                    <?php if ($comment->comment_approved == '0') : ?>
                        <em><?php echo __('[:ua]Ваш коментар очікує перевірки.[:ru]Ваш комментарий ожидает проверки.') ?></em>
                        <br />
                    <?php endif; ?>

                    <!--				<div class="comment-meta commentmetadata">-->
                    <!--					--><?php //edit_comment_link('(Редагувати)', '  ', '') ?>
                    <!--				</div>-->

                    <div class="comment_text"><?php comment_text() ?></div>

                    <div class="reply">
                        <?php comment_reply_link(array_merge( $args, array('reply_text' => '<img src="/wp-content/themes/nano/images/reply.png">'.__('[:ua]Відповісти[:ru]Ответить'),'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    </div>
                </div>
            </div>
        </ul>
    <?php }	?>
    <div class="commentlist">
        <?php wp_list_comments(array('type'=>'comment', 'callback'=>'comment')); ?>
    </div>
    <div class="navigation">
        <div class="alignleft"><?php previous_comments_link() ?></div>
        <div class="alignright"><?php next_comments_link() ?></div>
    </div>
<?php else : // this is displayed if there are no comments so far ?>
    <?php if ( comments_open() ) : ?>
        <!-- If comments are open, but there are no comments. -->
    <?php else : // comments are closed ?>
        <!-- If comments are closed. -->
        <p class="nocomments"><?php __('[:ua]Коментарі закриті.[:ru]Комментарии закрыты.'); ?></p>
    <?php endif; ?>
<?php endif; ?>

<!-- FORM!!!!!!! -->
<?php if ('open' == $post->comment_status) : ?>
    <div id="form_com_new">
        <h5 id="respond"><?php echo __('[:ua]Напишіть коментар[:ru]Напишите комментарий'); ?></h5>
        <div class="cancel-comment-reply">
            <small><?php cancel_comment_reply_link(); ?></small>
        </div>
        <?php if ( get_option('comment_registration') && !$user_ID ) : ?>
            <p><?php echo __('[:ua]Ви повинні[:ru]Вы должны') ?> <a class="rm" href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php echo __('[:ua]увійти[:ru]войти') ?>,</a> <?php echo __('[:ua]щоб залишити коментар[:ru]чтобы оставить комментарий') ?>.</p>
        <?php else : ?>
            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
                <?php if ( $user_ID ) : ?>
                    <p><?php echo __('[:ua]Ви увійшли як[:ru]Вы вошли как') ?> <a class="rm" href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a class="rm" href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php echo __('[:ua]Вийти з Вашого акаунту[:ru]Выйти из Вашего аккаунта') ?>"><?php echo __('[:ua]Вийти[:ru]Выйти') ?> &raquo;</a></p>
                <?php else : ?>
                    <div class="form_line">
                        <span class="wpcf7-form-control-wrap">
                            <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
                        </span>
                        <label><?php echo __('[:ua]Ім\'я[:ru]Имя'); ?></label>
                    </div>
                    <div class="form_line">
                        <span class="wpcf7-form-control-wrap">
                            <input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
                        </span>
                        <label>Email</label>
                    </div>
                <?php endif; ?>
                <!--<p><small><strong>XHTML:</strong> You can use these tags: <code><?php //echo allowed_tags(); ?></code></small></p>-->
                <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"></textarea>
                    </span>
                    <label><?php echo __('[:ua]Коментар[:ru]Kомментарий'); ?></label>
                </div>
                <script src="https://www.google.com/recaptcha/api.js?render=6LfDescUAAAAABPSfakdWzjiY_gXOJEbG3KkyRgS"></script>

                <div class="g-recaptcha" data-sitekey="6LfDescUAAAAABPSfakdWzjiY_gXOJEbG3KkyRgS"></div>
                <button type="submit" id="submit" class="button empty"><?php echo __('[:ua]Залишити коментар[:ru]Oставить комментарий'); ?></button>
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', $post->ID); ?>
            </form>
        <?php endif; // If registration required and not logged in ?>
    </div>
<?php endif; // if you delete this the sky will fall on your head ?>
