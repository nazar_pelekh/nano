<?php get_header(); /*Template Name: For Teachers*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); if ( get_the_content() ) : ?>
    <section class="top_for_teachers content">
        <div class="container">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>
    </section>
<?php endif; endwhile; endif; ?>
<?php if ($info_boxes = get_field('info_boxes' )) { ?>
  <section class="for_teachers_boxes">
      <div class="container is_smaller flex__mob">
          <?php foreach ($info_boxes as $box) { ?>
              <div class="box">
                  <figure><img src="<?php echo $box['image']['url']; ?>" alt="<?php echo $box['image']['alt']; ?>"></figure>
                  <?php echo $box['text']; ?>
              </div>
          <?php } ?>
      </div>
  </section>
<?php } ?>
<section class="bottom_for_teachers">
    <div class="container flex__mob">
        <?php echo(get_field('text') ? '<div class="content">'. get_field('text') .'</div>' : '') ?>
        <div class="join_teacher_form">
            <?php
            if ( 'ua' === $GLOBALS['q_config']['language']) {
                echo do_shortcode('[contact-form-7 id="432" title="For Teachers (join form)"]');
            } else {
                echo do_shortcode('[contact-form-7 id="976" title="For Teachers (join form) (ru)"]');
            }
            ?>
            <?php  ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>
