<?php get_header(); /*Template Name: Teachers*/ ?>
<section class="top_teachers_content">
    <?php if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
    } ?>
    <?php if ($posts = get_field('teachers')) { ?>
        <div class="wrap_top_teachers_content">
            <div class="container">
                <h2><?php echo __('[:ua]Вчителі[:ru]Учителя') ?></h2>
            </div>
            <div class="wrap_teacher_slider">
                <div class="container">
                    <div class="swiper-container teachers_slider">
                        <div class="swiper-wrapper">
                            <?php $i = 1;
                            foreach ($posts as $post) { ?>
                                <?php setup_postdata($post); ?>
                                <div class="swiper-slide">
                                    <div class="teacher">
                                        <a href="#teacher_info_<?php echo $i; ?>" data-fancybox class="teacher_thumb">
                                            <img src="<?php echo image_src(get_post_thumbnail_id(), 'thumbnail'); ?>"
                                                 alt="<?php the_title(); ?>">
                                        </a>
                                        <a href="#teacher_info_<?php echo $i; ?>" data-fancybox
                                           class="teacher_name"><?php the_title(); ?></a>
                                        <div id="teacher_info_<?php echo $i; ?>" class="teacher_info">
                                            <div class="flex__rwd">
                                                <div class="teacher_details">
                                                    <h5 class="rwd_show"><?php the_title(); ?></h5>
                                                    <?php
                                                    $video = get_field('video_link');
                                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match);
                                                    $youtube_id = $match[1];
                                                    if ($video) {
                                                        echo '<div class="def_video_popup rwd_show"><a href="' . $video . '" data-fancybox class="play_icon"><img src="' . theme() . '/images/play.svg" alt="play video"></a><img src="' . youtube_image($youtube_id) . '" alt="" /></div>';
                                                    }
                                                    ?>
                                                    <div class="flex rwd_hide">
                                                        <?php $teacher_thumb = get_post_thumbnail_id(); ?>
                                                        <figure class="teacher_box_thumb"><img
                                                                    src="<?php echo ($teacher_thumb) ? image_src($teacher_thumb, 'thumbnail') : theme() . '/images/gravatar.png' ?>"
                                                                    alt="<?php the_title() ?>"></figure>
                                                        <div>
                                                            <h5><?php the_title(); ?></h5>
                                                            <?php if (get_field('age')) { ?>
                                                                <div class="teacher_age rwd__hide"><?php the_field('age'); ?></div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="table_info">
                                                        <?php if (get_field('age')) { ?>
                                                            <div class="table_row flex__rwd rwd_show">
                                                                <div class="t_title"><?php echo __('[:ua]Вік:[:ru]Возраст:') ?></div>
                                                                <div class="t_value"><?php the_field('age'); ?></div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if (get_field('nationality')) { ?>
                                                            <div class="table_row flex__rwd">
                                                                <div class="t_title"><?php echo __('[:ua]Національність:[:ru]Национальность:') ?></div>
                                                                <div class="t_value"><?php the_field('nationality'); ?></div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if (get_field('experience')) { ?>
                                                            <div class="table_row flex__rwd">
                                                                <div class="t_title"><?php echo __('[:ua]Стаж роботи:[:ru]Стаж работы:') ?></div>
                                                                <div class="t_value"><?php the_field('experience'); ?></div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if (get_field('courses')) { ?>
                                                            <div class="table_row flex__rwd">
                                                                <div class="t_title"><?php echo __('[:ua]Викладає курси:[:ru]Преподает курсы:') ?></div>
                                                                <div class="t_value"><?php the_field('courses'); ?></div>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if (get_field('interests')) { ?>
                                                            <div class="table_row flex__rwd">
                                                                <div class="t_title"><?php echo __('[:ua]Інтереси:[:ru]Интересы:') ?></div>
                                                                <div class="t_value"><?php the_field('interests'); ?></div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <?php
                                                    $video = get_field('video_link');
                                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match);
                                                    $youtube_id = $match[1];
                                                    if ($video) {
                                                        echo '<div class="def_video_popup rwd_hide"><a href="' . $video . '" data-fancybox class="play_icon"><img src="' . theme() . '/images/play.svg" alt="play video"></a><img src="' . youtube_image($youtube_id) . '" alt="" /></div>';
                                                    }
                                                    ?>
                                                    <a href="#t_lesson" class="button rwd_hide"><?php echo __('[:ua]Записатися на пробне заняття[:ru]Записаться на пробное занятие') ?></a>
                                                </div>
                                                <div class="teacher_description">
                                                    <h5><?php echo __('[:ua]Про себе:[:ru]Про себя:') ?></h5>
                                                    <div>
                                                        <?php the_content(); ?>
                                                        <div class="full_text rwd_show">
                                                            <span class="show_txt"><?php echo __('[:ua]Розгорнути[:ru]Развернуть') ?></span>
                                                            <span class="hide_txt"><?php echo __('[:ua]Згорнути[:ru]Свернуть') ?></span>
                                                        </div>
                                                    </div>
                                                    <a href="#t_lesson" class="button rwd_show"><?php echo __('[:ua]Записатися на пробне заняття[:ru]Записаться на пробное занятие') ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;
                            } ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
                <div class="t_slider_nav nav_left"><img src="<?php echo theme(); ?>/images/arrow-l.svg" alt=""></div>
                <div class="t_slider_nav nav_right"><img src="<?php echo theme(); ?>/images/arrow-r.svg" alt=""></div>
            </div>
            <div class="flex_center__mob buttons container">
                <a href="/vchitelyam/" class="button transparent"><?php echo __('[:ua]Хочу бути вчителем[:ru]Хочу быть учителем') ?></a>
                <a href="#t_lesson" class="button"><?php echo __('[:ua]Записатися на пробне заняття[:ru]Записаться на пробное занятие') ?></a>
            </div>
        </div>
    <?php } ?>
</section>
<?php if (get_field('content')) { ?>
    <section class="teachers_content container">
        <?php the_field('content'); ?>
    </section>
<?php } ?>
<?php get_template_part('tpl-parts/trial-lesson-row') ?>
<?php get_footer(); ?>
