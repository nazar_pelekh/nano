<?php get_header(); ?>
<div class="single_sales_top bg" style="<?php echo (get_field('bg_img') ? image_src( get_field('bg_img'), 'top_default', true ) : 'background-image: url('. theme('images/top_default.jpg') .')'); ?>">
    <?php if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
    } ?>
   <div class="flex__rwd container">
       <div class="hide" id="date_end"><?php the_field('end_date') ?></div>
       <?php
       $type = get_field('type_tb');
       $text_f = get_field('text_form');
       $text_v = get_field('text_video');
       $text_t = get_field('text_time'); ?>
       <div class="single_sales_top_left">
           <?php if ($type == "text_f") { ?>
               <?php echo $text_f['text']; ?>
           <?php } elseif ($type == "text_v") { ?>
               <?php echo $text_v['text']; ?>
           <?php } elseif ($type == "text_t") { ?>
               <?php echo $text_t['text']; ?>
               <?php if ($text_t['promo_code']) { ?>
                   <div class="sales_promo_code flex">
                       <div id="s_p_code">
                           <?php echo $text_t['promo_code']; ?>
                       </div>
                       <button onclick="copyToClipboard('#s_p_code')" class="button"><i class="far fa-clone"></i>Копіювати</button>
                       <script>
                           function copyToClipboard(element) {
                               var $temp = $("<input>");
                               $("body").append($temp);
                               $temp.val($(element).html()).select();
                               document.execCommand("copy");
                               $temp.remove();
                           }
                       </script>
                   </div>
               <?php } ?>
               <?php echo $text_t['list']; ?>
           <?php } ?>
       </div>
       <div class="single_sales_top_right">
           <?php if ($type == "text_f") { ?>
               <?php echo $text_f['form']; ?>
           <?php } elseif ($type == "text_v") { ?>
              <div class="single_sales_video_review">
                  <h5><?php echo __('[:ua]Приклад відео-відгуку[:ru]Пример видео-отзыва') ?></h5>
                  <?php
                  $video = $text_v['video'];
                  if ($video['type'] == 'library') { ?>
                      <div class="def_video_popup bg" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                          <a href="#video" data-fancybox class="play_icon">
                              <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                          </a>
                          <video id="video" src="<?php echo $video['file']; ?>" controls muted style="display: none"></video>
                      </div>
                  <?php } elseif ($video['type'] == 'link') { ?>
                      <div class="def_video_popup bg" style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                          <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                              <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                          </a>
                      </div>
                  <?php } ?>
              </div>
           <?php } elseif ($type == "text_t") { ?>
               <h4><?php echo __('[:ua]До кінця акції залишилось[:ru]До конца акции осталось') ?></h4>
               <div class="sales_counter" data-counter="<?php echo get_field('end_date') ?>">
                   <div class="days-title">
                       <div class="days"></div>
                       <?php echo __('[:ua]дні[:ru]дни') ?>
                   </div>
                   <div class="hours-title">
                       <div class="hours"></div>
                       <?php echo __('[:ua]годин[:ru]часов') ?>
                   </div>
                   <div class="minutes-title">
                       <div class="minutes"></div>
                       <?php echo __('[:ua]хвилин[:ru]минут') ?>
                   </div>
                   <div class="seconds-title">
                       <div class="seconds"></div>
                       секунд
                   </div>
               </div>
           <?php } ?>
           <div class="promo_period"><?php the_field('prom_val') ?></div>
       </div>

   </div>
</div>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="single_sales_content container">
        <?php the_content(); ?>
    </section>
<?php endwhile; endif; ?>
<section class="flex_text_video single_sales_vt">
    <div class="container">
        <div class="flex__mob">
            <div class="half_text">
                <?php the_field('text_vt') ?>
            </div>
            <?php
            $video = get_field('video_vt');
            if ($video['type'] == 'library') { ?>
                <div class="half_video def_video_popup"
                     style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                    <a href="#video" data-fancybox class="play_icon">
                        <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                    </a>
                    <video id="video" src="<?php echo $video['file']; ?>" controls muted
                           style="display: none"></video>
                </div>
            <?php } elseif ($video['type'] == 'link') { ?>
                <div class="half_video def_video_popup"
                     style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                    <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                        <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                    </a>
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<?php get_template_part('tpl-parts/how-we-teach') ?>

<?php if (get_field('trial_lesson') == true) { get_template_part('tpl-parts/trial-lesson-row'); } ?>
<?php get_footer(); ?>
