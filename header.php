<?php if( @!WP_DEBUG) {	ob_start('ob_html_compress'); } ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php wpa_title(); ?></title>
    <meta name="MobileOptimized" content="width"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#F81C29">

    <link rel="icon" type="image/x-icon" href="<?php echo theme('images/nano_icon.svg'); ?>" sizes="16x16">

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> data-hash="<?php wpa_fontbase64(true); ?>" data-a="<?php echo admin_url('admin-ajax.php'); ?>" >
<div id="main">
    <header>
        <div class="header_desktop hide_mob">
            <div class="header_top">
                <div class="container flex__rwd">
                    <nav id="menu"><?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s" class="flex__rwd">%3$s</ul>', 'theme_location'  => 'main_menu')); ?></nav>
                    <div class="header_top__buttons flex">
                        <a href="tel:+<?php echo preg_replace('/[^0-9]/', '', get_field("phone","option")); ?>" class="button smaller black"><i class="fas fa-phone-alt"></i><?php the_field("phone","option"); ?></a>
<!--                        <div class="user_window">-->
<!--                            --><?php //if(!wp_get_current_user()->exists()){ ?>
<!--                                <a href="javascript:;" class="enter_link" data-fancybox data-src="#register">--><?php ///*echo __('[:ua]Реєстрація[:ru]Регистрация'); */?><!--</a><a href="javascript:;" class="enter_link" data-fancybox data-src="#enter"><?php //echo __('[:ua]Вхід[:ru]Вход'); ?></a>-->
<!--                            --><?php //} else { ?>
<!--                                <div class="enter_link">-->
<!--                                    <p>-->
<!--                                        <span class="avatar">-->
<!--                                            <i class="fas fa-smile"></i>-->
<!--                                        </span>-->
<!--                                        <i class="fas fa-chevron-down"></i>-->
<!--                                    </p>-->
<!--                                </div>-->
<!--                                <div class="exit_window">-->
<!--                                    <a href="https://app.moyklass.com/login">-->
<!--                                        --><?php //echo __('[:ua]Кабінет[:ru]Кабинет'); ?>
<!--                                    </a>-->
<!--                                    <a href="--><?php //echo wp_logout_url(get_permalink()); ?><!--" title="exit">-->
<!--                                        --><?php //echo __('[:ua]Вихід[:ru]Выход'); ?>
<!--                                    </a>-->
<!--                                </div>-->
<!--                            --><?php //} ?>
<!--                        </div>-->
<!--                        --><?php //echo qtranxf_generateLanguageSelectCode('list') ?>
                    </div>
                </div>
            </div>
            <div class="header_bottom">
                <div class="container flex__rwd">
                    <?php echo (get_field('logo', 'option') ? '<a href="' . site_url() . '" class="logo"><img src="' . get_field('logo', 'option') . '" alt="logo"></a>' : ''); ?>
                    <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s" class="flex__rwd">%3$s</ul>', 'theme_location'  => 'course_menu')); ?>
                    <a href="/nanotest" class="button smaller"><?php echo __('[:ua]Пройти nanoTest[:ru]Пройти nanoTest') ?></a>
                </div>
            </div>
        </div>
        <div class="header_mob hide_desk">
            <div class="header_mob__top flex">
                <?php echo (get_field('logo_mobile', 'option') ? '<a href="' . site_url() . '" class="logo_mob"><img src="' . get_field('logo_mobile', 'option') . '" alt="logo"></a>' : ''); ?>
                <a href="/nanotest" class="button smaller"><?php echo __('[:ua]Пройти nanoTest[:ru]Пройти nanoTest') ?></a>
                <?php echo (get_field('logo', 'option') ? '<a href="' . site_url() . '" class="logo"><img src="' . get_field('logo', 'option') . '" alt="logo"></a>' : ''); ?>
                <button class="nav_icon" href=""><i></i><i></i><i></i></button>
            </div>
            <div class="header_mob__bottom">
                <a href="tel:+<?php echo preg_replace('/[^0-9]/', '', get_field("phone","option")); ?>"><i class="fas fa-phone-alt"></i> <?php the_field("phone","option"); ?></a>
            </div>
        </div>
        <div class="header_mob__open hide_desk">
            <div class="header_mob__open_top">
                <a href="tel:+<?php echo preg_replace('/[^0-9]/', '', get_field("phone","option")); ?>" class="button smaller reverse"><i class="fas fa-phone-alt"></i> <?php the_field("phone","option"); ?></a>
                <div class="flex">
<!--                    --><?php //if(!wp_get_current_user()->exists()){ ?>
<!--                        <a href="javascript:;" class="enter_link" data-fancybox data-src="#enter">--><?php //echo __('[:ua]Вхід[:ru]Вход') ?><!--</a>-->
<!--                    --><?php //} else { ?>
<!--                        <a href="https://app.moyklass.com/login" class="enter_link">-->
<!--                            --><?php ///*echo __('[:ua]Кабінет[:ru]Кабинет'); */?>
<!--                        </a>-->
<!--                        <a href="--><?php //echo wp_logout_url(get_permalink()); ?><!--" class="enter_link" title="exit">-->
<!--                            --><?php //echo __('[:ua]Вихід[:ru]Выход'); ?>
<!--                        </a>-->
<!--                    --><?php //} ?>
<!--                    --><?php //echo qtranxf_generateLanguageSelectCode('list') ?>
                </div>
            </div>
            <div class="header_mob__open_top__open_menu">
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s" class="flex__rwd">%3$s</ul>', 'theme_location'  => 'course_menu')); ?>
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s" class="flex__rwd">%3$s</ul>', 'theme_location'  => 'main_menu')); ?>
                <div class="nanotest_button">
                    <a href="/nanotest" class="button smaller"><?php echo __('[:ua]Пройти nanoTest[:ru]Пройти nanoTest') ?></a>
                </div>
            </div>
        </div>
        <?php echo do_shortcode('[ajax_login]'); ?>
        <?php echo do_shortcode('[ajax_register]'); ?>

        <div id="thanks_mail" class="popup" style="display: none;">
            <div class="close"></div>
            <h4><?php echo __('[:ua]Дякуємо за Ваш вибір![:ru]Спасибо за Ваш выбор!') ?></h4>
            <p style="text-align: center;">
                <?php echo __("[:ua]Ми зв'яжемось з Вами.[:ru]Мы свяжемся с Вами") ?>
                <strong id="nano_user_mail"></strong>
            </p>
<!--            <img src="--><?php //echo theme() ?><!--/images/check_success_nano.svg" alt="">-->
<!--            <a href="--><?php //echo site_url(); ?><!--" class="button transparent">--><?php //echo __('[:ua]На головну[:ru]На главную') ?><!--</a>-->
        </div>

        <!-- Forgot password popup -->
        <div id="forgot_password" class="popup" style="display: none;">
            <div class="close"></div>
            <h4><?php echo __('[:ua]Забув пароль[:ru]Забыл пароль') ?></h4>
            <form method="post" class="form_inputs">
                <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_login" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                    <label>Email</label>
                </div>
                <input class="button" name="wp-submit" type="submit" value="<?php echo __('[:ua]Відправити[:ru]Отправить') ?>" class="wpcf7-form-control wpcf7-submit button">
                <div id="messageforgot"></div>
            </form>
        </div>

<!--        <div style="display:none">-->
<!--        <a id="restore_password_button" href="javascript:;" data-fancybox data-src="#restore_password"></a>-->
<!--        </div>-->

        <!-- Restore password popup -->
        <div id="restore_password" class="popup" style="display: none;">
            <div class="close"></div>
            <h4><?php echo __('[:ua]Відновити пароль[:ru]Восстановить пароль') ?></h4>
            <form method="post" class="form_inputs">
                <div class="form_line">
                    <input type="hidden" name="login" value="<?php echo $_GET['login'] ? $_GET['login'] : ""; ?>">
                    <input type="hidden" name="key" value="<?php echo $_GET['key'] ? $_GET['key'] : ""; ?>">
                    <div class="form_line">
                        <span class="wpcf7-form-control-wrap">
                            <input type="text" name="pass1" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                        </span>
                        <label><?php echo __('[:ua]Пароль[:ru]Пароль') ?></label>
                    </div>
                    <div class="form_line">
                        <span class="wpcf7-form-control-wrap">
                            <input type="text" name="pass2" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                        </span>
                        <label><?php echo __('[:ua]Повторіть пароль[:ru]Повторите пароль') ?></label>
                    </div>
                </div>
                <input class="button" name="wp-submit" type="submit" value="<?php echo __('[:ua]Відправити[:ru]Отправить') ?>" class="wpcf7-form-control wpcf7-submit button">
                <div id="messagerestore"></div>
            </form>
        </div>

        <!-- Subscribe popup -->
        <div id="subscribe_popup" class="popup" style="display: none;">
            <div class="close"></div>
            <h4><?php echo __('[:ua]Підписатися на розсилку[:ru]Подписаться на рассылку') ?></h4>
            <?php
            if ( 'ua' === $GLOBALS['q_config']['language']) {
                echo do_shortcode('[contact-form-7 id="294" title="Subscribe popup"]');
            } else {
                echo do_shortcode('[contact-form-7 id="979" title="Subscribe popup (ru)"]');
            }
            ?>
        </div>

        <!-- Trial lesson popup -->
        <div id="trial_popup" class="popup" style="display: none;">
            <div class="close"></div>
            <h4><?php echo __('[:ua]Запис на пробне заняття[:ru]Запись на пробное занятие') ?></h4>
            <?php
            if ( 'ua' === $GLOBALS['q_config']['language']) {
                echo do_shortcode('[contact-form-7 id="5" title="Trial lesson"]');
            } else {
                echo do_shortcode('[contact-form-7 id="981" title="Trial lesson (ru)"]');
            }
            ?>
        </div>
    </header>