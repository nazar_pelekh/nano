<?php get_header(); ?>

<div id="breadcrumbs" class="regular-test__breadcrumbs">
    <div class="container">
        <span>
            <span>
                <a href="https://nanoenglish.com/"><?php echo __('[:ua]Головна[:ru]Главная') ?></a> /
                <span class="breadcrumb_last" aria-current="page"><?php echo __('[:ua]Тести[:ru]Тесты') ?></span>
            </span>
        </span>
    </div>
</div>

<section class="content container">

    <?php
    $args = array(
        'type'          => 'post',
        'orderby'       => 'term_group',
        'hide_empty'    => 0,
        'hierarchical'  => 0,
        'parent'        => 0,
        'taxonomy'      => 'type_test'
    );
    if($top_l = get_categories( $args )) { ?>
        <div class="regular-test__home">
            <?php the_field('title__regular','cpt_regular_tests'); ?>
            <div class="regular-test__categories">
                <?php foreach ($top_l as $top) { ?>
                    <div class="regular-test__category">
                        <div class="regular-test__icon">
                            <?php echo file_get_contents(z_taxonomy_image_url($top->term_id)) ?>
                        </div>
                        <h2><?php echo $top->name ?></h2>
                        <div class="hover-block">
                            <span class="regular-test__count"><?php echo $top->count .' '. __('[:ua]тестів[:ru]тестов') ?></span>
                            <a href="<?php echo get_category_link($top->term_id) ?>" class="button"><?php echo __('[:ua]Обрати[:ru]Выбрать') ?></a>
                        </div>

                    </div>
                <?php } ?>
            </div>

        </div>
    <?php } ?>

    <?php if($desc = get_field('description__regular','cpt_regular_tests')) { ?>
        <div class="regular-test__description">
            <?php echo $desc; ?>
        </div>
    <?php } ?>


</section>

<?php get_footer(); ?>
