<?php

get_header();

$format_in = 'Y-m-d H:i:s'; // the format your value is saved in (set in the field options)
$format_out = 'd.m.Y'; // the format you want to end up with
$date = DateTime::createFromFormat($format_in, get_field('webinar_date'));
$time = boolval();

$now = date('Y-m-d H:i:s');

if($now <= $date->format( $format_in )) {
    $time = true;
} else {
    $time = false;
}

?>

<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>

<div class="container">
    <div class="flex_start__rwd webinar_section">
        <div class="item webinar_info">
            <?php if($author = get_field('guest')['name']) { ?>
                <div class="guest flex_center rwd_show <?php echo !$time ? 'expired' : ''; ?>">
                    <img src="<?php echo get_field('guest')['avatar'] ?>" alt="">
                    <div class="guest_info">
                        <h4><?php echo $author ?></h4>
                        <p><?php echo $time ? get_field('webinar_date_text') : __('[:ua]Вебінар завершено![:ru]Вебинар завершен!') ?></p>
                    </div>
                </div>
            <?php } ?>
            <h2><?php the_title(); ?></h2>
            <?php the_field('main_content') ?>
        </div>
        <div class="item">
            <div class="guest_item">
                <?php if($author = get_field('guest')['name']) { ?>
                    <div class="guest flex_center rwd_hide <?php echo !$time ? 'expired' : ''; ?>">
                        <img src="<?php echo get_field('guest')['avatar'] ?>" alt="">
                        <div class="guest_info">
                            <h4><?php echo $author ?></h4>
                            <p><?php echo $time ? get_field('webinar_date_text') : __('[:ua]Вебінар завершено![:ru]Вебинар завершен!') ?></p>
                        </div>
                    </div>
                <?php } ?>

                <div class="webinar_form">
                    <div id="webinar_send_link" class="hide">
                        <?php echo $time ? get_field('webinar_link')['online'] : get_field('webinar_link')['download'];?>
                    </div>
                    <?php
                    if($time) {
                        if ( 'ua' === $GLOBALS['q_config']['language']) {
                            echo do_shortcode('[contact-form-7 id="964" title="Webinar Will Be (ua)"]');
                        } else {
                            echo do_shortcode('[contact-form-7 id="965" title="Webinar Will Be (ru)"]');
                        }
                    } else {
                        if ( 'ua' === $GLOBALS['q_config']['language']) {
                            echo do_shortcode('[contact-form-7 id="969" title="Webinar Done (ua)"]');
                        } else {
                            echo do_shortcode('[contact-form-7 id="970" title="Webinar Done (ru)"]');
                        }
                    }
                    ?>
                </div>

                <?php if($time) { ?>

                    <div class="webinar_countdown">
                        <h4><?php echo get_field('сountdown_title') ? get_field('сountdown_title') : __('[:ua]До кінця акції залишилось[:ru]До конца акции осталось') ?></h4>
                        <div class="sales_counter" data-counter="<?php echo get_field('webinar_date') ?>">
                            <div class="days-title">
                                <div class="days"></div>
                                <?php echo __('[:ua]дні[:ru]дни') ?>
                            </div>
                            <div class="hours-title">
                                <div class="hours"></div>
                                <?php echo __('[:ua]годин[:ru]часов') ?>
                            </div>
                            <div class="minutes-title">
                                <div class="minutes"></div>
                                <?php echo __('[:ua]хвилин[:ru]минут') ?>
                            </div>
                            <div class="seconds-title">
                                <div class="seconds"></div>
                                секунд
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>


<?php get_template_part('tpl-parts/reviews-slider') ?>

<?php get_footer(); ?>
