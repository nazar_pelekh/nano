<?php get_header();
$term = get_queried_object();

?>

<div id="breadcrumbs" class="regular-test__breadcrumbs">
    <div class="container">
        <span>
            <span>
                <a href="https://nanoenglish.com/"><?php echo __('[:ua]Головна[:ru]Главная') ?></a> /
                <a href="https://nanoenglish.com/tests"><?php echo __('[:ua]Тести[:ru]Тесты') ?></a> /
                <span class="breadcrumb_last" aria-current="page"><?php echo $term->name ?></span>
            </span>
        </span>
    </div>
</div>

<section class="content container hide_mob">

    <?php if($title = get_field('title_category', $term)) {
        echo '<div class="regular-test__title">'.$title.'</div>';
    } ?>

    <?php
    $args = array(
        'type'          => 'post',
        'orderby'       => 'term_group',
        'hide_empty'    => 0,
        'hierarchical'  => 0,
        'parent'        => 0,
        'taxonomy'      => 'type_test'
    );
    if($top_l = get_categories( $args )) { ?>
        <div class="regular-test__tabber">
            <div class="regular-test__top-categories">
                <?php foreach ($top_l as $top) { ?>
                    <a href="<?php echo get_category_link($top->term_id) ?>" class="regular-test__top-category <?php echo $term->term_id == $top->term_id ? 'is_active' : ''; ?>">
                        <?php echo $top->name ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php
    $parent_cat_ID = $term->term_id;
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent_cat_ID,
        'taxonomy' => 'type_test',
        'orderby' => 'slug'
    );
    $subcats = get_categories($args); ?>
    <div class="regular-test__tests">
        <div class="regular-test__tests-drop">
            <?php foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                echo '<div class="regular-test__tests-drop-item">';
                echo '<div class="'.$sc->slug.' regular-test__tests-drop-top">'.$sc->name.'<img src="'.get_template_directory_uri().'/images/right_arr_black.svg" alt=""></div>';

                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                if($subsubcats) {
                    echo '<div class="regular-test__tests-drop-items">';
                    foreach ($subsubcats as $a) {
                        $link = get_term_link($a->slug, $a->taxonomy);
                        echo '<a href="javascript:;" data-cat="' . $a->slug . '">' . $a->name . '</a>';
                    }
                    echo '</div>';
                }
                echo '</div>';
            } ?>
        </div>
        <div class="regular-test__tests-res">
            <div class="regular-test__tab tests--default">
                <img src="<?php echo get_template_directory_uri() ?>/images/user_interface_2.svg" alt="">
                <?php the_field('default_text', $term) ?>
            </div>
            <?php foreach ($subcats as $sc) { ?>
                <?php
                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                foreach ($subsubcats as $a) { ?>
                    <div class="regular-test__tab tests--posts" data-term="<?php echo $a->slug ?>">
                <?php
                    $arg = array(

                        'post_type' => 'regular_tests',
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'type_test',
                                'field'    => 'slug',
                                'terms'    => array( $a->slug )
                            )
                        )
                    );
                    $tests = new WP_Query( $arg );
                    if ( $tests->have_posts() ) { ?>
                        <?php foreach ($tests->posts as $post) {
                            echo '<a href="/tests/'.$term->slug./*'/'.$sc->slug.*/'/'.$post->post_name.'"><span>'.$post->post_title.'</span></a>';
                        } ?>
                    <?php }
                    wp_reset_postdata(); ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>


    <?php if($desc = get_field('description',$term)) { ?>
        <div class="regular-test__description">
            <?php echo $desc; ?>
        </div>
    <?php } ?>

</section>

<section class="content container hide_desk">

    <div class="regular-test__filtering-mob">
        <?php
        $args = array(
            'type'          => 'post',
            'orderby'       => 'term_group',
            'hide_empty'    => 0,
            'hierarchical'  => 0,
            'parent'        => 0,
            'taxonomy'      => 'type_test'
        );
        if($top_l = get_categories( $args )) { ?>
            <p><?php echo __('[:ua]Оберіть розділ[:ru]Выберите раздел') ?></p>
            <select name="" id="test__title">
                <?php foreach ($top_l as $top) { ?>
                    <option data-title="<?php echo $top->slug ?>" data-select="<?php echo $top->name ?>" data-href="<?php echo get_category_link($top->term_id) ?>" class="<?php echo $term->term_id == $top->term_id ? 'is_active' : ''; ?>" value="<?php echo $top->slug ?>">
                        <?php echo $top->name ?>
                    </option>
                <?php } ?>
            </select>
        <?php } ?>

        <?php
        $parent_cat_ID = $term->term_id;
        $args = array(
            'hierarchical' => 1,
            'show_option_none' => '',
            'hide_empty' => 0,
            'parent' => $parent_cat_ID,
            'taxonomy' => 'type_test',
            'orderby' => 'slug'
        );
        $subcats = get_categories($args); ?>
        <p><?php echo __('[:ua]Оберіть рівень складності[:ru]Выберите уровень сложности') ?></p>
        <select name="" class="regular-test__filtering-select test__category">
            <option value=""><?php echo __('[:ua]Оберіть рівень[:ru]Выберите уровень') ?></option>
            <?php $count = 0; foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                echo '<option data-title="'.$term->name.'" data-cat="'.$sc->slug.'" value="'.$count++.'">'.$sc->name.'</option>';
            } ?>
        </select>

        <div class="regular-test__mob-themes is_disabled">
            <p><?php echo __('[:ua]Оберіть цікаву тему[:ru]Выберите интересную тему'); ?></p>
            <?php foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );

                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                if($subsubcats) { ?>
                    <div class="regular-test__mob-theme hide">
                        <select name="" class="test__theme regular-test__filtering-select hide">
                            <option value=""><?php echo __('[:ua]Оберіть тему[:ru]Выберите тему') ?></option>
                            <?php foreach ($subsubcats as $a) {
                                $link = get_term_link($a->slug, $a->taxonomy);
                                echo '<option data-title="'.$a->name.'" value="'.$a->slug.'" >'.$a->name.'</option>';
                            } ?>
                        </select>
                    </div>
                <? }
            } ?>
        </div>

        <div class="regular-test__mob-posts is_disabled">
                <p><?php echo __('[:ua]Оберіть тип завдання[:ru]Выберите тип задания') ?></p>

            <?php foreach ($subcats as $sc) { ?>
                <?php
                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                foreach ($subsubcats as $a) { ?>
                    <div class="regular-test__mob-post hide" data-cat="<?php echo $a->slug; ?>">
                        <?php
                        $arg = array(
                            'post_type' => 'regular_tests',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'type_test',
                                    'field'    => 'slug',
                                    'terms'    => array( $a->slug )
                                )
                            )
                        );
                        $tests = new WP_Query( $arg );
                        if ( $tests->have_posts() ) { ?>
                        <select name="" class="regular-test__filtering-select test__post">
                            <option value="#"><?php echo __('[:ua]Оберіть тип[:ru]Выберите тип') ?></option>
                            <?php foreach ($tests->posts as $post) {
                                echo '<option value="/tests/'.$term->slug./*'/'.$sc->slug.*/'/'.$post->post_name.'" >'.$post->post_title.'</option>';
                            } ?>
                        </select>
                        <?php } wp_reset_postdata(); ?>
                    </div>
                <?php } ?>
            <?php } ?>
            </div>

        <button class="button regular-test__filtering-submit is_disabled"><?php echo __('[:ua]Почати Тестування[:ru]Начать Тестирование') ?></button>
    </div>





    <div class="regular-test__tests hide">
        <div class="regular-test__tests-drop">
            <?php foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                echo '<div class="regular-test__tests-drop-item">';
                echo '<div class="'.$sc->slug.' regular-test__tests-drop-top">'.$sc->name.'<img src="'.get_template_directory_uri().'/images/right_arr_black.svg" alt=""></div>';

                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                if($subsubcats) {
                    echo '<div class="regular-test__tests-drop-items">';
                    foreach ($subsubcats as $a) {
                        $link = get_term_link($a->slug, $a->taxonomy);
                        echo '<a href="javascript:;" data-cat="' . $a->slug . '">' . $a->name . '</a>';
                    }
                    echo '</div>';
                }
                echo '</div>';
            } ?>
        </div>
        <div class="regular-test__tests-res">
            <div class="regular-test__tab tests--default">
                <img src="<?php echo get_template_directory_uri() ?>/images/user_interface_2.svg" alt="">
                <?php the_field('default_text', $term) ?>
            </div>
            <?php foreach ($subcats as $sc) { ?>
                <?php
                $arg = array(
                    'child_of' => $sc->term_id,
                    'hierarchical' => 2,
                    'show_option_none' => '',
                    'hide_empty' => 0,
                    'taxonomy' => 'type_test',
                );
                $subsubcats = get_categories($arg);
                foreach ($subsubcats as $a) { ?>
                    <div class="regular-test__tab tests--posts" data-term="<?php echo $a->slug ?>">
                        <?php
                        $arg = array(

                            'post_type' => 'regular_tests',
                            'tax_query' => array(
                                'relation' => 'AND',
                                array(
                                    'taxonomy' => 'type_test',
                                    'field'    => 'slug',
                                    'terms'    => array( $a->slug )
                                )
                            )
                        );
                        $tests = new WP_Query( $arg );
                        if ( $tests->have_posts() ) { ?>
                            <?php foreach ($tests->posts as $post) {
                                echo '<a href="/tests/'.$term->slug./*'/'.$sc->slug.*/'/'.$post->post_name.'"><span>'.$post->post_title.'</span></a>';
                            } ?>
                        <?php }
                        wp_reset_postdata(); ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>


    <?php if($desc = get_field('description',$term)) { ?>
        <div class="regular-test__description">
            <?php echo $desc; ?>
        </div>
    <?php } ?>

</section>

<?php get_footer(); ?>
