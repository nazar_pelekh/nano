<?php /*Template Name: mail*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta charset="UTF-8">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
    <!--[if !mso]> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
    <!--<![endif]-->
</head>

<body>
<div class="es-wrapper-color">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#ffffff"></v:fill>
    </v:background>
    <![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="esd-email-paddings" valign="top">
                <table class="es-content esd-header-popover" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                    <tr>
                        <td class="esd-stripe" style="background-color: #ffffff;" bgcolor="#ffffff" align="center">
                            <table class="es-content-body" style="background-color: #ffffff;" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p20t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="esd-container-frame" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" class="esd-block-image" style="font-size: 0px;"><a target="_blank"><img class="adapt-img" src="https://fdjnmo.stripocdn.email/content/guids/fc738660-2906-437d-b86d-c68eefb14243/images/38621585231852296.png" alt style="display: block;" width="188"></a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="es-footer" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                    <tr>
                        <td class="esd-stripe" style="background-color: #ffffff;" bgcolor="#ffffff" align="center">
                            <table class="es-footer-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center">
                                <tbody>
                                <tr>
                                    <td class="esd-structure es-p20t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="esd-container-frame" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="esd-block-text">
                                                                <p style="line-height: 200%;">Привіт,&nbsp;[your-name]</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" class="es-content esd-footer-popover" align="center">
                    <tbody>
                    <tr>
                        <td class="esd-stripe" align="center" bgcolor="transparent" style="background-color: transparent;">
                            <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600">
                                <tbody>
                                <tr>
                                    <td class="es-p20t es-p20r es-p20l esd-structure" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="esd-container-frame" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="esd-block-text es-p10b">
                                                                <p style="line-height: 150%; color: #000000;">Ти успішно пройшов nanoTest.<br>Ось твої результати:</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="560" class="esd-container-frame" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="esd-block-text es-p10t" bgcolor="#f3f6f8">
                                                                <p style="color: #f81c29; line-height: 150%;">VOCABULARY&nbsp;&nbsp;<strong><span style="color: #000000;">[text-290]</span><span style="color: #717171; font-size: 13px; line-height: 150%;">/27</span></strong><br>GRAMMAR<strong>&nbsp; &nbsp; &nbsp; &nbsp;</strong><strong><span style="color: #000000;">[text-291]</span><span style="color: #717171; font-size: 13px; line-height: 150%;">/[text-294]</span></strong><br><span style="font-size: 16px;">LISTENING</span><strong><span style="font-size: 16px;">&nbsp; &nbsp; &nbsp; &nbsp;<span style="color: #000000;">[text-292]</span><span style="color: #717171; font-size: 13px; line-height: 150%;">/9</span></span></strong><br><span style="font-size: 16px; color: #000000;">TOTAL&nbsp;</span><strong><span style="font-size: 16px;"><span style="color: #000000; font-size: 16px;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; [text-296]</span><span style="color: #717171; font-size: 13px; line-height: 150%;">/63<br>____________________________</span></span></strong><br><br><span style="color: #000000;"><strong>Твій рівень англійської&nbsp;</strong></span>[text-174]<br><span style="color:#000000;">[text-175]</span></p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="esd-structure es-p20t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="esd-container-frame" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" class="esd-block-text">
                                                                <p style="color: #000000;"><strong><span style="color:null;">Рекомендація nanoEnglish:</span></strong><br>Враховуючи результати тесту, та обрану мету навчання:</p>
                                                                <p style="color: #000000; text-align: left;"><u>[text-176]</u><br>ми пропонуємо прямо зараз <strong>обрати найбільш підходящий для тебе</strong></p>
                                                                <p style="color: #000000; text-align: center;"><strong>КУРС&nbsp;[text-173]</strong></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-button es-m-txt-l es-p10t es-p25b es-p10r"><span class="es-button-border es-button-border-1585243189197" style="border-radius: 11px; border-color: #f81c29; background: #ffffff; border-width: 1px;"><a href="https://nanoenglish.com/prices/" class="es-button es-button-1585243189160" target="_blank" style="border-radius: 11px; color: #f81c29; border-width: 15px 20px;">ОПЛАТИТИ КУРС</a></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-text" bgcolor="#f3f6f8">
                                                                <p style="color: #000000; font-size: 16px;">або ж пройти <span style="color:#f81c29;"><strong>безкоштовне заняття</strong></span>&nbsp;з вчителем з курсу<br><strong>[text-173]</strong></p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-button es-m-txt-l es-p10t es-p40b es-p10r" bgcolor="#f3f6f8"><span class="es-button-border es-button-border-1585244291106" style="border-radius: 11px; border-color: #3d5ca3; background: #f81c29; border-width: 0px;"><a href="https://nanoenglish.com/probne-zanyattya/" class="es-button es-button-1585244291105" target="_blank" style="background: #f81c29; border-color: #f81c29; color: #ffffff; border-radius: 11px; border-width: 15px 20px;">БЕЗКОШТОВНЕ ЗАНЯТТЯ</a></span></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="esd-structure es-p25t es-p25b es-p25r es-p25l" style="background-color: #333333;" bgcolor="#333333" align="left" esd-custom-block-id="56950">
                                        <!--[if mso]><table width="550" cellpadding="0" cellspacing="0"><tr><td width="173" valign="top"><![endif]-->
                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame es-m-p20b" width="173" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-image es-m-txt-l" align="left" style="font-size: 0px;"><a target="_blank"><img src="https://fdjnmo.stripocdn.email/content/guids/CABINET_eb5610758521eaf32899576bfb7d56fc/images/24191585245660386.png" alt style="display: block;" width="158"></a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td><td width="181" valign="top"><![endif]-->
                                        <table class="es-left" cellspacing="0" cellpadding="0" align="left">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame es-m-p20b" esd-custom-block-id="11022" width="181" align="left">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-image es-p10r" width="30" valign="top" align="right" style="font-size: 0px;"><a href target="_blank"><img src="https://fdjnmo.stripocdn.email/content/guids/CABINET_eb5610758521eaf32899576bfb7d56fc/images/14481585256581148.png" alt style="display: block;" width="19"></a></td>
                                                            <td align="left">
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="esd-block-text" align="left">
                                                                            <p style="color: #f81c29;">+12 34567890</p>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td><td width="0"></td><td width="196" valign="top"><![endif]-->
                                        <table class="es-right" cellspacing="0" cellpadding="0" align="right">
                                            <tbody>
                                            <tr>
                                                <td class="esd-container-frame" esd-custom-block-id="11022" width="196" align="left">
                                                    <table width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td class="esd-block-image es-p10r" width="30" valign="top" align="right" style="font-size: 0px;"><a href target="_blank"><img src="https://fdjnmo.stripocdn.email/content/guids/CABINET_eb5610758521eaf32899576bfb7d56fc/images/41191585256609117.png" alt style="display: block;" width="21"></a></td>
                                                            <td align="left">
                                                                <table width="100%" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="esd-block-text" align="left" esd-links-color="#F81C29">
                                                                            <p><a target="_blank" href="mailto:info@company-name.com" style="color: #f81c29;">info@nanoenglish.com</a></p>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>

</html>