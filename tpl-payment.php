<?php

/* Template Name: Payment */
get_header();
$isLogged = is_user_logged_in();
?>

<section class="payment_content">
    <div class="container">
        <div style='display:none;' id='isLogged' data-logged='<?php echo $isLogged ? "true" : "false"; ?>'>
            <a href="javascript:;" class="enter_link" data-fancybox data-src="#success" id="successBtn"></a>
        </div>
        <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
        } ?>
        <div class="payment_text">
            <?php the_content(); ?>
        </div>
        <div class="payment_prices">
            <div class="payment_courses flex">
                <ul class="tabs flex">
                    <li class="tab-link current" data-tab="basicEnglish">basicEnglish</li>
                    <li class="tab-link" data-tab="businessEnglish">businessEnglish</li>
                    <li class="tab-link" data-tab="IELTS">IELTS</li>
                </ul>
                <div class="currency flex currency_1">
                    <button class="active" data-cur="UAH">UAH</button>
                    <button data-cur="USD">USD</button>
                    <button data-cur="EUR">EUR</button>
                </div>
            </div>
            <?php
            $courses = new WP_Query(array(
                    'post_type' => 'courses',
                    'posts_per_page' => '-1',
                    'post_status'   => 'publish',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'courses_cat',
                            'field'    => 'slug',
                            'terms'    => 'basicenglish'
                        )
                    ),
                    'order' => 'ASC'
            ));
            ?>
            <div id="basicEnglish" class="tab-content current">
                <div class="flex__rwd">
                    <?php foreach($courses->posts as $course): ?>
                    <div class="item flex">
                        <div class="courses_count">
                            <p>
                                <strong><?php echo get_field("courses_count",$course->ID); ?></strong>
                                <?php echo __('[:ua]Занять[:ru]Занятий') ?>
                            </p>
                            <button class="button small chooseCourse" data-price="<?php echo get_field("price_for_1_course",$course->ID); ?>" data-courseid="<?php echo $course->ID; ?>" data-coursesale="<?php echo get_field("sale",$course->ID) ?>" data-coursescount="<?php echo get_field("courses_count",$course->ID); ?>" data-coursename="Basic English" data-fancybox data-src="#payment">
                                <?php echo __('[:ua]Обрати план[:ru]Выбрать план') ?>
                            </button>
                        </div>
                        <div class="courses_info">
                            <p>
                                <i class="check_icon"></i>
                                <span><?php echo __('[:ua]'.get_field("course_time",$course->ID).' хв[:ru]'.get_field("course_time",$course->ID).' мин') ?></span>
                            </p>
                            <p>
                                <i class="check_icon"></i>
                                <span><?php echo __('[:ua]'.get_field("sale",$course->ID).' знижка[:ru]'.get_field("sale",$course->ID).' скидка') ?></span>
                            </p>
                            <p>
                                <i class="check_icon"></i>
                                <span class="pricefoc" data-pricefoc="<?php echo get_field("price_for_1_course", $course->ID); ?>"><span><?php echo get_field("price_for_1_course",$course->ID); ?>₴</span><?php echo __('[:ua]/заняття[:ru]/занятие') ?></span>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php
            $courses = new WP_Query(array(
                'post_type' => 'courses',
                'posts_per_page' => '-1',
                'post_status'   => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'courses_cat',
                        'field'    => 'slug',
                        'terms'    => 'businessenglish'
                    )
                ),
                'order' => 'ASC'
            ));
            ?>
            <div id="businessEnglish" class="tab-content">
                <div class="flex__rwd">
                    <?php foreach($courses->posts as $course): ?>
                        <div class="item flex">
                            <div class="courses_count">
                                <p>
                                    <strong><?php echo get_field("courses_count",$course->ID); ?></strong>
                                    <?php echo __('[:ua]Занять[:ru]Занятий') ?>
                                </p>
                                <button class="button small chooseCourse" data-price="<?php echo get_field("price_for_1_course",$course->ID); ?>" data-courseid="<?php echo $course->ID; ?>" data-coursesale="<?php echo get_field("sale",$course->ID) ?>" data-coursescount="<?php echo get_field("courses_count",$course->ID); ?>" data-coursename="Business English" data-fancybox data-src="#payment">
                                    <?php echo __('[:ua]Обрати план[:ru]Выбрать план') ?>
                                </button>
                            </div>
                            <div class="courses_info">
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("course_time",$course->ID).' хв[:ru]'.get_field("course_time",$course->ID).' мин') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("sale",$course->ID).' знижка[:ru]'.get_field("sale",$course->ID).' скидка') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span class="pricefoc" data-pricefoc="<?php echo get_field("price_for_1_course", $course->ID); ?>"><span><?php echo get_field("price_for_1_course",$course->ID); ?>₴</span><?php echo __('[:ua]/заняття[:ru]/занятие') ?></span>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php
            $courses = new WP_Query(array(
                'post_type' => 'courses',
                'posts_per_page' => '-1',
                'post_status'   => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'courses_cat',
                        'field'    => 'slug',
                        'terms'    => 'ielts'
                    )
                ),
                'order' => 'ASC'
            ));
            ?>
            <div id="IELTS" class="tab-content">
                <div class="flex__rwd">
                    <?php foreach($courses->posts as $course): ?>
                        <div class="item flex">
                            <div class="courses_count">
                                <p>
                                    <strong><?php echo get_field("courses_count",$course->ID); ?></strong>
                                    <?php echo __('[:ua]Занять[:ru]Занятий') ?>
                                </p>
                                <button class="button small chooseCourse" data-price="<?php echo get_field("price_for_1_course",$course->ID); ?>" data-courseid="<?php echo $course->ID; ?>" data-coursesale="<?php echo get_field("sale",$course->ID) ?>" data-coursescount="<?php echo get_field("courses_count",$course->ID); ?>" data-coursename="IELTS" data-fancybox data-src="#payment">
                                    <?php echo __('[:ua]Обрати план[:ru]Выбрать план') ?>
                                </button>
                            </div>
                            <div class="courses_info">
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("course_time",$course->ID).' хв[:ru]'.get_field("course_time",$course->ID).' мин') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("sale",$course->ID).' знижка[:ru]'.get_field("sale",$course->ID).' скидка') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span class="pricefoc" data-pricefoc="<?php echo get_field("price_for_1_course", $course->ID); ?>"><span><?php echo get_field("price_for_1_course",$course->ID); ?>₴</span><?php echo __('[:ua]/заняття[:ru]/занятие') ?></span>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('tpl-parts/payment-popup') ?>

<?php get_footer(); ?>
