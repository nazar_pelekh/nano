<?php get_header();
global $post;
?>

<section class="single_post">
    <div class="container">
        <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
        } ?>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="container flex_start__rwd contain_side">
        <div class="content">
            <h1><?php the_title(); ?></h1>
            <!--            <img src="--><?php //echo image_src( get_post_thumbnail_id( $post->ID ), 'single' ); ?><!--" alt="--><?php //the_title(); ?><!--">-->
            <!--            <time datetime="--><?php //echo get_the_date('Y-m-d'); ?><!--">--><?php //echo get_the_date('F j, Y'); ?><!--</time>-->
<!--            <div class="cats">Category: --><?php //echo cats($post->ID); ?><!--</div>-->
            <?php

            if( have_rows('blocks') ): ?>
                <?php while( have_rows('blocks') ): the_row(); ?>
                    <?php if( get_row_layout() == 'anchor_links' ): ?>
                        <section class="post_links">
                            <?php the_sub_field('anchor_links'); ?>
                        </section>
                    <?php elseif( get_row_layout() == 'content' ): ?>
                        <section id="<?php the_sub_field('anchor') ?>" class="post_content">
                            <?php the_sub_field('content'); ?>
                        </section>
                    <?php elseif( get_row_layout() == 'vocabulary_words' ): ?>
                        <section id="<?php the_sub_field('anchor') ?>" class="vocabulary_words">
                            <?php the_sub_field('title'); ?>
                            <?php if($word_line = get_sub_field('word_line')) { ?>
                                <div class="word_lines">
                                    <?php foreach ($word_line as $w) { ?>
                                        <div class="word_line">
                                            <div class="item original">
                                                <?php echo $w['frase'] ?>
                                            </div>
                                            <?php if($w['translate']) { ?>
                                                <i class="fas fa-chevron-right"></i>
                                                <div class="item translate">
                                                    <?php echo $w['translate'] ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <?php the_sub_field('content'); ?>
                        </section>
                    <?php elseif( get_row_layout() == 'vocabulary_text' ): ?>
                        <section id="<?php the_sub_field('anchor') ?>" class="vocabulary_text">
                            <?php the_sub_field('text') ?>
                        </section>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif;

            foreach( get_the_category() as $category ){
                echo '<a href="'.$category->slug.'" class="cat">'.$category->cat_name.'</a>';
            }
            ?>

            <div class="post_user flex">
                <?php echo do_shortcode('[avatar]') ?>
                <h3><?php the_author_meta('display_name') ?></h3>
            </div>
            
            <div class="post_like_share flex">
                <?php echo get_simple_likes_button( get_the_ID() ); ?>
                <div class="share_buttons">
                    <a class="i_fcbk" href="https://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share at Facebook" target="_blank" rel="noopener"><i class="fab fa-facebook-square"></i></a>
                    <a href="tg://msg_url?url=<?php the_permalink();?>&amp;text=<?php the_title(); ?>" title="Share at Telegram" target="_blank" rel="noopener"><i class="fab fa-telegram"></i></a>
                    <a title="WeChat" onclick="e_mailit.updateCounter(this);e_mailit.gaSendEvent('E-MAILiT Share','WeChat','<?php the_permalink();?>');return !window.open(this.href, 'WeChat', 'width=600,height=600');" target="_blank" href="https://www.e-mailit.com/ext/wechat/share/#url=<?php the_permalink(); ?>" class="E_mailit_WeChat"><i class="fab fa-weixin"></i></a>
                    <!--                <a class="i_wechat" href="https://www.linkedin.com/shareArticle?mini=true&amp;title=--><?php //the_title(); ?><!--&amp;url=--><?php //the_permalink(); ?><!--" title="Share at LinkedIn" target="_blank" rel="noopener"></a>-->
                    <a class="i_viber" href="viber://forward?text=<?php the_permalink(); ?>" title="Share at Viber" target="_blank" rel="noopener"><img src="<?php echo theme('/images/viber.png') ?>" alt=""></a>
                    <a class="i_pntrst" href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>" title="Pin It" target="_blank" rel="noopener"></a>
                </div>
            </div>

            <?php if(function_exists('comments_template')) :
                comments_template();
            endif; ?>
        </div>
        <aside class="sidebar">
            <ul class="sidebar_item posts_categories">
                <li><h6><?php echo __('[:ua]Теми публікацій[:ru]Tемы публикаций'); ?></h6></li>
                <li class="cat-item"><a href="/blog"><?php echo __('[:ua]Усі теми[:ru]Bсе темы'); ?></a></li>
                <?php
                $highlight = array();
                $categories = get_the_category();
                foreach ($categories as $category)
                    $highlight[] = $category->cat_ID;

                $args = array(
                    'hierarchical' => 0,
                    'title_li' => '',
                    'show_option_none' => '',
                    'highlight' => $highlight,
//                    'walker' => new TFCategoryWalker(),
                );
                wp_list_categories($args);
                ?>
            </ul>
            <div class="subscribe sidebar_item">
                <h5><?php echo __('[:ua]Хочеш отримувати корисні листи? Тоді підпишись на нашу розсилку![:ru]Хочешь получать полезные письма? Тогда подпишись на нашу рассылку!'); ?></h5>
                <?php
                if ( 'ua' === $GLOBALS['q_config']['language']) {
                    echo do_shortcode('[contact-form-7 id="240" title="Subscribe sidebar"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="980" title="Subscribe sidebar (ru)"]');
                }
                ?>
            </div>
            <div class="related_posts sidebar_item">

                <?php

                $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
                if( $related ) { ?>
                    <h6><?php echo __('[:ua]Інші публікації з теми[:ru]Другие публикации по теме'); ?></h6>
                    <?php foreach( $related as $post ) {
                    setup_postdata($post); ?>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>" class="flex flex_item">
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'thumbnail') ?>" alt="">
                        <span><?php the_title(); ?></span>
                    </a>

                <?php }
                wp_reset_postdata(); } ?>
            </div>
            <div class="sidebar_item trial_lesson_aside">
                <h5><?php echo __('[:ua]Потрібна допомога у досягенні мети? Запишиться на пробне заняття![:ru]Нужна помощь в достижения цели? Запишиться на пробное занятие!'); ?></h5>
                <?php
                if ( 'ua' === $GLOBALS['q_config']['language']) {
                    echo do_shortcode('[contact-form-7 id="241" title="Trial Lesson Post"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="982" title="Trial Lesson Post (ru)"]');
                }
                ?>
            </div>
            <div class="sidebar_item sidebar__ads">
                <?php if($ads = get_field('ads')) {
                    echo $ads;
                } else {
                    the_field('ads','option');
                } ?>
            </div>
        </aside>
    </div>
    <?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>
