<?php get_header(); /*Template Name: For Partners*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section class="for_partners_title">
    <div class="container">
        <h1><?php the_title(); ?></h1>
    </div>
</section>
<?php if ($reverse_content = get_field('reverse_content' )) { ?>
   <section class="for_partners_content">
      <div class="container">
          <?php foreach ($reverse_content as $row) { ?>
              <div class="row flex__mob">
                  <figure><img src="<?php echo $row['image']['sizes']['large']; ?>" alt="<?php echo $row['image']['alt']; ?>"></figure>
                  <div class="content"><?php echo $row['text']; ?></div>
              </div>
          <?php } ?>
      </div>
   </section>
<?php } ?>
<section class="for_partners_wyg">
    <div class="container">
        <?php echo(get_field('title_wyg') ? get_field('title_wyg') : '') ?>
        <?php if ($boxes = get_field('boxes' )) { ?>
          <div class="flex_center__mob">
              <?php foreach ($boxes as $box) { ?>
                  <div class="item">
                      <figure>
                          <img src="<?php echo $box['image']['url']; ?>" alt="<?php echo $box['image']['alt']; ?>">
                      </figure>
                      <?php echo $box['text']; ?>
                  </div>
              <?php } ?>
          </div>
        <?php } ?>
    </div>
</section>
<section class="contact_content for_partners_contact">
    <div class="container flex__mob">
        <?php if (get_field('partners_form')) { ?>
            <div class="content form_box"><?php the_field('partners_form'); ?></div>
        <?php } ?>


        <div class="contact_boxes">
            <div class="contact_consultation">
                <h5><?php echo __('[:ua]Отримайте консультацію[:ru]Получите консультацию') ?></h5>
                <?php if ($contact_faces = get_field('contact_faces', 397)) { ?>
                    <div class="contact_faces">
                        <?php foreach ($contact_faces as $face) { ?>
                            <figure><img src="<?php echo $face['image']['sizes']['thumbnail']; ?>"
                                         alt="<?php echo $face['image']['alt']; ?>"></figure>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php echo(get_field('consultation_info', 397) ? get_field('consultation_info', 397) : '') ?>
                <?php if ($messengers = get_field('messengers', 397)) { ?>
                    <div class="contact_messengers">
                        <?php foreach ($messengers as $messenger) { ?>
                            <a href="<?php echo $messenger['link'] ?>" target="_blank"><span
                                        class="<?php echo $messenger['messenger']; ?>"></span></a>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
            <?php if ($socials = get_field('links', 'option')) { ?>
                <div class="contact_socials">
                    <h5><?php echo __('[:ua]Ми в соцмережах[:ru]Мы в соцсетях') ?></h5>
                    <?php foreach ($socials as $s) { ?>
                        <a href="<?php echo $s['link'] ?>" target="_blank"><?php echo $s['icon'] ?></a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
