<?php
acf_form_head();
get_header(); /*Template Name: Reviews*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs" class="reviews_breadcrumbs"><div class="container">', '</div></div>');
} ?>
<?php get_template_part('tpl-parts/reviews-slider') ?>
    <section class="leave_review">
        <div class="container">
            <h4><?php echo __('[:ua]Залиште свій відгук[:ru]Оставьте свой отзыв') ?></h4>
            <?php
            if ( 'ua' === $GLOBALS['q_config']['language']) {
                acf_form(array(
                    'post_id' => 'new_post',
                    'field_groups' => array('295'),
                    'uploader' => 'basic',
                    'post_title' => true,
                    'submit_value' => 'Залишити відгук',
                    'new_post' => array(
                        'post_type' => 'reviews',
                        'post_status' => 'draft',
                    )
                ));
                echo do_shortcode('[contact-form-7 id="310" title="Review form"]');
            } else {
                acf_form(array(
                    'post_id' => 'new_post',
                    'field_groups' => array('295'),
                    'uploader' => 'basic',
                    'post_title' => true,
                    'submit_value' => 'Oставить отзыв',
                    'new_post' => array(
                        'post_type' => 'reviews',
                        'post_status' => 'draft',
                    )
                ));
                echo do_shortcode('[contact-form-7 id="977" title="Review form (ru)"]');
            }
            ?>
        </div>
    </section>
    <div id="thanks_for_review">
        <h4><?php echo __('[:ua]Дякуємо![:ru]Спасибо!') ?></h4>
        <h5><?php echo __('[:ua]Ваш відгук успішно додано[:ru]Ваш отзыв успешно добавлен') ?></h5>
        <div class="t_r_check"><span class="fas fa-check"></span></div>
        <a href="#" data-fancybox-close class="button transparent"><?php echo __('[:ua]Окей, Закрити[:ru]Окей, Закрыть') ?></a>
    </div>
<?php get_template_part('tpl-parts/trial-lesson-row') ?>
<?php get_footer(); ?>