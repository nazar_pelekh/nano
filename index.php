<?php get_header();  /*$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;*/
//if(is_home()) :
//    $queryname = 'Blog';
//else :
//    $queryname = 'Archive of ' . get_the_archive_title();
//endif;
?>

<section class="posts_holder">
    <div class="container">
        <?php if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
        } ?>
        <aside class="sidebar rwd_show">
            <ul class="sidebar_item posts_categories">
                <li><h6><?php echo __('[:ua]Теми публікацій[:ru]Tемы публикаций'); ?></h6></li>
                <li class="cat-item"><a href="/blog"><?php echo __('[:ua]Усі теми[:ru]Bсе темы'); ?></a></li>
                <?php
                $highlight = array();
                $categories = get_the_category();
                foreach ($categories as $category)
                    $highlight[] = $category->cat_ID;

                $args = array(
                    'hierarchical' => 0,
                    'title_li' => '',
                    'show_option_none' => '',
                    'highlight' => $highlight,
//                    'walker' => new TFCategoryWalker(),
                );
                wp_list_categories($args);
                ?>
            </ul>
        </aside>
        <h2><?php echo get_the_title(BLOG_ID) ?></h2>
    </div>
    <div class="container flex_start__rwd contain_side">
        <div class="content">
            <div class="posts">
                <?php if (have_posts()) : $p = 0; while (have_posts()) : the_post(); ?>
                    <?php if($p == 0 /*&& $paged == 1*/) : ?>
                        <div class="first_post blog__post">
                            <a href="<?php the_permalink(); ?>" class="thumb">
                                <img src="<?php echo image_src( get_post_thumbnail_id( $post->ID ), 'single' ); ?>" alt="<?php the_title(); ?>">
                            </a>
                            <div class="blog__post_info">
                                <div class="cats">
                                    <a href="/<?php echo get_the_category($post->ID)[0]->slug ?>/"><?php echo get_the_category($post->ID)[0]->name ?></a>
                                </div>
                                <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('j'); echo ' '. month_translate($post->ID) ?></time>
                            </div>
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <div class="blog__post_preview rwd_show">
                                <?php echo wp_trim_words(get_field('preview_text'), 40, '...'); ?>
                            </div>
                            <div class="blog__post_info2">
                                <?php echo do_shortcode('[post-views]') ?>
                                <div class="likes flex">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-heart fa-w-16 fa-3x"><path fill="currentColor" d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z" class=""></path></svg>
                                    <?php echo get_post_meta( $post->ID, "_post_like_count" )[0] ? get_post_meta( $post->ID, "_post_like_count" )[0] : '0'; ?>
                                </div>
                                <div class="comment_count flex">
                                    <img src="<?php echo theme('/images/comment.svg') ?>">
                                    <?php comments_number( '0', '1', '%' ); ?>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="blog__post">
                            <a href="<?php the_permalink(); ?>" class="thumb">
                                <img src="<?php echo image_src( get_post_thumbnail_id( $post->ID ), 'blog' ); ?>" alt="<?php the_title(); ?>">
                            </a>
                            <div class="blog__post_info">
                                <div class="cats">
                                    <a href="/<?php echo get_the_category($post->ID)[0]->slug ?>/"><?php echo get_the_category($post->ID)[0]->name ?></a>
                                </div>
                                <time datetime="<?php echo get_the_date('Y-m-d'); ?>"><?php echo get_the_date('j'); echo ' '. month_translate($post->ID) ?></time>
                            </div>
                            <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <div class="blog__post_preview">
                                <?php echo wp_trim_words(get_field('preview_text'), 40, '...'); ?>
                            </div>
                            <div class="blog__post_info2">
                                <?php echo do_shortcode('[post-views]') ?>
                                <div class="likes flex">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="heart" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-heart fa-w-16 fa-3x"><path fill="currentColor" d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z" class=""></path></svg>
                                    <?php echo get_post_meta( $post->ID, "_post_like_count" )[0] ? get_post_meta( $post->ID, "_post_like_count" )[0] : '0'; ?>
                                </div>
                                <div class="comment_count flex">
                                    <img src="<?php echo theme('/images/comment.svg') ?>">
                                    <?php comments_number( '0', '1', '%' ); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; $p++; ?>
                <?php endwhile; endif; ?>
                <?php if(function_exists('wp_pagenavi')) :
                    wp_pagenavi();
                endif; ?>
            </div>
        </div>
        <aside class="sidebar">
            <ul class="sidebar_item posts_categories rwd_hide">
                <li><h6><?php echo __('[:ua]Теми публікацій[:ru]Tемы публикаций'); ?></h6></li>
                <li class="cat-item"><a href="/blog"><?php echo __('[:ua]Усі теми[:ru]Bсе темы'); ?></a></li>
                <?php
                $highlight = array();
                $categories = get_the_category();
                foreach ($categories as $category)
                    $highlight[] = $category->cat_ID;

                $args = array(
                    'hierarchical' => 0,
                    'title_li' => '',
                    'show_option_none' => '',
                    'highlight' => $highlight,
//                    'walker' => new TFCategoryWalker(),
                );
                wp_list_categories($args);
                ?>
            </ul>
            <div class="subscribe sidebar_item">
                <h5><?php echo __('[:ua]Хочеш отримувати корисні листи? Тоді підпишись на нашу розсилку![:ru]Хочешь получать полезные письма? Тогда подпишись на нашу рассылку!'); ?></h5>
                <?php
                if ( 'ua' === $GLOBALS['q_config']['language']) {
                    echo do_shortcode('[contact-form-7 id="240" title="Subscribe sidebar"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="980" title="Subscribe sidebar (ru)"]');
                }
                ?>
            </div>

        </aside>
    </div>
</section>

<?php get_template_part('tpl-parts/trial-lesson-row') ?>

<?php get_footer(); ?>
