<?php
$face_text = get_field('facebook_text','option');
$widget = get_field('facebook_widget', 'option');
if($face_text || $widget) { ?>
    <section class="widget">
        <div class="container is_smaller flex__rwd">
            <?php echo $face_text ? '<div class="item">'.$face_text.'</div>' : "" ; ?>
            <?php echo $widget ? '<div class="item">'.$widget.'</div>' : "" ; ?>
        </div>
    </section>
<?php } ?><?php
