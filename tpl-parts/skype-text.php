<?php if($steps_content = get_field('content_steps','options')){ ?>
    <div class="container is_smaller">
        <div class="steps_content flex_start__rwd">
            <div class="expand_text">
                <?php the_field('content_steps','options'); ?>
                <!--                        <button class="not_active">--><?php //echo __('[:ua]Розгорнути[:ru]Развернуть') ?><!--</button>-->
                <!--                        <button class="is_active hide">--><?php //echo __('[:ua]Згорнути[:ru]Свернуть') ?><!--</button>-->
            </div>
            <?php echo get_field('image_steps','options') ? '<img src="' . get_field('image_steps','options') . '" class="rwd_hide"/>' : 'empty' ?>
        </div>
    </div>
<?php } ?>