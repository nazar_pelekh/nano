<?php
$terms_cat = wp_get_post_terms(get_the_ID(), 'type_test')[0]->slug;
$terms_tit = end(wp_get_post_terms(get_the_ID(), 'type_test'))->slug;
?>

<div class="nanotest_exit">
    <div class="container flex">
        <a href="javascript:;" data-fancybox data-src="#exit_test"><?php echo __('[:ua]Вийти[:ru]Выйти') ?></a>
        <div class="question_next flex">
            <div class="total_count">
                <span class="current_index"></span>
                /
                <span class="total_index"></span>
            </div>
            <button class="button next_slide is_disabled"><?php echo __('[:ua]Наступне питання[:ru]Следующий вопрос') ?><i class="fas fa-chevron-right"></i></button>
            <a href="/rezultati?<?php echo $terms_cat.'_'.$terms_tit; ?>" class="button next_slide is_disabled hide finish_test"><?php echo __('[:ua]Завершити тест[:ru]Завершить тест') ?><i class="fas fa-chevron-right"></i></a>
        </div>
    </div>
</div>