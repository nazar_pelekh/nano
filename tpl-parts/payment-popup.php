<div id="payment" style="display: none;">
    <div style="display: none"><a href="javascript:;" class="enter_link" id="registerp" data-fancybox data-src="#register"></a></div>
    <!--    <div class="close"></div>-->
    <div class="flex__mob">
        <div class="item flex_column">
            <div class="currency flex currency_2">
                <button class="active" data-cur="UAH">UAH</button>
                <button data-cur="USD">USD</button>
                <button data-cur="EUR">EUR</button>
            </div>
            <div class="payment_info">
                <div class="line flex">
                    <div class="payment_line"><?php echo __('[:ua]Обраний курс[:ru]Выбранный курс') ?></div>
                    <span id="course_name">basicEnglish</span>
                </div>
                <div class="line flex">
                    <div class="payment_line"><?php echo __('[:ua]Кількість занять[:ru]Количество занятий') ?></div>
                    <span id="lessons_count">27</span>
                </div>
                <div class="line flex">
                    <div class="payment_line"><?php echo __('[:ua]Вартість пакету[:ru]Стоимость пакета') ?></div>
                    <div class="course_price">
                        <span id="course_price">5400</span>
                        <span class="currency_type">₴</span>
                    </div>
                </div>
                <div class="line flex discount">
                    <div class="payment_line"><?php echo __('[:ua]Ваша знижка[:ru]Ваша скидка') ?></div>
                    <div class="course_price">
                        <span>-</span>
                        <span id="discount_price">1350</span>
                        <span class="currency_type">₴</span>
                    </div>
                </div>
                <div class="line flex discount promocode_discount" style="display: none">
                    <div class="payment_line"><?php echo __('[:ua]Промокод[:ru]Промокод') ?></div>
                    <div class="course_price">
                        <span>-</span>
                        <span id="promocode_price"></span>
                        <span class="currency_type">₴</span>
                    </div>
                </div>
                <div class="line flex">
                    <div class="payment_line"><?php echo __('[:ua]Сума до сплати[:ru]Сумма к оплате') ?></div>
                    <div class="course_price">
                        <span id="total_price"></span>
                        <span class="currency_type">₴</span>
                    </div>
                </div>
            </div>
            <div class="promocode">
                <span id="promocode_message"></span>
                <input type="text" id="promocode" placeholder="<?php echo __('[:ua]Введіть Промокод[:ru]Введите Промокод') ?>">
            </div>
        </div>
        <div class="item flex_column">
            <div class="payment_type">
                <p><?php echo __('[:ua]Оберіть спосіб оплати[:ru]Выберите способ оплаты') ?></p>
                <form method="POST" action="https://www.liqpay.ua/api/3/checkout" id="liqpay" accept-charset="utf-8">
                    <input type="hidden" name="data" id="liqdata" value=""/>
                    <input type="hidden" name="signature" id="liqsignat" value=""/>
                    <div class="flex choose_payment">
                        <label>
                            <input type="radio" name="pay_type" value="privat24">
                            <img src="<?php echo theme('/images/privat.svg') ?>">
                        </label>
                        <label>
                            <input type="radio" name="pay_type" value="moment_part">
                            <img src="<?php echo theme('/images/part_pay.png') ?>">
                        </label>
                        <label>
                            <input type="radio" name="pay_type" value="card">
                            <img src="<?php echo theme('/images/visa.svg') ?>">
                        </label>
                    </div>
                    <input class="button is_disabled" type="submit" value="<?php echo __('[:ua]Перейти до оплати[:ru]Перейти к оплате') ?>">
                </form>
            </div>
            <?php echo do_shortcode('[ajax_register_desk]') ?>
            <?php echo do_shortcode('[ajax_login_desk]') ?>
            <div id="forgot_password_desk" class="log_desk is_hidden">
                <p><?php echo __('[:ua]Забув пароль[:ru]Забыл пароль') ?></p>
                <form method="post" class="form_inputs">
                    <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_login" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                        <label>Email</label>
                    </div>
                    <input class="wpcf7-form-control wpcf7-submit button" name="wp-submit" type="submit" value="<?php echo __('[:ua]Відправити[:ru]Отправить') ?>">
                    <div id="messageforgot_desk"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="success" class="popup" style="display: none;">
    <h4 id="liqSucName"></h4>
    <?php if($thanks = get_field('thank_you_text','option')) { ?>
        <div class="thanks">
            <?php echo $thanks; ?>
        </div>
    <?php } ?>
    <div class="check_wrap flex">
        <img src="<?php echo theme('/images/check_success.svg') ?>" alt="">
    </div>
    <button class="close_thanks"><?php echo __('[:ua]Окей, Закрити[:ru]Окей, Закрыть') ?></button>
</div>