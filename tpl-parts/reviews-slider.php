<?php $posts = new WP_Query(array(
    'post_type' => 'reviews',
    'posts_per_page' => -1,
)); ?>
<section class="reviews">
    <div class="container">
        <h2><?php echo __('[:ua]Відгуки про Nano English[:ru]Отзывы о Nano English') ?></h2>
    </div>
    <div class="wrap_reviews_slider">
        <div class="swiper-container ">
            <div class="swiper-wrapper">
                <?php if ($posts->have_posts()) :
                    while ($posts->have_posts())  : $posts->the_post(); ?>
                        <div class="swiper-slide">
                            <div class="review_author_info">
                                <div class="flex">
                                    <?php $author_avatar = get_field('photo') ?>
                                    <figure class="author_logo"><img src="<?php echo ($author_avatar['sizes']['thumbnail']) ? $author_avatar['sizes']['thumbnail'] : theme("/images/gravatar.png"); ?>"
                                                                     alt=""></figure>
                                    <div class="info">
                                        <div class="author_name"><?php the_title(); ?></div>
                                        <?php if (get_field('chose_goal')) { ?>

                                            <div class="author_goal">
                                                <span><?php echo __('[:ua]Мета:[:ru]Цель:') ?>: </span><?php the_field('chose_goal'); ?></div>
                                        <?php } ?>
                                        <div class="author_soc_media">
                                            <?php if (get_field('insta_link')) { ?>
                                                <a href="<?php the_field('insta_link'); ?>" target="_blank"><img
                                                        src="<?php echo theme(); ?>/images/review_inst.svg"
                                                        alt=""></a>
                                            <?php } ?>
                                            <?php if (get_field('fb_link')) { ?>
                                                <a href="<?php the_field('fb_link'); ?>" target="_blank"><img
                                                        src="<?php echo theme() ?>/images/review_fb.svg" alt=""></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (get_field('chose_goal')) { ?>
                                    <div class="author_goal author_goal_mob">
                                        <span><?php echo __('[:ua]Мета:[:ru]Цель:') ?> </span><?php the_field('chose_goal'); ?></div>
                                <?php } ?>
                            </div>
                            <div class="review_info">
                                <div class="review_text">
                                    <p>
                                        <?php the_field('message'); ?>
                                    </p>
                                    <div class="full_review">
                                        <span class="show_rev"><?php echo __('[:ua]Розгорнути[:ru]Развернуть') ?></span>
                                        <span class="hide_rev"><?php echo __('[:ua]Згорнути[:ru]Свернуть') ?></span>
                                    </div>
                                </div>
                                <?php
                                $video = get_field( 'video_link' );
                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video, $match);
                                $youtube_id = $match[1];
                                if  ($video) { echo '<div class="def_video_popup"><a href="'.$video.'" data-fancybox class="play_icon"><img src="'.theme().'/images/play.svg" alt="play video"></a><img src="'.youtube_image($youtube_id).'" alt="" /></div>' ; }
                                ?>
                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        <div class="rev_slider_nav nav_left"><img src="<?php echo theme(); ?>/images/arrow-l.svg" alt=""></div>
        <div class="rev_slider_nav nav_right"><img src="<?php echo theme(); ?>/images/arrow-r.svg" alt=""></div>
    </div>
    <?php if (is_page_template('tpl-reviews.php')) {} else { ?>
        <div class="reviews_buttons">
            <div class="container">
                <a href="javascript:;" data-fancybox data-src="#trial_popup" class="button"><?php echo __('[:ua]Хочу так само![:ru]Хочу так же!') ?></a>
                <a href="<?php echo site_url() ?>/vidguki/" class="button transparent"><?php echo __('[:ua]Більше Відгуків[:ru]Более Отзывов') ?></a>
            </div>
        </div>
    <?php } ?>
</section>
