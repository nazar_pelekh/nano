<?php if (get_field('trial_lesson', 'option')) { ?>
    <section id="t_lesson" class="trial_lesson_row">
        <div class="container">
            <?php the_field('trial_lesson', 'option'); ?>
        </div>
    </section>
<?php } ?>