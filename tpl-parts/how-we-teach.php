<?php if($steps = get_field('steps','options')){ ?>
    <section class="steps">
        <div class="container steps_icons">
            <?php echo get_field('title_steps','options') ? '<div class="title_steps">' . get_field('title_steps','options') . '</div>' : ''; ?>
            <div class="step_items flex_start__rwd">
                <?php foreach($steps['4_steps'] as $s) { ?>
                    <div class="step_item">
                        <div class="step_item__icon">
                            <img src="<?php echo $s['icon'] ?>" alt="">
                        </div>
                        <p><?php echo $s['text'] ?></p>
                    </div>
                <?php } ?>
                <?php if($last_step = $steps['last_step']) { ?>
                    <div class="step_item_last">
                        <div class="flex">
                            <?php foreach($last_step['icons'] as $i) { ?>
                                <div class="last_step_icon">
                                    <img src="<?php echo $i['icon'] ?>" alt="">
                                    <p><?php echo $i['text'] ?></p>
                                </div>
                            <?php } ?>
                            <img src="<?php echo $last_step['Image'] ?>" alt="">
                        </div>
                        <p><?php echo $last_step['text'] ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>
