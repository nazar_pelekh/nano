/*jslint browser: true, white: true, plusplus: true, regexp: true, indent: 4, maxerr: 50, es5: true */
/*jshint multistr: true, latedef: nofunc */
/*global jQuery, $, Swiper*/

let regular_tests = {
    points: {},
    answers: {}
};

$('.filters-group').each( function() {

    let $buttonGroup = $( buttonGroup );

    $buttonGroup.on( 'click', 'button', function() {

        let $attr = $(this).attr('data-filter');

        $('.filters-group').removeClass('is-checked');
        $(this).addClass('is-checked').attr('data-filter','*');

        if($(this).hasClass('is-checked')){
            $(this).removeClass('is-checked');
            $(this).attr('data-filter', $attr);
        }

    });

});

$(document).ready(function() {
    'use strict';

    //  hamburger + menu
    $('.nav_icon').on('click', function() {
        $(this).toggleClass('is_active');
        $('.header_mob').toggleClass('is_active');
        $('.header_mob__open').toggleClass('is_active');
        $('.header_mob__top').toggleClass('is_active');
        $('body').toggleClass('is_overflow');
        return false;
    });
    $('#menu .menu-item-has-children > a').after('<span />');
    $('#menu').on('click', '.menu-item-has-children > a + span', function() {
        $(this).toggleClass('is_open').next().stop().toggle().parent().toggleClass('is_active');
    });

    $('#menu-courses-menu-1 > li').on('click', function () {
        $(this).toggleClass('is_active');
        $(this).find('ul.sub-menu').slideToggle();
    })

    /* footer menu fix */
    if($('#menu-footer-column-2 li').length < 5) {
        $('#menu-footer-column-4').addClass('moveUp');
    }

    // payment active
    if($('.tab-content').length > 0) {
        $('.tab-content .item:nth-of-type(3)').addClass('active');
    }


    //  contact form 7
    $(this).on('click', '.wpcf7-not-valid-tip', function() {
        $(this).prev().trigger('focus');
        $(this).fadeOut(250,function(){
            $(this).remove();
        });
    });
    $(this).on('focus', '.wpcf7-form-control:not([type="submit"])', function() {
        $(this).parent().addClass('is_active');
    });
    $(this).on('blur', '.wpcf7-form-control:not([type="submit"])', function() {
        if($(this).val() !== "") {
            $(this).parent().addClass('is_active');
        } else {
            $(this).parent().removeClass('is_active');
        }
    });

    // contact form 7
    $(this).on('click', '.wpcf7-not-valid-tip', function() {
        $(this).prev().trigger('focus');
        $(this).parent().removeClass('is_error');
        $(this).fadeOut(250,function(){
            $(this).remove();
        });
    });
    $(this).on('focus', '.wpcf7-form-control:not([type="submit"])', function() {
        $(this).parent().addClass('is_active').addClass('is_focused').removeClass('is_filled').removeClass('is_error');
        $(this).next().fadeOut(250,function(){
            $(this).remove();
        });
        $('.wpcf7-response-output').fadeOut(250);
    });
    $(this).on('blur', '.wpcf7-form-control:not([type="submit"])', function() {
        if($(this).val() !== "") {
            $(this).parent().addClass('is_active').addClass('is_filled').removeClass('is_focused');
        } else {
            $(this).parent().removeClass('is_active').removeClass('is_filled').removeClass('is_focused');
        }
    });
    $('.input_holder input').each(function () {
        if ( $(this).val() !== "" ) {
            $(this).parent().addClass('is_active').addClass('is_filled');
        }

        var is_dis = $(this).attr('readonly');
        if ( typeof is_dis !== typeof undefined && is_dis !== false ) {
            $(this).parent().addClass('is_disabled');
        }
    });
    $(document).on('DOMNodeInserted', function(e) {
        if (e.target.className === 'wpcf7-not-valid-tip') {
            $('.wpcf7-not-valid-tip').parent().addClass('is_error');
        }
    });
    $('.wpcf7').on('wpcf7:mailsent', function() {
        $('.wpcf7-form-control-wrap.is_active').removeClass('is_active').removeClass('is_filled');
    });
    $('.input_holder textarea').each(function () {
        if ( $(this).val() !== "" ) {
            $(this).parent().addClass('is_active').addClass('is_filled');
        }
    });

    /* webinar link */
    if($('#webinar_send_link').length > 0) {
        $('#webinar_link').val($('#webinar_send_link').text());
    }

    /* expand text */
    /*$('.expand_text .not_active').on('click', function(){
        $('.expand_text').addClass('is_opened');
        $('.expand_text p').slideDown();
        $(this).addClass('hide');
        $(this).next().removeClass('hide');
    });
    $('.expand_text .is_active').on('click', function(){
        $('.expand_text').removeClass('is_opened');
        $('.expand_text p:not(:first-of-type)').slideUp();
        $(this).addClass('hide');
        $(this).prev().removeClass('hide');
    });*/


    /* tabber */
    $('ul.tabs li').click(function(){
        var tab_id = $(this).attr('data-tab');

        $('ul.tabs li').removeClass('current');
        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $("#"+tab_id).addClass('current');
    })

    /* course detail */
    $('.tpl-course-detail .tab-content .item').on('click', function () {
        $('.tab-content .item').removeClass('active')
        $(this).addClass('active');
        $('.course_column_row').removeClass('active').addClass('closed');
        $('.popup_row').removeClass('active').addClass('closed');
        if($(this).hasClass('lessons_9')) {
            $('.course_column_row.lessons_9').removeClass('closed').addClass('active');
            $('.popup_row.lessons_9').removeClass('closed').addClass('active');
        } else if ($(this).hasClass('lessons_18')) {
            $('.course_column_row.lessons_9').removeClass('closed').addClass('active');
            $('.course_column_row.lessons_18').removeClass('closed').addClass('active');
            $('.popup_row.lessons_9').removeClass('closed').addClass('active');
            $('.popup_row.lessons_18').removeClass('closed').addClass('active');
        } else if ($(this).hasClass('lessons_27')) {
            $('.course_column_row.lessons_9').removeClass('closed').addClass('active');
            $('.course_column_row.lessons_18').removeClass('closed').addClass('active');
            $('.course_column_row.lessons_27').removeClass('closed').addClass('active');
            $('.popup_row.lessons_9').removeClass('closed').addClass('active');
            $('.popup_row.lessons_18').removeClass('closed').addClass('active');
            $('.popup_row.lessons_27').removeClass('closed').addClass('active');
        } else {
            $('.course_column_row').removeClass('closed').addClass('active');
            $('.popup_row').removeClass('closed').addClass('active');
        }
    })

    /* payment */
    $('.tpl-payment .tab-content .item').on('click', function () {
        $('.tab-content .item').removeClass('active');
        $(this).addClass('active');
    });

    $('.currency button').on('click', function () {
        // $(this).parent().children('button').removeClass('active');
        $('.currency button').removeClass('active');
        $('.currency').find('button').eq($(this).index()).addClass('active');
        $('.currency_2').find('button').eq($(this).index()).addClass('active');
        currencyliq = $('.currency_2').find('button').eq($(this).index()).data('cur');
        // console.log(index);
    })
    $('.choose_payment input').on('click', function() {
        if($(this).is(':checked')) {
            $('.payment_type .button').removeClass('is_disabled');
        }
    });

    $('#payment button.fancybox-button').on('click', function () {
        $('#payment input[type="text"], #payment input[type="email"], #payment input[type="password"]').val('');
        $('#payment input[type="radio"]').prop('checked', false);
        $('#payment .error, #messageforgot_desk').text('');
        $('#payment .promocode').removeClass('promocode_error').removeClass('promocode_success');
        $('#payment .discount').hide();
        $('.payment_type .button').addClass('is_disabled')
    });

    $('.log_link_button').on('click', function () {
        $('#register_desk').addClass('is_hidden');
        $('#login_desk').removeClass('is_hidden');
    })

    $('.register_link_button').on('click', function () {
        $('#register_desk').removeClass('is_hidden');
        $('#login_desk').addClass('is_hidden');
    });

    $('#forgot_password_button_desk').on('click', function () {
        $('#forgot_password_desk').removeClass('is_hidden');
        $('#login_desk').addClass('is_hidden');
    })


    $('.nanotest_goals label').on('click', function () {
        $('.nanotest_goals label').removeClass('active');
        $(this).addClass('active');
        $('.nanotest_col .button.is_disabled').removeClass('is_disabled');
    })


    var exUSD, exEUR;

    $.ajax({
        type: 'GET',
        url: "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5",
        success: function (html) {
            for( var i = 0; i < html.length; i++)
            {
                if(html[i].ccy == "USD")
                    exUSD = html[i].sale;
                if(html[i].ccy == "EUR")
                    exEUR = html[i].sale;

            }
        }
    });

    $(".currency_1 button").on('click', function(){

        $(".currency_2 button[data-cur="+$(this).data('cur')+"]").click();
        changeCurrency();

    });


    function changeCurrency()
    {

        var currentCurrency = $(".currency_1 button.active").data('cur');
        if(!currentCurrency)
        {
            currentCurrency = $(".currency_2 button.active").data('cur');
        }
        var currentCurrSymbol;
        if(currentCurrency == "EUR") currentCurrSymbol = "€";
        if(currentCurrency == "USD") currentCurrSymbol = "$";
        if(currentCurrency == "UAH") currentCurrSymbol = "₴";
        var newPrice;

        $(".pricefoc").each(function(index){
            switch (currentCurrency) {
                case "UAH":newPrice = $(this).data('pricefoc');break;
                case "USD":newPrice = $(this).data('pricefoc') / exUSD;break;
                case "EUR":newPrice = $(this).data('pricefoc') / exEUR;break;
            }
            $(this).find("span").text(newPrice.toFixed(2) + currentCurrSymbol);
            // console.log(index + $(this).data('pricefoc'));
        });

        switch (currentCurrency) {
            case "UAH":
                $("#course_price").text(selCoursePrice + courseDiscount);
                $("#discount_price").text(courseDiscount);
                $("#total_price").text(selCoursePrice);
                if(promodiscount)
                {
                    $("#promocode_price").text((promodiscount).toFixed(2));
                    $("#total_price").text(((selCoursePrice - promodiscount)).toFixed(2));
                }
                break;
            case "USD":
                $("#course_price").text(((selCoursePrice + courseDiscount) / exUSD).toFixed(2));
                $("#discount_price").text((courseDiscount / exUSD).toFixed(2));
                $("#total_price").text((selCoursePrice / exUSD).toFixed(2));
                if(promodiscount)
                {
                    $("#promocode_price").text((promodiscount / exUSD).toFixed(2));
                    $("#total_price").text(((selCoursePrice - promodiscount) / exUSD).toFixed(2));

                }
                break;
            case "EUR":
                $("#course_price").text(((selCoursePrice + courseDiscount) / exEUR).toFixed(2));
                $("#discount_price").text((courseDiscount / exEUR).toFixed(2));
                $("#total_price").text((selCoursePrice / exEUR).toFixed(2));
                if(promodiscount)
                {
                    $("#promocode_price").text((promodiscount / exEUR).toFixed(2));
                    $("#total_price").text(((selCoursePrice - promodiscount) / exEUR).toFixed(2));
                }
                break;
        }
        $(".currency_type").text(currentCurrSymbol);
        //checkPromocode(true);
    }
    var courseID, currencyliq, selCoursePrice, selCourseTotalPrice, courseDiscount, promodiscount;

    var promocode = false;
    $("#promocode").on('change', function(){
        promocode = $(this).val();
        checkPromocode();
    });

    function checkPromocode(faked = false){
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: {
                action: "check_promo",
                promocode: promocode,
            },
            success: function (response) {
                response = JSON.parse(response);
                if(response.error && !faked)
                {
                    $(".promocode_discount").css('display', 'none');
                    $("#promocode_message").text(response.msg);
                    $("#total_price").text(selCoursePrice);
                    $("#total_price").data('pricefoc', selCoursePrice);
                    $('.promocode').addClass('promocode_error');
                    $('#promocode').val('');
                    promocode = false;
                }
                if(response.success)
                {
                    if(response.discount)
                    {
                        $(".promocode_discount").css('display', 'flex');
                        $("#promocode_message").text(response.msg);
                        promodiscount = selCoursePrice/100*response.discount;
                        $("#promocode_price").text(promodiscount);
                        $("#total_price").text(selCoursePrice - promodiscount);
                        $("#total_price").data('pricefoc', selCoursePrice - promodiscount);
                        $('.promocode').removeClass('promocode_error').addClass('promocode_success');
                        changeCurrency();
                    }
                }
            }
        });
        return false;
    }


    /* payment sum */
    $('#total_price').html( $('#discount_price').length > 0 ? $('#course_price').html() - $('#discount_price').html() : $('#course_price').html());

    $(".chooseCourse").on('click', function(){
        let coursePrice, coursesCount, courseName;
        courseID = $(this).data('courseid');
        coursePrice = parseFloat($(this).data('price'));
        coursesCount = $(this).data('coursescount');
        courseName = $(this).data('coursename');

        courseDiscount = $(this).data('coursesale');
        courseDiscount = courseDiscount.match(/\d+/)[0];

        selCoursePrice = coursePrice*coursesCount;


        // courseDiscount = parseFloat((courseDiscount/100*selCoursePrice).toFixed(2));
        var startPrice = parseFloat(selCoursePrice/((100-courseDiscount)/100)).toFixed(2);
        courseDiscount = startPrice - selCoursePrice;



        $("#course_name").text(courseName);
        $("#lessons_count").text(coursesCount);

        $("#course_price").text(courseDiscount);
        $("#total_price").text(startPrice);
        $("#total_price").data('pricefoc', selCoursePrice);

        $("#discount_price").text(courseDiscount);
        changeCurrency();
    });

    $(".currency_2 button, input[name='pay_type']").on('click', function(){
        var pay_type = $("input[name='pay_type']:checked").val();

        changeCurrency();

        var liqData, liqSignat;

        $.ajax({
            type: 'POST',
            url: simpleLikes.ajaxurl,
            data: {
                action: "getSignature",
                pay_type: pay_type,
                courseID: courseID,
                currency: currencyliq,
                promocode: promocode,
            },
            success: function (html) {
                var result = JSON.parse(html);
                if(result.error)
                {
                    alert(result.error);return false;
                }
                liqData = result.data;
                liqSignat = result.signature;
                $("#liqdata").val(liqData);
                $("#liqsignat").val(liqSignat);
            }
        });

    });

    $("#forgot_password form").on('submit', function () {
        var username = $(this).find('input[name=user_login]').val();
        $.ajax({
            type: 'POST',
            url: simpleLikes.ajaxurl,
            data: {
                action: 'forgot_pass',
                user_login: username
            },
            success: function (html) {
                $("#messageforgot").text(html);
            }
        });
        return false;
    });

    $("#forgot_password_desk form").on('submit', function () {
        var username = $(this).find('input[name=user_login]').val();
        $.ajax({
            type: 'POST',
            url: simpleLikes.ajaxurl,
            data: {
                action: 'forgot_pass',
                user_login: username
            },
            success: function (html) {
                $("#messageforgot_desk").text(html);
            }
        });
        return false;
    });

    function findGetParameter(parameterName) {
        var result = null,
            tmp = [];
        location.search
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    var actionRestore = findGetParameter("action");
    if(actionRestore == "rp")
    {
        $("#restore_password_button").click();
        window.history.pushState({}, document.title, "/");
    }

    $("#restore_password form").on("submit", function(){

        $.ajax({
            type: 'POST',
            url: simpleLikes.ajaxurl,
            data: {
                action: 	'reset_pass',
                pass1:		$(this).find("input[name=pass1]").val(),
                pass2:		$(this).find("input[name=pass2]").val(),
                user_key:	$(this).find("input[name=key]").val(),
                user_login:	$(this).find("input[name=login]").val(),
            },
            success: function (html) {
                $("#messagerestore").text(html);

            }
        });

        return false;
    });



    $( document ).ajaxComplete(function( event, xhr, settings ) {
        var data = settings.data;
        var urlParams = new URLSearchParams(data);
        var myParam = urlParams.get('action');
        if(myParam == "custom_register")
        {
            $("#isLogged").data("logged", true);
        }
        console.log(myParam);
    });

    $("#liqpay").on("submit", function(){
        var isLogged = $("#isLogged").data("logged");
        if(!isLogged)
        {
            $('.payment_type').addClass('is_hidden');
            $('#register_desk').removeClass('is_hidden');
            return false;
        }
    });

    $('#register_desk form').on('submit', function () {
        setTimeout(()=>{
            if($('#register_desk').find('.success').length !== 0) {
                $("#liqpay input.button").click();
            }
        }, 1000)
    });

    if($("#successLiq").length > 0)
    {
        $("#liqSucName").text($("#successLiq").data('name'));
        $("#successBtn").click();
    }

    /* close fancybox */
    $('.close').on('click', function () {
        parent.$.fancybox.close();
    })
    $('#forgot_password_button').on('click', function () {
        parent.$.fancybox.close();
    })
    $(".close_thanks").on('click', function(){
        parent.$.fancybox.close();
    });
    $('.close_popup').on('click', function () {
        parent.$.fancybox.close();
    })


     // custom select
    $('select').selectric({
        disableOnMobile: false,
        nativeOnMobile: false,
        arrowButtonMarkup: '<span class="select_arrow"></span>'
    });
    $('select.wpcf7-form-control').each(function () {
        $(this).find('option').first().val('');
    });

    $('.regular-test__text-selects select').selectric({
        disableOnMobile: false,
        nativeOnMobile: false,
        arrowButtonMarkup: '<span class="select_arrow"></span>'
    });

    //  custom code
    $(function() {    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            var hh = $('header').outerHeight();
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - hh - 20
                }, 1000);
                return false;
            }
        }
    });
    });

    if($('#promo_title').length > 0){
        $('#promo_title').val($('h2').eq(0).text());
    }
    if($('#promo_deadline').length > 0){
        let dat = $('#date_end').text().split('-');
        let dat_d = dat[2].split(' ');
        let day = dat_d[0];
        let month = dat[1];
        let year = dat[0];

        $('#promo_deadline').val(day+'.'+month+'.'+year);
    }

    if($('#web_title').length > 0){
        $('#web_title').val($('h2').eq(0).text());
    }
    if($('#web_deadline').length > 0){
        $('#web_deadline').val($('.guest_info p').eq(0).text());
    }

    /* Regular Test */
    $('.regular-test__vocabulary-choice').on('click', function(){
        $('.regular-test__vocabulary-row').addClass('hide');
        $(this).parent().removeClass('hide');
        $(this).addClass('hide');
        $(this).next().removeClass('hide')
    })
    $('.regular-test__vocabulary-choice button').on('click', function(){
        let count = $(this).parent().attr('data-count');
        $(this).closest('.nanotest_answers').prev().find('.regular-test__sentence').append('<button class="sentence_button" data-order="'+count+'">'+$(this).text()+'<span></span></button>');
        $('.sentence_button[data-order*="'+(count-1)+'"]').addClass('completed');
    });

    $(this).on('click', '.regular-test__vocabulary-3 .sentence_button span', function() {
        let count = $(this).parent().attr('data-order');
        $(this).parent().remove();
        $('.regular-test__vocabulary-buttons[data-count="'+count+'"]').removeClass('hide');
        $('.sentence_button[data-order="'+(count-1)+'"]').removeClass('completed');
        $('.regular-test__vocabulary-buttons button').removeClass('active');
        $('.regular-test__vocabulary-buttons[data-count="'+(parseInt(count, 10)+1)+'"]').addClass('hide');

        if(count == 1) {
            $('.regular-test__vocabulary-row1, .regular-test__vocabulary-row').removeClass('hide');
        }
    })
    $('.regular-test__vocabulary-row3').on('click', function () {
        let answer = [],
            correct = [],
            count = $(this).parent().parent().parent().attr('data-slide'),
            points = $(this).parent().attr('data-points');

        $('.sentence_button').each(function () {
            answer.push($(this).text());
        })

        let answer_str = answer[0]+' '+answer[1]+' '+answer[2];


        $('.regular-test__vocabulary-row-correct p').each(function () {
            correct.push($(this).text())
        })

        let check = correct.filter(function (val) {
            return answer_str == val;
        })

        regular_tests['points'][count] = points * check.length;
        regular_tests['answers'][count] = answer_str;
    })

    $(this).on('click', '.regular-test__vocabulary-7 button', function(){
        let arr = $(this).text().split(' ... '),
            slide = $(this).closest('.swiper-slide').attr('data-nanotest-slide');

        $('.swiper-slide[data-nanotest-slide="'+slide+'"] .code').each(function () {
            let eq = $(this).index();
            $(this).children('code').text(arr[eq]);
            $(this).children('.vis_hid').text(arr[eq]);
        })
    })

    $('.grammar_type_input').each(function () {
        $(this).find('code').html('<span>'+$(this).find('code').text()+'</span><input type="text">');

        let srt_length = $(this).find('code').find('span').html().length;
        let input = $(this).find('code').find('input');

        input.css('min-width', srt_length*11.5+'px');
    })
    $(this).on('click', '.grammar_type_input code span', function () {
        $(this).addClass('is_focused');
        $(this).next().focus();
    })
    $(this).on('focusin', '.grammar_type_input input', function () {
        $(this).prev().addClass('is_focused');
        $(this).focus();
    })
    $(this).on('focusout', '.grammar_type_input input', function () {
        if($(this).val() == 0){
            $(this).prev().removeClass('is_focused');
        }
    })
    $(this).on('keyup', '.grammar_type_input input', function () {
        let correct = $(this).closest('.nanotest_question_text').next().children().text(),
            correct_str = $(this).closest('.nanotest_question_text').next().children().text().replace(/ /gi, ''),
            slide   = $(this).closest('.nanotest_question_text').next().attr('data-slide'),
            points  = $(this).closest('.nanotest_question_text').next().attr('data-point');

        regular_tests['answers'][slide] = $(this).val()+'('+correct+')';
        regular_tests['points'][slide] = $(this).val().replace(/ /gi, '') == correct_str ? parseInt(points) : 0;

        if($(this).val().length > 0) {
            $(this).closest('.grammar_type_input').find('.next_slide').removeClass('is_disabled');
        } else {
            $(this).closest('.grammar_type_input').find('.next_slide').addClass('is_disabled');
        }

        console.log(regular_tests['answers'])
        console.log(regular_tests['points'])

    })

    $('.grammar_type_mistake').each(function () {
        let arr = $(this).find('.nanotest_question_text p').text().split(' '),
            slide = $(this).attr('data-nanotest-slide');

        $(this).find('.nanotest_question_text p').remove();

        arr.forEach(function (item, i) {
            $('.swiper-slide[data-nanotest-slide="'+slide+'"] .nanotest_question_text').append('<button data-arr="'+i+'">'+arr[i]+'</button>')
        })
    })
    $(this).on('click', '.grammar_type_mistake .nanotest_question_text button', function () {
        let index = $(this).attr('data-arr'),
            slide = $(this).closest('.grammar_type_mistake').attr('data-nanotest-slide'),
            value = $(this).text(),
            answer = $('.swiper-slide[data-nanotest-slide="'+slide+'"] .nanotest_answers p').text(),
            points  = $(this).closest('.nanotest_question_text').next().attr('data-point');


        $('.grammar_type_mistake .nanotest_question_text button').removeClass('is_active');
        $(this).addClass('is_active');

        regular_tests['answers'][slide] = value + '('+answer+')';
        regular_tests['points'][slide] = value == answer ? parseInt(points) : 0;

        console.log(regular_tests['answers']);
        console.log(regular_tests['points']);

        $('.swiper-slide[data-nanotest-slide="'+slide+'"] .question_next button').removeClass('is_disabled');

        if(slide == $('.swiper-slide').length) {
            $('.finish_test').removeClass('is_disabled');
        }
    })


    let selects_val = [];
    $('.regular-test__text-selects code').each(function () {
        let slide = $(this).closest('.swiper-slide').attr('data-nanotest-slide');

        regular_tests['answers'][slide] = {};
        regular_tests['points'][slide] = {};



        $(this).html($(this).closest('.swiper-slide').find('select').html()).selectric({
            disableOnMobile: false,
            nativeOnMobile: false,
            arrowButtonMarkup: '<span class="select_arrow"></span>',
            onBeforeOpen: function(element) {
                let drop = $(element).parent().parent().parent().parent().find('.selectric-wrapper').length;

                for(let i = 1; i < drop; i ++) {
                    $(element).closest('.swiper-slide').find('.selectric-wrapper').eq(i - 1).attr('data-count', i)
                }

                let eq = $(element).closest('.selectric-wrapper').attr('data-count'),
                    index = $(element).parent().next().next().find('li.selected').attr('data-index'),
                    letter = $(element).children().eq(index).attr('data-letter'),
                    correct = $(element).children().eq(index).attr('data-select-order') == eq ? 1 : 0,
                    point = $(element).children().eq(index).attr('data-point'),
                    page_h = $(element).closest('.nanotest_test').outerHeight();

                if($(this).parent().parent().hasClass('is_active')) {
                    let select_wrap = $(this).parent().parent(),
                        count = select_wrap.attr('data-count'),
                        index = select_wrap.find('li.selected').attr('data-index');

                    select_wrap.find('.label').text('select');
                    select_wrap.find('.select_arrow').removeClass('is_active');
                    select_wrap.find('.selectric.is_changed').removeClass('is_changed');

                    select_wrap.closest('.swiper-slide').find('li[data-index="'+index+'"]').show().removeClass('selected').removeClass('highlighted');
                    select_wrap.removeClass('is_active');

                    $(element).prop('selectedIndex', 0).selectric('refresh');

                    console.log(selects_val);

                    if (selects_val.indexOf(index) > -1) {
                        selects_val.splice(selects_val.indexOf(index), 1);
                    }

                    console.log(selects_val)

                    selects_val.forEach(function (item) {
                        select_wrap.closest('.swiper-slide').find('li[data-index="'+item+'"]').hide();
                    })
                }

            },

            onChange: function (element) {

                let drop = $(element).parent().parent().parent().parent().find('.selectric-wrapper').length;

                for(let i = 1; i < drop; i ++) {
                    $(element).closest('.swiper-slide').find('.selectric-wrapper').eq(i - 1).attr('data-count', i)
                }

                let eq = $(element).closest('.selectric-wrapper').attr('data-count'),
                    index = $(element).parent().next().next().find('li.selected').attr('data-index'),
                    letter = $(element).children().eq(index).attr('data-letter'),
                    correct = $(element).children().eq(index).attr('data-select-order') == eq ? 1 : 0,
                    point = $(element).children().eq(index).attr('data-point'),
                    page_h = $(element).closest('.nanotest_test').outerHeight();

                // $(element).closest('.swiper-wrapper').attr('style', 'height: calc('+page_h+'px + 137px)')

                regular_tests['answers'][slide][eq] = letter+'('+$(element).children().eq(index).attr('data-select-order')+')';
                regular_tests['points'][slide][eq] = correct * point;

                console.log(regular_tests['answers']);
                console.log(regular_tests['points']);

                $(this).closest('.swiper-slide').find('li[data-index="'+index+'"]').hide();
                /////////
                $(element).parent().next().next().find('li.selected').hide();
                $(element).parent().next().find('.select_arrow').addClass('is_active');
                ////////
                $(this).parent().next().addClass('is_changed');

                $(this).closest('.swiper-slide').find('.next_slide').removeClass('is_disabled')

                $(element).closest('.selectric-wrapper').addClass('is_active');

                selects_val.push(index);

                if($(this).closest('.swiper-slide').find('.selectric-wrapper.is_active').length == $(this).closest('.swiper-slide').find('.selectric-wrapper').length - 1) {
                    $(this).closest('.swiper-slide').find('.next_slide').removeClass('is_disabled')
                } else {
                    $(this).closest('.swiper-slide').find('.next_slide').addClass('is_disabled')
                }
                /*$('.select_arrow.is_active').on('click', function (e) {
                    console.log($(this));

                    $(this).closest('.swiper-slide').find('li[data-index="'+index+'"]').show();
                    $(element).closest('.selectric-wrapper[data-count="'+eq+'"]').find('code').parent().next().removeClass('is_changed');
                    $(element).closest('.selectric-wrapper[data-count="'+eq+'"]').find('.label').text('select');
                    // $(this).toggleClass('is_active');

                })*/

                // $(this).closest('.swiper-slide').find('li').removeClass('selected');
            },
            onClose: function() {
                $('li.highlighted').removeClass('highlighted');
            },
        });
    });



    let index_a = 1;
    $('.regular-test__text-row-multiply .nanotest_question_block button').each(function () {
        let slide = $(this).closest('.swiper-slide').attr('data-nanotest-slide');

        regular_tests['answers'][slide] = {};
        regular_tests['points'][slide] = {};

        $(this).on('click', function () {

            $(this).toggleClass('active');

            let correct = $(this).attr('data-correct'),
                order = $(this).attr('data-order'),
                point = correct ? correct * $(this).attr('data-val') : 0,
                length = $('.swiper-slide[data-nanotest-slide="'+slide+'"] .nanotest_question_block button.active').length,
                count = length >= 2 ? 'is_disabled' : '';

            $('.swiper-slide[data-nanotest-slide="'+slide+'"] .nanotest_question_block button').not('.active').attr('class', count);

            if(length == 2) {
                $('.swiper-slide[data-nanotest-slide="'+slide+'"] .question_next .next_slide').removeClass('is_disabled');
            } else {
                $('.swiper-slide[data-nanotest-slide="'+slide+'"] .question_next .next_slide').addClass('is_disabled');
            }

        })
    })

    $('.regular-test__text-row-multiply .next_slide').on('click', function () {
        let length = $(this).closest('.regular-test__text-row-multiply').find('.nanotest_question_block .active').length,
            slide = $(this).closest('.swiper-slide').attr('data-nanotest-slide'),
            count = 1;


        $(this).closest('.regular-test__text-row-multiply').find('.nanotest_question_block .active').each(function () {
            let order = $(this).attr('data-order'),
                correct = $(this).attr('data-correct'),
                point = correct ? correct * $(this).attr('data-val') : 0;
            
            regular_tests['answers'][slide][count] = order+'('+parseInt(parseInt($(this).closest('.nanotest_question_block').find('button[data-correct=1]').eq(count-1).index(), 10)+1) +')';
            regular_tests['points'][slide][count++] = point;
        })

        console.log(regular_tests['answers'][slide]);
        console.log(regular_tests['points'][slide]);

    })

    $('.regular-test__create-row').each(function () {
        let regular_test_create_row = 1;
        $(this).find('.nanotest_question_row').find('button').on('click', function () {
            let slide = $(this).closest('.swiper-slide').attr('data-nanotest-slide'),
                position = $(this).attr('data-order'),
                parent = $(this).closest('.regular-test__create-row'),
                point = parseInt($(this).attr('data-point'), 10);

            parent.find('.regular-test__sentence').append('<button class="sentence_button" data-order="'+ regular_test_create_row++ +'" data-position="'+position+'"><i>'+$(this).text()+'</i><span></span></button>');
            $(this).hide();
            parent.find('.regular-test__sentence button').eq(-2).addClass('completed');


            setTimeout(()=>{
                if(parent.find('.regular-test__sentence').find('button').length == 4){
                    let answer = [];
                    let points = [];

                    parent.find('.regular-test__sentence button').each(function () {
                        answer.push($(this).text() + '('+$(this).attr('data-position')+')');
                        points.push($(this).attr('data-order') == $(this).attr('data-position') ? point : 0)
                    })

                    regular_tests['answers'][slide] = answer.join(' | ');
                    regular_tests['points'][slide] = points.includes(0) ? 0 : point;


                    console.log(regular_tests['answers'])
                    console.log(regular_tests['points'])
                    console.log(points)

                    return regular_test_create_row = 1;
                }
            }, 100)
        })

    })
    $(this).on('click', '.regular-test__create-row .sentence_button span', function () {

        let index = $(this).parent().attr('data-position'),
            parent = $(this).closest('.regular-test__create-row');

        console.log(index);

        $(this).parent().prev().removeClass('completed');
        $(this).parent().remove();
        parent.find('.nanotest_question_row').find('button[data-order="'+index+'"]').show().removeClass('active');
    })

    function bake_cookie(name, value) {
        console.log(value);
        console.log(JSON.stringify(value));
        let cookie2 = name + "=" + JSON.stringify(value) + ";" + 99999 + "; domain="+ window.location.host.toString() +"; path=/";
        let cookie = [name, "='", JSON.stringify(value), "'; 6048e2; domain=", window.location.host.toString(), "; path=/;"].join('');
        console.info( "cookie =", cookie2 );

        document.cookie = cookie2;
    }

    $('.finish_test').on('click', function (e) {

        let cat_tit = $('#regular-test__category-eng').text().split('|'),
            cat_tit_tr = $('#regular-test__category-trans').text().split('|'),
            cat_en = cat_tit[0],
            tit_en = cat_tit[cat_tit.length - 2],
            cat_ua = cat_tit_tr[0],
            tit_ua = cat_tit_tr[cat_tit_tr.length - 2],
            total_result = {
                cat_en,
                tit_en,
                cat_ua,
                tit_ua,
                regular_tests
            }

        bake_cookie(cat_en+'_'+tit_en, total_result);

        // e.preventDefault();
    })

    $('.tpl-regular-test-thanks #category_name').val($('#category_name_get').text().replace('Array', '').replace('(', '').replace(')', ''));
    $('.tpl-regular-test-thanks #test_title').val($('#test_title_get').text().replace('Array', '').replace('(', '').replace(')', ''));
    $('.tpl-regular-test-thanks #regular_total_points').val($('#regular_total_points_get').text().replace('Array', '').replace('(', '').replace(')', ''));
    $('.tpl-regular-test-thanks #regular_total_res').val($('#regular_total_res_get').text().replace('Array', '').replace('(', '').replace(')', ''));
    $('.tpl-regular-test-thanks #test_total_count').val($('.regular-test__result-row').eq(0).find('b').text());
    $('.tpl-regular-test-thanks #test_total_correct').val($('.regular-test__result-row').eq(1).find('b').text());
    $('.tpl-regular-test-thanks #test_total_mistakes').val($('.regular-test__result-row').eq(2).find('b').text());
    $('.tpl-regular-test-thanks #test_total_percent_correct').val($('.regular-test__result-row').eq(3).find('b').text());

    $('.tpl-regular-test-thanks input[type="email"]').on('keyup', function () {
        $('#nano_user').text($(this).val());
    })

    $('#test__title').selectric({
        onInit: function () {
            var pageURL = window.location.href;
            var lastURLSegment = pageURL.split('/');
            lastURLSegment = lastURLSegment[lastURLSegment.length - 2];
            let index = $('option[data-title="'+lastURLSegment+'"]').index();
            $('#test__title').prop('selectedIndex', index).selectric('refresh');
            $('.regular-test__filtering-mob').addClass('is_loaded');
            // $('#test__title').val(lastURLSegment[lastURLSegment.length - 2]).selectric('refresh');
        },
        onChange: function (el) {
            // console.log($(el).val())
            window.location.replace('https://'+window.location.hostname+'/tests/'+$(el).val());
        }
    });
    $('.regular-test__filtering-select').selectric({
        onChange: function (element) {
        }
    });


    let regular_test__mob_link;
    $('.test__category').selectric({
        onInit: function() {
            $('.regular-test__mob-post').eq(0).removeClass('hide');
        },
        onChange: function (el) {
            $(this).closest('.selectric-regular-test__filtering-select').addClass('is_filled');

            let index = $(el).prop('selectedIndex');

            if($('.regular-test__mob-themes').length > 0) {
                $('.regular-test__mob-themes').removeClass('is_disabled');
                $('.regular-test__mob-theme').addClass('hide');
                $('.regular-test__mob-theme').eq(index - 1).removeClass('hide');
            } else {
                $('.regular-test__mob-posts').removeClass('is_disabled');
                $('.regular-test__mob-post').addClass('hide');
                $('.regular-test__mob-post').eq(index - 1).removeClass('hide');
            }
        }
    });
    $('.test__theme').selectric({
        onInit: function() {
            $('.regular-test__mob-theme').eq(0).removeClass('hide');
            $('.test__theme').selectric('refresh');
        },
        onChange: function (el) {
            $(this).closest('.selectric-regular-test__filtering-select').addClass('is_filled');
            $('.regular-test__mob-posts').removeClass('is_disabled');
            $('.regular-test__mob-post').addClass('hide');

            let name = $(el).val();
            console.log(name);
            $('.regular-test__mob-post[data-cat="'+name+'"]').removeClass('hide');
        }
    })
    $('.test__post').selectric({
        onChange: function (el) {
            if($('.regular-test__mob-themes').length > 0) {
                // $('.test__theme').selectric('refresh');
            }

            $(this).closest('.selectric-regular-test__filtering-select').addClass('is_filled');
            $('.regular-test__filtering-submit').removeClass('is_disabled');
            console.log($(el).val());



            return regular_test__mob_link = $(el).val()
        }
    })

    $('.regular-test__filtering-submit').on('click', function () {
        window.location = regular_test__mob_link;
    })

});

$(window).on('load', function() {
    'use strict';

    var test_res = [];

    if($('.tpl-course-detail').length > 0){
        $('.tpl-course-detail .tab-content .item.lessons_27').click();
    }
    $('.course_step').hover(function(){
        let index = $(this).parent().index();
        let parent = $(this).parent();
        parent.toggleClass('active');
        $('.course_column').eq(index).toggleClass('active');
        for(var i = 0; i < index; i++) {
            $('.course_step_wrapper').eq(i).toggleClass('prev');
        }
    })
    $('.course_column').hover(function(){
        let index = $(this).index();
        let parent = $(this).parent();
        // $('.course_column').removeClass('active');
        $(this).toggleClass('active');
        $('.course_step_wrapper').eq(index).toggleClass('active');
        for(var i = 0; i < index; i++) {
            $('.course_step_wrapper').eq(i).toggleClass('prev');
        }
    })

    // img svg
    $('img.img_svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            var $svg = jQuery(data).find('svg');

            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }


            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            $svg = $svg.removeAttr('xmlns:a');

            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }

            $img.replaceWith($svg);

        }, 'xml');

    });

    // swiper
    var nanoTest = new Swiper('.nanotest_slider', {
        spaceBetween: 30,
        effect: 'fade',
        allowTouchMove: false,
        lazy: true,
        autoHeight: true
    });

    $('.tpl-nanotest .next_slide').on('click', function () {
        nanoTest.slideNext();
        $('html').animate({ scrollTop: 0}, 500);
    });

    let nanotest_dot = 0;
    nanoTest.on('slideChange', function () {
        let attr = nanoTest.slides.eq(nanoTest.activeIndex).attr('data-nanotest-slide');
        let count = nanoTest.slides.length - 2;

        $('.swiper-slide[data-nanotest-slide="'+attr+'"] .current_index').text(attr);
        $('.swiper-slide[data-nanotest-slide="'+attr+'"] .total_index').text(count);

        if(attr == count) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next button').eq(0).hide();
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next .hide').removeClass('hide');
        }

        $('.tpl-nanotest .logo').addClass('is_disabled');

        $('#nanotest_progress .nanotest_line[data-line="'+(attr - 1)+'"]').addClass('active');
        $('#nanotest_progress .nanotest_dot[data-line="'+(attr)+'"]').addClass('active');
        $('#nanotest_progress .nanotest_progress_text[data-line="'+(attr - 1)+'"]').addClass('active');

        if( attr >= 1 ) {
            $('#nanotest_progress').addClass('active');
            // $('#nanotest_progress .nanotest_dot').eq(nanotest_dot).addClass('active');
            // nanotest_dot++;
        } else {
            $('#nanotest_progress').removeClass('active');
        }

        if($('.swiper-slide').hasClass('vocabulary_type_2')){

        }

        if( $('.swiper-slide').hasClass('grammar_type_4') || $('.swiper-slide').hasClass('grammar_type_5') || $('.swiper-slide').hasClass('listening_type_8')) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers').each(function () {
                let largest_4_arr = [];
                function longestString(){
                    var longest = largest_4_arr.reduce(function(a, b) {
                        return a.length > b.length ? a : b
                    }, '');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] code').wrap('<span class="code"></span>');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+longest+'</span><code></code>');
                }
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers button').each(function () {
                    largest_4_arr.push( $(this).text());
                });
                longestString();
            })
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_answers button').on('click', function () {
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+$(this).text()+'</span><code>'+$(this).text()+'</code>');
            })
        }
        if($('.swiper-slide').hasClass('listening_type')) {
            // console.log('video');
        }
    });

    // nanotest progress bar
    var progress_line = 1;
    $('#vocabulaty').each(function () {
        let voc = $('.vocabulary_type').length;
        let voc_t1 = $('.vocabulary_type_1').length;
        let voc_t2 = $('.vocabulary_type_2').length;
        let voc_t3 = $('.vocabulary_type_3').length;

        for ( var i = 0; i < voc; i++ ) {
            if( i % 9 == 0 && i != voc && i != 0 ) {
                $(this).append('<span class="nanotest_dot" data-line="'+progress_line+'"></span>');
            }
            $(this).append('<span class="nanotest_line" data-line="'+progress_line+'"></span>');
            test_res.push('{'+progress_line+++': -}');
            // $('#result').append('<p id="quest_'+progress_line+'"><strong>'+progress_line+++' </strong><span>-</span></p>');
        }
    })

    $('#grammar').each(function () {
        let voc = $('.vocabulary_type').length;
        let gram = $('.grammar_type').length;
        let gram_t4 = $('.grammar_type_4').length;
        let gram_t5 = $('.grammar_type_5').length;
        let gram_t6 = $('.grammar_type_6').length;

        for ( var i = 0; i < gram; i++ ) {
            if( i % 9 == 0 && i != gram && i != 0 ) {
                $(this).append('<span class="nanotest_dot" data-line="'+progress_line+'"></span>');
            }
            $(this).append('<span class="nanotest_line" data-line="'+progress_line+'"></span>');
            test_res.push('{'+progress_line+++': -}');
        }

        $('#grammar .nanotest_progress_text').attr('data-line', voc)
    })

    $('#listening').each(function () {
        let voc = $('.vocabulary_type').length;
        let gram = $('.grammar_type').length;
        let list = $('.listening_type').length;
        let list_t7 = $('.listening_type_7').length;
        let list_t8 = $('.listening_type_8').length;
        let list_t9 = $('.listening_type_9').length;

        for ( var i = 0; i < list; i++ ) {
            if( i % 3 == 0 && i != list && i != 0 ) {
                $(this).append('<span class="nanotest_dot" data-line="'+progress_line+'"></span>');
            }
            $(this).append('<span class="nanotest_line" data-line="'+progress_line+'"></span>');
            test_res.push('{'+progress_line+++': -}');
        }

        $('#listening .nanotest_progress_text').attr('data-line', voc+gram)
    })

    let points = 0,
        voc_p = 0,
        gram_p = 0,
        lis_p = 0;

    // test type 0
    $('.nanotest_slide_main .nanotest_goal').on('click', function () {
        let id = $(this).attr('data-for'),
            goal = $(this).attr('data-goal');
        $('.nanotest_slide_main .nanotest_goal input[type="radio"]').removeAttr('checked');
        $('.nanotest_slide_main .nanotest_goal').removeClass('active');
        $(this).addClass('active');
        $('#'+id).attr('checked','true');
        $('.nanotest_slide_main .button.is_disabled').removeClass('is_disabled');

        $('#test_goal').text($(this).find('strong').text());

        $(".nanotest_slide_main").animate({
            scrollTop: 1000
        }, 2000);

        $('#result').attr('data-goal', goal);

        $('html').animate({ scrollTop: $(window).outerHeight()}, 500);
    });

    // test type 1
    $('.nanotest_answers button').on('click', function () {
        let slide = $(this).closest('.swiper-slide');
        let slide_number = slide.attr('data-nanotest-slide');
        $(this).parent().find('button').removeClass('active');
        $(this).addClass('active');

        $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] .next_slide').removeClass('is_disabled');

        test_res[slide_number-1] = '{'+slide_number+':'+$(this).text()+'}';

        if($(this).attr('data-correct')) {
            let current = parseInt($(this).attr('data-val'), 10);
            $('#result').attr('data-point', points + current);
            points = points + current;

            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('vocabulary_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#voc_res').text());
                $('#voc_res').text(prev + current_p);
                voc_p = voc_p + current_p;
            }

            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('grammar_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#gram_res').text());
                $('#gram_res').text(prev + current_p);
                gram_p = gram_p + current_p;
            }
            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('listening_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#lis_res').text());
                $('#lis_res').text(prev + current_p);
                lis_p = lis_p + current_p;
            }
        }

    })

    // test type 2
    let arr_ans = [];
    $('.nanotest_question_select').selectric({
        onChange: function (element) {
            let slide = $(this).closest('.swiper-slide');
            let slide_number = slide.attr('data-nanotest-slide');
            let answers = slide.attr('data-answers');
            let total = slide.attr('data-total');
            let col = $(element).closest('.nanotest_question_col').attr('data-col');

            if(!$(this).parent().next().hasClass('is_changed')) {
                slide.attr('data-answers', ++answers);
            }

            if($(this).val() == $(this).parent().parent().prev().attr('data-correct')) {
                slide.attr('data-total', ++total);
            }

            $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer'+col, $(element).val());

            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer1').length > 0 && $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer2').length > 0 && $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer3').length > 0 && $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer1') !==  $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer2') && $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer2') !== $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer3') && $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer1') !== $('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-answer3')){
                $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] .next_slide').removeClass('is_disabled');
            } else {
                $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] .next_slide').addClass('is_disabled');
            }

            $(this).parent().next().addClass('is_changed');

            regular_tests['points'][slide_number] = 0;

            if(answers == 3 && total == 3) {
                let current = parseInt($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').attr('data-points'));
                $('#result').attr('data-point', points + current);
                points = points + current;

                regular_tests['points'][slide_number] = current;

                if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('vocabulary_type')) {
                    let current_p = parseInt(1, 10)
                    let prev = parseInt($('#voc_res').text());
                    $('#voc_res').text(prev + current_p);
                    voc_p = voc_p + current_p;
                }

                if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('grammar_type')) {
                    let current_p = parseInt(1, 10)
                    let prev = parseInt($('#gram_res').text());
                    $('#gram_res').text(prev + current_p);
                    gram_p = gram_p + current_p;
                }
                if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('listening_type')) {
                    let current_p = parseInt(1, 10)
                    let prev = parseInt($('#lis_res').text());
                    $('#lis_res').text(prev + current_p);
                    lis_p = lis_p + current_p;
                }
            }

            if(arr_ans.includes($(this).prop('selectedIndex'))) {
                for (var i=0; i<arr_ans.length; i++) {
                    if (arr_ans[i] === $(this).prop('selectedIndex') && i !== col - 1) {
                        let item = i;
                        $('.nanotest_question_col[data-col="'+ ++item+'"] select').prop('selectedIndex', 0).selectric('refresh');
                        $('.nanotest_question_col[data-col="'+item+'"] .is_changed').removeClass('is_changed');
                    }
                }
            }
            arr_ans[col - 1] = $(this).prop('selectedIndex');
        }
    });

    $('.vocabulary_type_2 .next_slide').on('click', function () {
        let slide = $(this).closest('.swiper-slide');
        let slide_number = slide.attr('data-nanotest-slide');
        let an1 = $(this).closest('.swiper-slide').attr('data-answer1');
        let an2 = $(this).closest('.swiper-slide').attr('data-answer2');
        let an3 = $(this).closest('.swiper-slide').attr('data-answer3');
        test_res[slide_number-1] = '{'+slide_number+':{1:'+an1+',2:'+an2+',3:'+an3+'}}';

        let current = parseInt($('.swiper-slide[data-nanotest-slide="'+slide+'"]').attr('data-points'));

        regular_tests['answers'][slide_number] = '1:'+an1+', 2:'+an2+', 3:'+an3;

        console.log( regular_tests['points']);
        console.log( regular_tests['answers']);
    });

    // test type 3
    $('.hide .selectric-nanotest_question_select_3:nth-of-type(1)').each(function () {
        $(this).appendTo($(this).parent().prev().find('code').eq(0));
    });
    $('.hide .selectric-nanotest_question_select_3:nth-of-type(1)').each(function () {
        $(this).appendTo($(this).parent().prev().find('code').eq(1));
    });
    $('.nanotest_question_select_3').selectric({
        onChange: function () {
            let slide = $(this).closest('.swiper-slide');
            let slide_number = slide.attr('data-nanotest-slide');
            let answers = slide.attr('data-answers');

            if(!$(this).parent().next().hasClass('is_changed')) {
                slide.attr('data-answers', ++answers);
            }
            if(answers == 2) {
                $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] .nanotest_question_row_bg').addClass('active');
            }

            $(this).parent().next().addClass('is_changed').removeClass('select_error');
        }
    });
    $('.nanotest_question_row_bg').on('click', function () {
        $(this).parent().prev().find('.selectric:not(.is_changed)').addClass('select_error');
    });
    $('.nanotest_question_item').on('click', function () {
        $(this).parent().find('.nanotest_question_item').removeClass('active');
        $(this).addClass('active');

        let slide = $(this).closest('.swiper-slide');
        let slide_number = slide.attr('data-nanotest-slide');
        $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] .next_slide').removeClass('is_disabled');

        test_res[slide_number-1] = '{'+slide_number+':'+$(this).children().text()+'}';

        if($(this).attr('data-select-correct')) {
            let current = parseInt($(this).attr('data-val'), 10);
            $('#result').attr('data-point', points + current);
            points = points + current;

            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('vocabulary_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#voc_res').text());
                $('#voc_res').text(prev + current_p);
                voc_p = voc_p + current_p;
            }

            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('grammar_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#gram_res').text());
                $('#gram_res').text(prev + current_p);
                gram_p = gram_p + current_p;
            }
            if($('.swiper-slide[data-nanotest-slide="'+slide_number+'"]').hasClass('listening_type')) {
                let current_p = parseInt(1, 10)
                let prev = parseInt($('#lis_res').text());
                $('#lis_res').text(prev + current_p);
                lis_p = lis_p + current_p;
            }

        }
    })

    // test listening_type
    $('.button_wrap').on('click', function () {
        if($(this).hasClass('is_disabled')){
            $(this).children('.button_tooltip').addClass('visible');
            setTimeout(function () {
                $('.visible').removeClass('visible');
            }, 3000);
        } else {
            let slide = $(this).closest('.swiper-slide');
            let slide_number = slide.attr('data-nanotest-slide');
            $('.swiper-slide[data-nanotest-slide="'+slide_number+'"] button').removeClass('active');
            $(this).children().addClass('active');
        }
    });
    $('.listening_video').on('click',function () {
        let video = $(this).children('video');
        let play_btn = $(this).children('.play_video');
        let btn = $(this).parent().next().children('.button_wrap');
        if(video.get(0).paused){
            video.get(0).play();
            play_btn.addClass('invisible');
        } else {
            video.get(0).pause();
            play_btn.removeClass('invisible');
        }
        video.on('ended', function () {
            btn.removeClass('is_disabled');
            btn.children('.button_tooltip').remove();
            play_btn.removeClass('invisible');
        })
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }


    $('.nanotest_slide_last .button').on('click', function () {
        $('#voc_res_field').val($('#voc_res').text());
        $('#gram_res_field').val($('#gram_res').text());
        $('#lis_res_field').val($('#lis_res').text());

        $('#voc_total_field').val($('.vocabulary_type').length);
        $('#gram_total_field').val($('.grammar_type').length);
        $('#lis_total_field').val($('.listening_type').length);

        $('#total_cor_field').val(lis_p+gram_p+voc_p);
        $('#total_quest_field').val($('.vocabulary_type').length + $('.grammar_type').length + $('.listening_type').length);

        $('#nano_user_mail').text($('.nanotest_slide_last input[name="email"]').val());

        $('#total_res').val(test_res);

        $('#te_lv').val($('#test_goal').text());
        $('#test').html($('#rec[data-point="2"]').html());
        points = $('#result').attr('data-point');
        console.log(points);
        if(points <= 54){
            $('#res_te').val($('#resTE[data-point="0"]').text());
            $('#res_tx').val($('#resTX[data-point="0"] p').text());
        } else if (points > 54 && points <= 108) {
            $('#res_te').val($('#resTE[data-point="1"]').text());
            $('#res_tx').val($('#resTX[data-point="1"] p').text());
        } else {
            $('#res_te').val($('#resTE[data-point="2"]').text());
            $('#res_tx').val($('#resTX[data-point="2"] p').text());
        }

        let goal =  $('#result').attr('data-goal');
        if(goal == 0) {
            $('#rec_te').val($('#rec[data-point="0"]').html())
            $('#price_url').val('https://nanoenglish.com/prices/');
        } else if (goal == 1) {
            $('#rec_te').val($('#rec[data-point="1"]').html())
            $('#price_url').val('https://nanoenglish.com/prices/#business');
        } else {
            $('#rec_te').val($('#rec[data-point="2"]').html())
            $('#price_url').val('https://nanoenglish.com/prices/#ielts');
        }


    });

    document.addEventListener( 'wpcf7mailsent', function( event ) {
        if ( '1246' == event.detail.contactFormId ) {
            $.fancybox.open({
                src: '#thanks_nano'
            });
            setTimeout(function () {
                window.location.replace("/");
            }, 4000)
        } else if ( '1247' == event.detail.contactFormId ) {
            $.fancybox.open({
                src: '#thanks_nano'
            });
            setTimeout(function () {
                window.location.replace("/");
            }, 4000)
        } else if ( '2162' == event.detail.contactFormId ) {
            $.fancybox.open({
                src: '#thanks_nano'
            });
            setTimeout(function () {
                window.location.replace("/tests/");
            }, 4000)
        } else if ( '310' == event.detail.contactFormId ) {
            $.fancybox.open({
                src: '#thanks_for_review'
            });
        } else {
            $.fancybox.close();
            $.fancybox.open({
                src: '#thanks_mail'
            });
        }
    }, false );



    //  fluid video (iframe)
    $('.content iframe').each(function(i) {
        var t = $(this),
            p = t.parent();
        if (p.is('p') && !p.hasClass('full_frame')) {
            p.addClass('full_frame');
        }
    });
    $('.wp-video').each(function() {
        $('.mejs-video .mejs-inner', this).addClass('full_frame');
    });

    if($('#entry_video').length > 0) {
        $('#entry_video').parent().css('height','auto!important')
    }

    if(window.location.hash) {
        if(window.location.hash == '#business') {
            $('li[data-tab="businessEnglish"]').click();
        } else if (window.location.hash == '#ielts') {
            $('li[data-tab="IELTS"]').click();
        }
    }

    $.fancybox.defaults.touch = false;
    $.fancybox.defaults.arrows = false;



    /* Sticky Ads */
    var $sticky = $('.sidebar__ads');
    var $stickyrStopper = $('footer');
    if (!!$sticky.offset()) { // make sure ".sticky" element exists

        var generalSidebarHeight = $sticky.innerHeight();
        var stickyTop = $sticky.offset().top;
        var stickOffset = 131;
        var stickyStopperPosition = $stickyrStopper.offset().top;
        var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
        var diff = stopPoint + stickOffset - 200;

        $(window).scroll(function(){ // scroll event
            if ($(window).width() >= 1025) {
                var windowTop = $(window).scrollTop(); // returns number

                if (stopPoint < windowTop) {
                    $sticky.css({ position: 'absolute', top: diff });
                } else if (stickyTop < windowTop+stickOffset) {
                    $sticky.css({ position: 'fixed', top: stickOffset });
                } else {
                    $sticky.css({position: 'absolute', top: 'initial'});
                }
            }
        });

    }

    /* Phone Mask */
    $('input[type="tel"]').mask('+38 (000) 000 – 00 – 00');
    $('input.phone_field').mask('+38 (000) 000 – 00 – 00');

    /* Temporary code */
    $('section.reviews').prev().css('background-color', '#f9f9ff');

    /* Tests Tabber */
    $('.regular-test__tests-cat a').on('click', function () {
        let className = $(this).attr('data-cat');

        $('.regular-test__tests-cat a').removeClass('is_active')
        $(this).addClass('is_active');

        $('.tests--default').addClass('hide');
        $('.tests--posts').removeClass('is_active');
        $('.tests--posts[data-term="'+className+'"]').addClass('is_active');
    })
    $('.regular-test__tests-drop-items a').on('click', function () {
        let className = $(this).attr('data-cat');

        $('.regular-test__tests-drop-items a').removeClass('is_active')
        $(this).addClass('is_active');

        $('.tests--default').addClass('hide');
        $('.tests--posts').removeClass('is_active');
        $('.tests--posts[data-term="'+className+'"]').addClass('is_active');
    })
    $('.regular-test__tests-drop-top').on('click', function() {
        $('.regular-test__tests-drop-top').removeClass('is_active');
        $('.regular-test__tests-drop-items').slideUp(0)

        $(this).addClass('is_active');
        $(this).next().slideDown(0);
    })

    /* Regular Tests */
    var regularTest = new Swiper('.regular-test__slider', {
        spaceBetween: 30,
        effect: 'fade',
        allowTouchMove: false,
        lazy: true,
        autoHeight: true,
        init: false
    });

    regularTest.on('init', function () {

        $('.logo').addClass('is_disabled');

        let attr = regularTest.activeIndex + 1;
        let count = $('.regular-test__text').length > 0 ? regularTest.slides.length - 1 : regularTest.slides.length;

        $('.swiper-slide:nth-of-type('+attr+') .current_index').text(attr);
        $('.swiper-slide:nth-of-type('+attr+') .total_index').text(count);

        for (let i = 0; i < count; i++) {
            $('#regulartest_progress-lines').append('<span class="nanotest_line" data-line="'+i+'"></span>');
        }

        if( $('.swiper-slide').hasClass('grammar_type_4') || $('.swiper-slide').hasClass('grammar_type_5') || $('.swiper-slide').hasClass('listening_type_8')) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers').each(function () {
                let largest_4_arr = [];
                function longestString(){
                    var longest = largest_4_arr.reduce(function(a, b) {
                        return a.length > b.length ? a : b
                    }, '');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] code').wrap('<span class="code"></span>');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+longest+'</span><code></code>');
                }
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers button').each(function () {
                    largest_4_arr.push( $(this).text());
                });
                longestString();
            })
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_answers button').on('click', function () {
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+$(this).text()+'</span><code>'+$(this).text()+'</code>');
            })
        }

        if($('.swiper-slide').hasClass('regular-test__text-selects') && $('.regular-test__text-selects .nanotest_question_text').outerHeight() >= 450){
            $('.swiper-wrapper').attr('style', 'padding-top: 100px');
        }
    })

    regularTest.init();

    // regular_tests['3'] = '3(1)';

    localStorage.setItem($('#regular-test__category-eng').html(), JSON.stringify(regular_tests));
    // console.log( localStorage.getItem($('#regular-test__category-eng').html()) ); // 1

    // GET VALUE FROM LOCALSTORAGE!!!!
    /*var retrievedObject = localStorage.getItem($('#regular-test__category-eng').html());
    console.log('retrievedObject: ', JSON.parse(retrievedObject)[0]);*/


    $('.regular-test__slider .nanotest_answers button').on('click', function () {
        let slide = parseInt(regularTest.slides.eq(regularTest.activeIndex).attr('data-nanotest-slide'),10),
            point = $(this).attr('data-val'),
            correct = $(this).attr('data-correct') ? 1 : 0,
            choosed = $(this).index()+1,
            is_correct = $(this).parent().find($('button[data-correct="1"]')).index()+1;

        regular_tests['points'][slide] = point * correct;
        regular_tests['answers'][slide] = choosed+'('+is_correct+')';
        console.log(regular_tests['answers']);
        console.log(regular_tests['points']);
    });

    $('.regular-test__slider .next_slide').on('click', function () {
        regularTest.slideNext();
        $('html').animate({ scrollTop: 0}, 500);
    });

    let regulartest_dot = 0;
    regularTest.on('slideChange', function () {
        let attr = regularTest.slides.eq(regularTest.activeIndex).attr('data-nanotest-slide');
        let count = regularTest.slides.length;

        console.log(attr);
        console.log(count);

        if($('.swiper-slide').hasClass('regular-test__text')){
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .current_index').text(attr);
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .total_index').text(count-1);
        } else {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .current_index').text(attr);
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .total_index').text(count);
        }

        if($('.swiper-slide').hasClass('regular-test__text-selects')){
            $('.swiper-wrapper').attr('style', 'padding-top: 100px');
        }

        if(attr == count) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next button').eq(0).hide();
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next .hide').removeClass('hide');
        }

        if(attr == count-1 && $('.swiper-slide').hasClass('regular-test__text')) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next button').eq(0).hide();
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .question_next .hide').removeClass('hide');
        }

        $('#regulartest_progress-lines .nanotest_line[data-line="'+(attr - 2)+'"]').addClass('active');

        if( attr >= 1 ) {
            $('#nanotest_progress').addClass('active');
            // $('#nanotest_progress .nanotest_dot').eq(nanotest_dot).addClass('active');
            // nanotest_dot++;
        } else {
            $('#nanotest_progress').removeClass('active');
        }



        if( $('.swiper-slide').hasClass('grammar_type_4') || $('.swiper-slide').hasClass('grammar_type_5') || $('.swiper-slide').hasClass('listening_type_8')) {
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers').each(function () {
                let largest_4_arr = [];
                function longestString(){
                    var longest = largest_4_arr.reduce(function(a, b) {
                        return a.length > b.length ? a : b
                    }, '');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] code').wrap('<span class="code"></span>');
                    $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+longest+'</span><code></code>');
                }
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_type4 .nanotest_answers button').each(function () {
                    largest_4_arr.push( $(this).text());
                });
                longestString();
            })
            $('.swiper-slide[data-nanotest-slide="'+attr+'"] .nanotest_answers button').on('click', function () {
                $('.swiper-slide[data-nanotest-slide="'+attr+'"] .code').html('<span class="vis_hid">'+$(this).text()+'</span><code>'+$(this).text()+'</code>');
            })
        }
        if($('.swiper-slide').hasClass('listening_type')) {
            // console.log('video');
        }

        if($('.swiper-slide').hasClass('regular-test__create-row-with-variants')) {
            $('.regular-test__vocabulary-row').removeClass('hide');
        }
    });

    if($('.regular-test__text').length > 0) {
        if ($(window).width() >= 1025) {
            let height =  $('.regular-test__text-selects .nanotest_test').outerHeight();
            $('.swiper-wrapper').attr('style', 'height: calc('+height+'px - 38px)');
        } else {
            $('.swiper-wrapper').attr('style', 'height: auto');
        }
    }

    if($(window).height() <= 700 && $(window).width() >= 1025 && $('.container.is_smaller').outerHeight() >= 300) {
        $('.single-regular_tests .swiper-wrapper' ).css('padding-top', '100px');
        $('.regular-test__text').parent().css('padding-top', '0');
    }

    if($('.regular-test__text-row-boolean .nanotest_answers button').length > 2) {
        $('.regular-test__text-row-boolean .nanotest_question_row').attr('style', 'width: calc(100% + 16px)')
    }

});

$(window).resizeEnd(function() {
    'use strict';

});
