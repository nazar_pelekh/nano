$(document).ready(function () {
    var width = document.body.clientWidth;

    $('[data-fancybox]').fancybox();


    $('.review_text p').each(function (index, element) {
        if ($(this).height() > 100) {
            $(this).toggleClass('hide_txt');
            $(this).next().show();
        }
    });

    $('.review_text .full_review').each(function (index, element) {
        $(this).click(function () {
            $(this).toggleClass('active');
            $(this).prev().toggleClass();
        });
    });

    $(this).on('click', '.faq_question', function () {
        var question = $(this),
            answer = question.next();
        if (!question.hasClass('active')) {
            $('.faq_question').removeClass('active');
            question.addClass('active');
            $('.faq_answer').slideUp(400);
            answer.slideDown(400);
        } else {
            $('.faq_question').removeClass('active').next().slideUp(400);
        }
        return false;
    });

    $(this).on('click', '.open_all_questions', function () {
        $(this).toggleClass('opened');
        var par = $(this).closest('.faq_group');
        par.find('.faq_answer').slideToggle(400);
        par.find('.faq_question').toggleClass('active');
    });

    if (width <= 1024) {
        var t_p = $('.teacher_info p');
        $('.teacher a').on('click', function () {
            if (t_p.height() > 130) {
                $('.full_text').show();
                t_p.toggleClass('hide_text');
            }
        });
    }

    $('.full_text').click(function () {
        $(this).toggleClass('active');
        $(this).prev().toggleClass();
    });


    $('.rev_f_l_name input').change(function () {
        $('#acf-_post_title').val($(this).val());
    });
    $('.rev_video_link input').change(function () {
        $('#acf-field_5e021292e2c2c').val($(this).val());
    });
    $('.rev_fac_link input').change(function () {
        $('#acf-field_5e0212d1e2c2d').val($(this).val());
    });
    $('.rev_inst_link input').change(function () {
        $('#acf-field_5e0212f6e2c2e').val($(this).val());
    });
    $('.rev_message textarea').change(function () {
        $('#acf-field_5e021309e2c2f').val($(this).val());
    });

    $(".rev_goal select").change(function () {
        $('#acf-field_5e0211a8e2c2b').val($(this).find("option:selected").text());
    });

    $('.leave_review .post_review').click(function () {
        $('#acf-form .acf-button').click();
    });

    $('.leave_review .rev_upload_img').click(function () {
        $('#acf-field_5e020f6379085').click();
    });

    $('input[name="acf[field_5e020f6379085]"]').change(function () {
        if ($(this).val().length > 0) {
            $('.leave_review .rev_upload_img').toggleClass('added');
        }
    });

    $("a[href^='#']").click(function (e) {
        e.preventDefault();
        var position = $($(this).attr("href")).offset().top;

        $("body, html").animate({
            scrollTop: position
        });
    });

    $('.teacher_info a.button').click(function () {
        $.fancybox.close();
    });

    $('.join_teacher_form .fileBox').click(function () {
        $('.join_teacher_form .file-cv input').click();
    });

    $('.join_teacher_form .file-cv input').change(function () {
        $('.join_teacher_form .file_format').text(this.files[0].name);
    });

    //Sales countdown
    var goalDay = $('.sales_counter').data('counter');

    var timerId = 0;
    timerId = setInterval(function() {
        var t = Date.parse(goalDay) - Date.parse(new Date());
        if (t < 0) {
            $(".sales_counter .days").text("0");
            $(".sales_counter .hours").text("0");
            $(".sales_counter .minutes").text("0");
            $(".sales_counter .seconds").text("0");
            clearInterval(timerId);
        } else {
            var seconds = Math.floor((t / 1000) % 60);
            var minutes = Math.floor((t / 1000 / 60) % 60);
            var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            var days = Math.floor(t / (1000 * 60 * 60 * 24));
            $(".sales_counter .days").text(days);
            $(".sales_counter .hours").text(hours);
            $(".sales_counter .minutes").text(minutes);
            $(".sales_counter .seconds").text(seconds);
        }
    }, 1000); // repeat forever, polling every second

});


$(window).on('load', function () {
    var reviewSwiper = new Swiper('.reviews .swiper-container', {
        centeredSlides: true,
        slidesPerView: 'auto',
        loop: true,
        spaceBetween: 10,
        navigation: {
            nextEl: '.rev_slider_nav.nav_right',
            prevEl: '.rev_slider_nav.nav_left',
        },
        breakpoints: {
            1025: {
                spaceBetween: 125
            }
        }
    });

    var swiper = new Swiper('.teachers_slider', {
        spaceBetween: 0,
        navigation: {
            nextEl: '.t_slider_nav.nav_right',
            prevEl: '.t_slider_nav.nav_left',
        },
        breakpoints: {
            768: {
                slidesPerView: 4,
                slidesPerColumn: 3,
            },
            480: {
                slidesPerView: 3,
                slidesPerColumn: 3
            },
            320: {
                slidesPerView: 2,
                slidesPerColumn: 2
            }
        }
    });

});


