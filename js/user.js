// NEW selector
jQuery.expr[':'].Contains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

$(document).ready(function () {

    $("#ajaxlogin").on('submit', function (e) {
        // var $form = $(this);
        var login = $("#user_login").val();
        var password = $("#user_pass").val();
        var security = $("#ajaxlogin #security").val();
        $.ajax({
            type: 'POST',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin',
                'username': login,
                'password': password,
                'security': security,
            },
            dataType: "json",
            success: function (data) {
                //data = JSON.parse(data);
                if (data.loggedin == true) {
                    window.location.reload();
                }
            },
            error: function(error){
                if(error.responseJSON.loggedin == false)
                {
                    $("#login-message").html(error.responseJSON.message);
                }
            }
        });
        e.preventDefault();
        //return false;
    });

    $("#ajaxlogin_desk").on('submit', function (e) {
        // var $form = $(this);
        var login = $("#user_login_desk").val();
        var password = $("#user_pass_desk").val();
        var security = $("#ajaxlogin_desk #security").val();
        $.ajax({
            type: 'POST',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'ajaxlogin',
                'username': login,
                'password': password,
                'security': security,
            },
            dataType: "json",
            success: function (data) {
                //data = JSON.parse(data);
                if (data.loggedin == true) {
                    window.location.reload();
                }
            },
            error: function(error){
                if(error.responseJSON.loggedin == false)
                {
                    $("#login-message_desk").html(error.responseJSON.message);
                }
            }
        });
        e.preventDefault();
        //return false;
    });

    $(document).on('submit', '#ajaxregi', function () {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: $form.serialize(),
            dataType: "json",
            success: function (response) {
                $form.find('.ajax-result').html(response.error);
                if (response.log_in === 1) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: ajax_login_object.ajaxurl,
                        data: {
                            'action': 'ajaxlogin',
                            'username': response.email,
                            'password': response.password,
                            'security': response.security
                        },
                        success: function (data) {
                            if (data.loggedin === true) {
                                window.location.reload();
                            }
                        }
                    });
                }
            }
        });
        return false;
    });

    $(document).on('submit', '#ajaxregi_desk', function () {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: $form.serialize(),
            dataType: "json",
            success: function (response) {
                $form.find('.ajax-result').html(response.error);
                if (response.log_in === 1) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: ajax_login_object.ajaxurl,
                        data: {
                            'action': 'ajaxlogin',
                            'username': response.email,
                            'password': response.password,
                            'security': response.security
                        },
                        success: function (data) {
                            if (data.loggedin === true) {
                                window.location.reload();
                            }
                        }
                    });
                }
            }
        });
        return false;
    });

    $('#ajaxprofile').submit(function () {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: $form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response.msg !== undefined)
                    $form.find('.ajax-result').html(response.msg);
                if (response.reload == 1)
                    window.location.reload();

            }
        });
        return false;
    });

    $('#ajaxprofile_desk').submit(function () {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: $form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response.msg !== undefined)
                    $form.find('.ajax-result').html(response.msg);
                if (response.reload == 1)
                    window.location.reload();

            }
        });
        return false;
    });


    $('#ajaxchangepass').submit(function () {
        var $form = $(this);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: $form.serialize(),
            dataType: "json",
            success: function (response) {
                if (response.msg !== undefined)
                    $form.find('.ajax-result').html(response.msg);
                if (response.reset_form == 1)
                    $form.find('input[type=password]').val();

            }
        });
        return false;
    });
 
    $(document).on('click', '#admin-order-list .accordeon_item', function () {
        order_id = $(this).data('orderid');
        $('.order-tables').hide();
        $('#' + order_id).fadeIn(400);
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: "action=set_viewed_contractors&order_id=" + order_id,
            success: function (response) {

            }
        });
    })

    $(document).on('click', '.order-tables .plus, .order-tables .minus', function () {
        $this = $(this);
        contractor_id = $this.parents('tr').data('contractor-id');
        order_id = $this.parents('tr').data('order-id');

        if ($this.hasClass('plus')) {
            event = 'add';
            $this.removeClass('plus').addClass('minus');
        } else if ($this.hasClass('minus')) {
            event = 'remove';
            if( $this.parents('.order-tables').hasClass('order-tables-admin') ){
                $sel = $this;
            }else{
                $this.parents('tr').remove();
                $sel = $('#' + order_id + " tr[data-contractor-id="+contractor_id+"] .minus");
            }
            $sel.removeClass('minus').addClass('plus');
        }

        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: "action=edit_contributors&event=" + event + "&contractor_id=" + contractor_id + "&order_id=" + order_id,
            success: function (response) {
                $('#self-order-list .accordeon_content').html(response);
            }
        });
        return false;
    })

    $(document).on('click', '#self-order-list .accordeon_item', function () {
        order_id = $(this).data('orderid');
        $('.order-tables').hide();

        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: "action=print_self_orders_list&pnl_type=self&order_id=" + order_id,
            success: function (response) {
                $('.own-list').hide().html(response).fadeIn(400);
            }
        });
    })

    $('.remodal-confirm').on("click", function(){
        $.ajax({
            type: "POST",
            url: ajax_login_object.ajaxurl,
            data: "action=remove_current_user",
            success: function (response) {
                if(parseInt(response) == 1)
                    window.location.reload();
            }
        });

    })

    $(document).on('keyup','.search_active', function(){
        val = $(this).val().toLowerCase();
        par = $(this).parents('.order-tables');
        //if (val.length >= 2){
        elems = par.find(".main_table .file-name:Contains('"+val+"')");
        par.find(".main_table tr").hide();
        $(elems).parents('tr').show();
        //}
    })
})