<?php get_header(); /*Template Name: Contact*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section
        class="contact_content" <?php echo(get_field('bg_img') ? 'style=" background-image: url(' . get_field('bg_img') . ')"' : '') ?>>
    <div class="container is_smaller flex__mob">
        <?php if (have_posts()) : while (have_posts()) : the_post();
            if (get_the_content()) : ?>
                <div class="content form_box">
                    <?php the_content(); ?>
                </div>
            <?php endif; endwhile; endif; ?>
        <div class="contact_boxes">
            <div class="contact_consultation">
                <h5><?php echo __('[:ua]Отримайте консультацію[:ru]Получите консультацию') ?></h5>
                <?php if ($contact_faces = get_field('contact_faces')) { ?>
                    <div class="contact_faces">
                        <?php foreach ($contact_faces as $face) { ?>
                            <figure><img src="<?php echo $face['image']['sizes']['thumbnail']; ?>"
                                         alt="<?php echo $face['image']['alt']; ?>"></figure>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php echo(get_field('consultation_info') ? get_field('consultation_info') : '') ?>
                <?php if ($messengers = get_field('messengers')) { ?>
                   <div class="contact_messengers">
                       <?php foreach ($messengers as $messenger) { ?>
                           <a href="<?php echo $messenger['link'] ?>" target="_blank"><span
                                       class="<?php echo $messenger['messenger']; ?>"></span></a>
                       <?php } ?>
                   </div>
                <?php } ?>

            </div>
            <?php if ($socials = get_field('links', 'option')) { ?>
                <div class="contact_socials">
                    <h5><?php echo __('[:ua]Ми в соцмережах[:ru]Мы в соцсетях') ?></h5>
                    <?php foreach ($socials as $s) { ?>
                        <a href="<?php echo $s['link'] ?>" target="_blank"><?php echo $s['icon'] ?></a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php get_template_part('tpl-parts/trial-lesson-row') ?>
<?php get_footer(); ?>
