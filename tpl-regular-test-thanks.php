<?php get_header(); /*Template Name: Regular Test Thank You Page*/ ?>


<?php

$cookie = json_decode(stripslashes($_COOKIE[key($_REQUEST)]), true) ? json_decode(stripslashes($_COOKIE[key($_REQUEST)]), true) : 0;
$questions = $cookie['regular_tests']['points'];
$answers = $cookie['regular_tests']['answers'];
$empty = [];

foreach ($questions as $ar) {
    if(is_array($ar)) {
        foreach ($ar as $a) {
            array_push($empty, $a);
        }
    } else {
        array_push($empty, $ar);
    }
}

function correct($var) {
    return $var == true;
}

function incorrect($var) {
    return $var == 0 || $var == NULL;
}

?>
<div id="breadcrumbs" class="regular-test__breadcrumbs">
    <div class="container">
        <span>
            <span>
                <a href="https://nanoenglish.com/"><?php echo __('[:ua]Головна[:ru]Главная') ?></a> /
                <a href="https://nanoenglish.com/tests"><?php echo __('[:ua]Тести[:ru]Тесты') ?></a> /
                <?php if($cookie) { ?>
                    <a href="https://nanoenglish.com/tests/<?php echo $cookie['cat_en'] ?>"><?php echo $cookie['cat_ua'] ?></a> /
                    <span class="breadcrumb_last" aria-current="page"><?php echo $cookie['tit_ua'] ?></span> /
                <?php } ?>
                <span class="breadcrumb_last" aria-current="page"><?php echo __('[:ua]Результати[:ru]Результаты') ?></span>
            </span>
        </span>
    </div>
</div>

<section class="content container">

    <div class="regular-test__result flex_start__rwd">
        <div class="regular-test__result-col">
            <h2><?php echo __('[:ua]Вітаємо![:ru]Поздравляем!') ?></h2>
            <div id="regular-test__result">
                <h5>Ваш результат</h5>

                <div class="regular-test__result-row flex">
                    <span><?php echo __('[:ua]завдань пройдено[:ru]заданий пройдено') ?></span>
                    <b><?php echo count($empty) ?></b>
                </div>
                <div class="regular-test__result-row flex">
                    <span><?php echo __('[:ua]правильно[:ru]правильно') ?></span>
                    <b><?php echo count(array_filter($empty, 'correct')) ?></b>
                </div>
                <div class="regular-test__result-row flex">
                    <span><?php echo __('[:ua]неправильно[:ru]неправильно') ?></span>
                    <b style="color:#F81C29"><?php echo count(array_filter($empty, 'incorrect')) ?></b>
                </div>
                <div class="regular-test__result-row flex">
                    <span><?php echo __('[:ua]відсоток правильних[:ru]процент правильных') ?></span>
                    <b><?php echo $cookie ? round((count(array_filter($empty, 'correct'))/count($empty))*100, 2) : 0 ?>%</b>
                </div>

                <button class="button" data-fancybox data-src="#regular-test__detail">
                    <?php echo __('[:ua]Отримати детальний аналіз[:ru]Получить подробный анализ') ?>
                </button>
            </div>
        </div>
        <div class="regular-test__result-col regular-test__result_back aligned_center">
            <img src="<?php echo get_field('picture__regular_thanks')['url'] ?>" alt="<?php echo get_field('picture__regular_thanks')['alt'] ?>">
            <a href="/tests" class="button white"><?php echo __('[:ua]Повернутись до тестів[:ru]Вернуться к тестам') ?></a>
        </div>
    </div>

    <div id="category_name_get" class="hide"><?php echo $cookie['cat_ua'] ?></div>
    <div id="test_title_get" class="hide"><?php echo $cookie['tit_ua'] ?></div>
    <div id="regular_total_points_get" class="hide"><?php print_r($questions) ?></div>
    <div id="regular_total_res_get" class="hide"><?php print_r($answers) ?></div>

</section>

<div id="regular-test__detail" class="popup" style="display: none;">
    <h4>Детальний аналіз тесту</h4>
    <?php echo do_shortcode('[contact-form-7 id="2162" title="Detailer Regular Test"]') ?>
</div>

<div id="thanks_nano" class="popup" style="display: none;">
    <h4><?php echo __('[:ua]Дякуємо![:ru]Спасибо!') ?></h4>
    <p id="thanks_nano_text">
        <?php echo __('[:ua]Ваші результати успішно відправлено на вказану вами електронну скриньку[:ru]Ваши результаты успешно отправлена на указанный вами электронный адрес') ?>
        <strong id="nano_user"></strong>
    </p>
    <img src="<?php echo theme() ?>/images/check_success_nano.svg" alt="">
    <a href="<?php echo site_url(); ?>/tests/" class="button transparent"><?php echo __('[:ua]На головну[:ru]На главную') ?></a>
</div>

<?php if($goal = get_field('goals', 2)){ ?>
    <section class="goals">
        <div class="container is_smaller">
            <div class="goals_text">
                <?php the_field('text_goal') ?>
            </div>
            <div class="goals_items flex__rwd">
                <?php foreach ($goal as $g) { ?>
                    <div class="goal_item flip-card">
                        <div class="flip-card-inner">
                            <div class="goal__side flip-card-front">
                                <div class="goal_item__img flex_center">
                                    <img src="<?php echo $g['image'] ?>" alt="">
                                </div>
                                <div class="goal_item__text flex_center">
                                    <?php echo $g['text'] ?>
                                </div>
                            </div>
                            <div class="goal__side flip-card-back">
                                <h4><?php echo $g['text'] ?></h4>
                                <?php echo $g['text_hover']; ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php get_template_part('tpl-parts/trial-lesson-row') ?>

<?php get_footer(); ?>
