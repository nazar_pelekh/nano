<?php get_header();
/*Template Name: Course Detail*/
$isLogged = is_user_logged_in();

$post_data = get_post($post->post_parent);
$parent_slug = $post_data->post_name;
//echo $parent_slug;

$parent_title = get_the_title($post->post_parent)
?>

<section class="course_top" style="<?php echo image_src(get_post_thumbnail_id(), 'large', true); ?>">
    <?php if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
    } ?>
    <div class="container course_top__inner">
        <div class="item">
            <?php the_content(); ?>
        </div>
    </div>
</section>

<?php get_template_part('tpl-parts/trial-lesson-row') ?>

<section class="payment_content">
    <div class="container">
        <div style='display:none;' id='isLogged' data-logged='<?php echo $isLogged ? "true" : "false"; ?>'>
            <a href="javascript:;" class="enter_link" data-fancybox data-src="#success" id="successBtn"></a>
        </div>
        <div class="payment_text">
            <h2><?php echo __('[:ua]Програма Курсу[:ru]Программа Курса') ?></h2>
        </div>
        <div class="payment_prices">
            <?php
            $courses = new WP_Query(array(
                'post_type' => 'courses',
                'posts_per_page' => '-1',
                'post_status'   => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'courses_cat',
                        'field'    => 'slug',
                        'terms'    => $parent_slug
                    )
                ),
                'order' => 'ASC'
            ));
            ?>
            <div id="<?php echo $parent_title ?>" class="tab-content current">
                <div class="flex__rwd">
                    <?php foreach($courses->posts as $course): ?>
                        <div class="item flex lessons_<?php echo get_field("courses_count",$course->ID) ?>">
                            <div class="courses_count">
                                <p>
                                    <strong><?php echo get_field("courses_count",$course->ID); ?></strong>
                                    <?php echo __('[:ua]Занять[:ru]Занятий') ?>
                                </p>
                                <button class="button small chooseCourse" data-price="<?php echo get_field("price_for_1_course",$course->ID); ?>" data-courseid="<?php echo $course->ID; ?>" data-coursesale="<?php echo get_field("sale",$course->ID) ?>" data-coursescount="<?php echo get_field("courses_count",$course->ID); ?>" data-coursename="Basic English" data-fancybox data-src="#payment">
                                    <?php echo __('[:ua]Обрати план[:ru]Выбрать план') ?>
                                </button>
                            </div>
                            <div class="courses_info">
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("course_time",$course->ID).' хв[:ru]'.get_field("course_time",$course->ID).' мин') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span><?php echo __('[:ua]'.get_field("sale",$course->ID).' знижка[:ru]'.get_field("sale",$course->ID).' скидка') ?></span>
                                </p>
                                <p>
                                    <i class="check_icon"></i>
                                    <span class="pricefoc" data-pricefoc="<?php echo get_field("price_for_1_course", $course->ID); ?>"><span><?php echo get_field("price_for_1_course",$course->ID); ?>₴</span><?php echo __('[:ua]/заняття[:ru]/занятие') ?></span>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if($course_program = get_field('course_program')) { ?>
    <div class="container course_info">
        <div class="course_steps flex">
            <?php $i = 1; foreach($course_program as $count) { ?>
                <div class="course_step_wrapper">
                    <div id="step_<?php echo $i; ?>" class="course_step">
                        <i class="fas fa-check"></i>
                        <span class="count"><?php echo $i++; ?></span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="course_columns flex_start">
            <?php foreach($course_program as $course) { ?>
                <div class="course_column">
                    <span class="course_column_title"><?php echo $course['title'] ?></span>
                    <?php foreach($course['steps'] as $s) { ?>
                        <div class="course_column_row <?php echo $s['lessons_count'] ?> active">
                            <p class="flex_row">
                                <img src="<?php echo theme('/images/check_bg.png') ?>" class="check_icon_row" alt="">
                                <img src="<?php echo theme('/images/close.svg') ?>" class="close_icon" alt="">
                                <strong><span class="row_title"><?php echo $s['title'] ?></span></strong>
                            </p>
                            <span class="row_text"><?php echo $s['text'] ?></span>
                        </div>
                    <?php } ?>
                    <div class="course_popup">
                        <span class="popup_title">
                            <?php echo $course['popup_title'] ?>
                        </span>
                        <div class="popup_rows">
                            <?php foreach ($course['popup'] as $p) { ?>
                                <div class="popup_row <?php echo $p['lessons_count'] ?> closed">
                                    <span>
                                        <i class="fas fa-check"></i>
                                        <img src="<?php echo theme('/images/close.svg') ?>" class="close_icon" alt="">
                                    </span>
                                    <p>
                                        <?php echo $p['row'] ?>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>

<?php if($steps = get_field('steps_course')){ ?>
    <section class="steps">
        <div class="container steps_icons">
            <?php echo get_field('title_steps_course') ? '<div class="title_steps">' . get_field('title_steps_course') . '</div>' : ''; ?>
            <div class="step_items flex_start__rwd">
                <?php foreach($steps['4_steps'] as $s) { ?>
                    <div class="step_item">
                        <div class="step_item__icon">
                            <img src="<?php echo $s['icon'] ?>" alt="">
                        </div>
                        <p><?php echo $s['text'] ?></p>
                    </div>
                <?php } ?>
                <?php if($last_step = $steps['last_step']) { ?>
                    <div class="step_item_last">
                        <div class="flex">
                            <?php foreach($last_step['icons'] as $i) { ?>
                                <div class="last_step_icon">
                                    <img src="<?php echo $i['icon'] ?>" alt="">
                                    <p><?php echo $i['text'] ?></p>
                                </div>
                            <?php } ?>
                            <img src="<?php echo $last_step['Image'] ?>" alt="">
                        </div>
                        <p><?php echo $last_step['text'] ?></p>
                    </div>
                <?php } ?>
            </div>
            <?php if($button = get_field('button_course')) { ?>
                <div class="button_course">
                    <?php echo $button ?>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>

<?php get_template_part('tpl-parts/payment-popup') ?>

<?php get_template_part('tpl-parts/reviews-slider') ?>

<?php get_template_part('tpl-parts/facebook-widget') ?>

<?php get_footer(); ?>
