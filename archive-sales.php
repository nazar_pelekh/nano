<?php get_header(); ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section class="content container sales_page_content">
    <h2><?php echo __('[:ua]Акції[:ru]Акции') ?></h2>

    <?php
    // WP_Query arguments
    $args = array (
        'post_type'              => array( 'sales' ),
        'post_status'            => array( 'publish' ),
        'meta_key'               => 'end_date',
        'orderby'                => 'meta_value',
        'order'                  => 'ASC',
    );

    // The Query
    $sales = new WP_Query( $args );

    // The Loop
    if ( $sales->have_posts() ) { ?>
        <div class="tpl_sales_grid flex__mob">
            <?php while ( $sales->have_posts() ) {
                $sales->the_post();

                $format_in = 'Y-m-d H:i:s'; // the format your value is saved in (set in the field options)
                $format_out = 'd.m.Y'; // the format you want to end up with
                $date = DateTime::createFromFormat($format_in, get_field('end_date'));

                $now = date('Y-m-d H:i:s');

                if($now <= $date->format( $format_in )){ ?>
                    <div class="sale_item">
                        <a href="<?php the_permalink(); ?>" class="sale_thumb bg" style="<?php echo image_src(get_post_thumbnail_id(), 'large', true); ?>">
                            <span><?php echo __('[:ua]Діє до[:ru]Действует до') .' '. $date->format( $format_out ); ?></span>
                        </a>
                        <a href="<?php the_permalink(); ?>" class="sale_title"><?php the_title(); ?></a>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>

    <?php }

    // Restore original Post Data
    wp_reset_postdata();
    ?>

    <div class="tpl_sales_grid">

    </div>

</section>

<?php get_footer(); ?>
