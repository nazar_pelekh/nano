<?php get_header(); ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<section class="content container pagination_lower">

    <?php // WP_Query arguments
    $args = array (
        'post_type'              => array( 'webinars' ),
    );

    $webinars = new WP_Query( $args );

    if ( $webinars->have_posts() ) {
        $future = 0;
        $past = 0;
        while ( $webinars->have_posts() ) {
            $webinars->the_post();

            $format_in = 'Y-m-d H:i:s'; // the format your value is saved in (set in the field options)
            $format_out = 'd.m.Y'; // the format you want to end up with
            $format_out2 = 'H:i'; // the format you want to end up with
            $date = DateTime::createFromFormat($format_in, get_field('webinar_date'));
            $time = DateTime::createFromFormat($format_in, get_field('webinar_date'));

            $now = date('Y-m-d H:i:s');

            if($now <= $date->format( $format_in )){
                ++$future;
            } else {
                ++$past;
            }
        }
    }
    wp_reset_postdata();
    wp_reset_query(); ?>


    <?php
    $count = 0;
    if($future%2 == 0) {
        $count = 6;
    } else {
        $count = 5;
    }

    $order = 6;

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

    $args = array (
        'post_type'              => 'webinars',
        'meta_key'               => 'webinar_date',
        'orderby'                => 'meta_value',
        'order'                  => 'DESK',
        'paged'                  => $paged,
    );

    // The Query
    $webinars = new WP_Query( $args );

    // The Loop
    if ( $webinars->have_posts() ) { ?>
        <div class="tpl_webinars_grid  flex__mob">
            <?php

            $future_title = 0;
            $past_title = 0;

            while ( $webinars->have_posts() ) {

                $webinars->the_post();

                $format_in = 'Y-m-d H:i:s'; // the format your value is saved in (set in the field options)
                $format_out = 'd.m.Y'; // the format you want to end up with
                $format_out2 = 'H:i'; // the format you want to end up with
                $date = DateTime::createFromFormat($format_in, get_field('webinar_date'));
                $time = DateTime::createFromFormat($format_in, get_field('webinar_date'));

                $now = date('Y-m-d H:i:s');

                if($now <= $date->format( $format_in )){ ?>
                    <?php
                    echo $future_title == 0 ? "<h2>". __('[:ua]Вебінари[:ru]Вебинары') . "</h2>" : '';
                    ++$future_title;
                    ?>

                    <div class="webinar_item" style="order:<?php echo $order--; ?>">
                        <a href="<?php the_permalink(); ?>" class="webinar_thumb bg" style="<?php echo image_src(get_post_thumbnail_id(), 'large', true); ?>">
                            <span class="web_date"><?php echo $date->format( $format_out ); ?></span><span class="web_time"><?php echo $date->format( $format_out2 ); ?></span>
                        </a>

                        <a href="<?php the_permalink(); ?>" class="webinar_title"><?php the_title(); ?></a>

                        <?php if($preview = get_field('preview_text')) { ?>
                            <div class="webinar_preview">
                                <?php echo $preview ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <?php
                    echo $past_title == 0 ? "<h2>". __('[:ua]Архів вебінарів[:ru]Архив вебинаров') . "</h2>" : '';
                    ++$past_title;
                    ?>

                    <div class="webinar_item">
                        <a href="<?php the_permalink(); ?>" class="webinar_thumb bg" style="<?php echo image_src(get_post_thumbnail_id(), 'large', true); ?>">
                            <span class="expired">Завершено</span>
                        </a>

                        <a href="<?php the_permalink(); ?>" class="webinar_title"><?php the_title(); ?></a>

                        <?php if($preview = get_field('preview_text')) { ?>
                            <div class="webinar_preview">
                                <?php echo $preview ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

            <?php } ?>
        </div>

    <?php }

    if (function_exists('wp_pagenavi')) :
        wp_pagenavi(array( 'query' => $webinars ));
    endif;

    wp_reset_postdata();
    ?>


</section>

<?php get_footer(); ?>
