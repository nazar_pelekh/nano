<?php get_header(); /*Template Name: Trial Lesson*/ ?>
<?php if (function_exists('yoast_breadcrumb')) {
    yoast_breadcrumb('<div id="breadcrumbs"><div class="container">', '</div></div>');
} ?>
<?php if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) : ?>
        <section class="flex_text_video trial_text_video">
            <div class="container">
                <h1><?php the_title(); ?></h1>
                <div class="flex__mob">
                    <?php
                    $video = get_field('video');
                    if ($video['type'] == 'library') { ?>
                        <div class="half_video def_video_popup"
                             style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                            <a href="#video" data-fancybox class="play_icon">
                                <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                            </a>
                            <video id="video" src="<?php echo $video['file']; ?>" controls muted
                                   style="display: none"></video>
                        </div>
                    <?php } elseif ($video['type'] == 'link') { ?>
                        <div class="half_video def_video_popup"
                             style="background-image: url(<?php echo $video['poster']['sizes']['medium_large'] ?>);">
                            <a href="<?php echo $video['link']; ?>" data-fancybox class="play_icon">
                                <img src="<?php echo theme('images/play.svg') ?>" alt="play video">
                            </a>
                        </div>
                    <?php } ?>
                    <div class="half_text">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; endwhile; endif; ?>

<section class="trial_lesson_stats">
    <div class="container">
        <?php echo(get_field('title_li') ? '<div class="trial_lesson_text">' . get_field('title_li') . '</div>' : '') ?>
        <?php if($diag_text = get_field('diagram_text_li')) { ?>
            <div class="trial_lesson_block">
            <?php $diagram = get_field('picture_li'); echo $diagram ? '<img class="trial_lesson_img" src="'.$diagram.'"/>' : 'empty' ?>
                <?php foreach ($diag_text as $d) { ?>
                    <div class="<?php echo $d['title'] ?> trial_lesson_line">
                        <span class="rwd_hide" style="background-color: <?php echo $d['color'] ?>;"></span>
                        <h5 style="color:<?php echo $d['color'] ?>"><span class="rwd_show"><?php echo $d['percent'] ?> </span><?php echo $d['title'] ?></h5>
                        <?php echo $d['text'] ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</section>

<?php get_template_part('tpl-parts/trial-lesson-row') ?>

<?php get_footer(); ?>
