<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
define('PASS_LEN', 6);
//login shortcode
add_shortcode('ajax_login', 'ajaxlogin_shortcode_handler');

function ajaxlogin_shortcode_handler($atts, $content = null) {
    ob_start();
    ?>

    <div id="enter" class="popup" style="display: none;">
        <h4><?php echo __('[:ua]Вхід у nanoEnglish[:ru]Вход в nanoEnglish') ?></h4>
        <form name="ajaxlogin" id="ajaxlogin" method="post" class="form_inputs">
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="username" id="user_login" value="" size="20" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label><?php echo __('[:ua]Логін[:ru]Логин') ?></label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="password" name="password" id="user_pass" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label>Пароль</label>
            </div>
            <a id="forgot_password_button" class="forgot_password_button" href="javascript:;" data-fancybox data-src="#forgot_password"><?php echo __('[:ua]Забули пароль?[:ru]Забыли пароль?') ?></a>
            <input class="button" type="submit" name="wp-submit" id="log-submit" value="<?php echo __('[:ua]Увійти[:ru]Войти') ?>" class="wpcf7-form-control wpcf7-submit button">
<!--            <input type="hidden" name="redirect_to" value="--><?php //echo site_url($_SERVER["REQUEST_URI"]); ?><!--" />-->
            <?php if($login_text = get_field('login_text','option')) { ?>
                <div class="login_text">
                    <?php echo $login_text; ?>
                </div>
            <?php } ?>
            <input type="hidden" name="action" value="ajaxlogin" />
            <div id="login-result"><span></span></div>
            <p id="login-message"></p>
            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
        </form>
    </div>

    <?php
    $logform = ob_get_contents();
    ob_end_clean();
    return $logform;
}


//login desk shortcode
add_shortcode('ajax_login_desk', 'ajaxlogin_shortcode_handler_desk');

function ajaxlogin_shortcode_handler_desk($atts, $content = null) {
    ob_start();
    ?>

    <div id="login_desk" class="log_desk is_hidden">
        <p><?php echo __('[:ua]Увійдіть в свій акаунт для продовження[:ru]Войдите в свой аккаунт для продолжения') ?></p>
        <br>
        <p><span><?php echo __('[:ua]На ваш акаунт будуть надіслані усі деталі[:ru]На ваш аккаунт будут направлены все детали') ?></span></p>
        <form name="ajaxlogin_desk" id="ajaxlogin_desk" method="post" class="form_inputs">
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="username" id="user_login_desk" value="" size="20" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label><?php echo __('[:ua]Логін[:ru]Логин') ?></label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="password" name="password" id="user_pass_desk" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label>Пароль</label>
            </div>
            <a id="forgot_password_button_desk" class="forgot_password_button" href="javascript:;"><?php echo __('[:ua]Забули пароль?[:ru]Забыли пароль?') ?></a>
            <input class="button" type="submit" name="wp-submit" id="log-submit" value="<?php echo __('[:ua]Увійти[:ru]Войти') ?>" class="wpcf7-form-control wpcf7-submit button">

            <input type="hidden" name="action" value="ajaxlogin" />
            <div id="login-result_desk"><span></span></div>
            <p id="login-message_desk"></p>
            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
        </form>
        <div class="log_link">
            <p><?php echo __('[:ua]Не маєш акаунту?[:ru]Не имеешь аккаунта?') ?> <a href="javascript:;" class="register_link_button"><?php echo __('[:ua]Зареєструйся[:ru]Зарегистрируйся') ?></a></p>
        </div>
    </div>

    <?php
    $logform = ob_get_contents();
    ob_end_clean();
    return $logform;
}

/* register shortcode */
add_shortcode('ajax_register', 'ajax_regi_form');

function ajax_regi_form($atts, $content = null) {
    $atts = shortcode_atts(array('page' => 'register'), $atts);
    $frm_name = $atts['page'] == 'profile' ? "ajaxprofile_desk" : "ajaxregi_desk";
    $frm_subm = $atts['page'] == 'profile' ? __('[:ua]Зберегти[:ru]Сохранить') : __('[:ua]Продовжити[:ru]Продолжить');
    $usr_email = $usr_name = $usr_phone = $usr_phone_2 = "";

    if (is_user_logged_in()) {
        global $current_user;
        $cur_usr = $current_user;
        $usr_meta = get_user_meta($cur_usr->ID);

        $usr_email = $cur_usr->user_email;
        $usr_name = isset($usr_meta) && trim($cur_usr->first_name) ? $cur_usr->first_name : $cur_usr->display_name;
        $usr_phone = isset($usr_meta['phone']) ? $usr_meta['phone'][0] : "";
        $usr_phone_2 = isset($usr_meta['phone_2']) ? $usr_meta['phone_2'][0] : "";
    }

    ob_start();
    ?>


    <div id="register" class="popup" style="display: none;">
        <div class="close"></div>
        <h4><?php echo __('[:ua]Зареєструватись у nanoEnglish[:ru]Регистрация в nanoEnglish') ?></h4>
        <form name="<?php echo $frm_name; ?>" id="<?php echo $frm_name; ?>" class="wpcf7 form_inputs">
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="first_name" id="regi_user_name" value="<?php echo $usr_name; ?>" size="20" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label><?php echo __('[:ua]ПІБ[:ru]ФИО') ?></label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_phone" id="regi_user_phone" value="<?php echo $usr_phone; ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label>Моб. номер</label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_email" id="regi_user_email" value="<?php echo $usr_email; ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label>Email</label>
            </div>
            <input class="button" type="submit" name="wp-submit" id="wp-submit" value="<?php echo __('[:ua]Зареєструватись[:ru]Зарегистрироваться') ?>" class="wpcf7-form-control wpcf7-submit button">
            <?php
            if ($atts['page'] == 'profile') {
                echo '<input type="hidden" name="action" value="update_profile" />';
                echo wp_nonce_field('prof_edit_nonce', 'prof_security', null, false);
            } else {
                echo '<input type="hidden" name="redirect_to" value="' . site_url($_SERVER["REQUEST_URI"]) . '" />';
                echo '<input type="hidden" name="action" value="custom_register" />';
                echo wp_nonce_field('ajax-login-nonce', 'reg_security', null, false);
            }
            ?>
        <div class="ajax-result"></div>
        </form>
    </div>

    <?php
    $reform = ob_get_contents();
    ob_end_clean();
    return $reform;
}

/* register desk shortcode */
add_shortcode('ajax_register_desk', 'ajax_regi_form_desk');

function ajax_regi_form_desk($atts, $content = null) {
    $atts = shortcode_atts(array('page' => 'register'), $atts);
    $frm_name = $atts['page'] == 'profile' ? "ajaxprofile" : "ajaxregi";
    $frm_subm = $atts['page'] == 'profile' ? __('[:ua]Зберегти[:ru]Сохранить') : __('[:ua]Продовжити[:ru]Продолжить');
    $usr_email = $usr_name = $usr_phone = $usr_phone_2 = "";

    if (is_user_logged_in()) {
        global $current_user;
        $cur_usr = $current_user;
        $usr_meta = get_user_meta($cur_usr->ID);

        $usr_email = $cur_usr->user_email;
        $usr_name = isset($usr_meta) && trim($cur_usr->first_name) ? $cur_usr->first_name : $cur_usr->display_name;
        $usr_phone = isset($usr_meta['phone']) ? $usr_meta['phone'][0] : "";
        $usr_phone_2 = isset($usr_meta['phone_2']) ? $usr_meta['phone_2'][0] : "";
    }

    ob_start();
    ?>


    <div id="register_desk" class="log_desk is_hidden">
        <p><?php echo __('[:ua]Зареєструйтесь щоб продовжити[:ru]Зарегистрируйтесь чтобы продолжить') ?></p>
        <br>
        <p><span><?php echo __('[:ua]На ваш акаунт будуть надіслані усі деталі[:ru]На ваш аккаунт будут направлены все детали') ?></span></p>
        <form name="<?php echo $frm_name; ?>" id="<?php echo $frm_name; ?>" class="wpcf7 form_inputs">
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_email" id="regi_user_email_desk" value="<?php echo $usr_email; ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label>Email<sup>*</sup></label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="user_phone" id="regi_user_phone_desk" value="<?php echo $usr_phone; ?>" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label><?php echo __('[:ua]Номер телефону[:ru]Номер телефона') ?><sup>*</sup></label>
            </div>
            <div class="form_line">
                    <span class="wpcf7-form-control-wrap">
                        <input type="text" name="first_name" id="regi_user_name_desk" value="<?php echo $usr_name; ?>" size="20" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
                    </span>
                <label><?php echo __('[:ua]ПІБ[:ru]ФИО') ?><sup>*</sup></label>
            </div>
            <input class="button" type="submit" name="wp-submit" id="wp-submit_desk" value="<?php echo __('[:ua]Зареєструватись[:ru]Зарегистрироваться') ?>" class="wpcf7-form-control wpcf7-submit button">
            <?php
            if ($atts['page'] == 'profile') {
                echo '<input type="hidden" name="action" value="update_profile" />';
                echo wp_nonce_field('prof_edit_nonce', 'prof_security', null, false);
            } else {
                echo '<input type="hidden" name="redirect_to" value="' . site_url($_SERVER["REQUEST_URI"]) . '" />';
                echo '<input type="hidden" name="action" value="custom_register" />';
                echo wp_nonce_field('ajax-login-nonce', 'reg_security', null, false);
            }
            ?>
            <div class="ajax-result"></div>
        </form>
        <div class="log_link">
            <p><?php echo __('[:ua]Вже зареєстрований?[:ru]Уже зарегистрирован?') ?> <a href="javascript:;" class="log_link_button"><?php echo __('[:ua]Увійти[:ru]Войти') ?></a></p>
        </div>
    </div>

    <?php
    $reform = ob_get_contents();
    ob_end_clean();
    return $reform;
}

add_shortcode('ajax_changepass', 'ajaxchangepass_shortcode_handler');

function ajaxchangepass_shortcode_handler($atts, $content = null) {
    ob_start();
    ?>

    <form name="ajaxchangepass" id="ajaxchangepass" action="" method="post" class="wpcf7">
        <div class="ajax-result"></div>
        <span class="regi-password req_star">
            <label for="user_oldpass"><?php echo __('[:ua]Старий пароль[:ru]Старый пароль') ?></label>
            <input type="password" name="user_oldpass" id="user_oldpass" class="input" value="" size="20">
        </span>
        <span class="regi-password req_star">
            <label for="user_pass"><?php echo __('[:ua]Новий пароль[:ru]Новый пароль') ?></label>
            <input type="password" name="user_pass" id="user_pass" class="input" value="" size="20">
        </span>
        <span class="regi-password req_star">
            <label for="user_repass"><?php echo __('[:ua]Підтвердити новий пароль[:ru]Подтвердить новый пароль') ?></label>
            <input type="password" name="user_repass" id="user_repass" class="input" value="" size="20">
        </span>
        <div class="regi-submit cfx">
            <input type="submit" name="wp-submit" class="button" value="<?php echo __('[:ua]Зберегти[:ru]Сохранить') ?>">
            <input type="hidden" name="action" value="change_password" />
            <?php echo wp_nonce_field('ajax-changepass-nonce', 'changepass_security', null, false); ?>
        </div>
    </form>

    <?php
    $form = ob_get_contents();
    ob_end_clean();
    return $form;
}

add_action('wp_ajax_change_password', 'ajax_change_password');
function ajax_change_password() {
    $errors = array();
    $addMsg = "";
    $user = get_user_by('id', get_current_user_id());


    if (!is_user_logged_in() || !check_ajax_referer('ajax-changepass-nonce', 'changepass_security')) {
        $result['msg'] = alertMessage("You don't have sufficient permissions to edit profile!", 1);
    } else {
        extract($_POST);
        if (!trim($user_oldpass)) {
            $errors[] = getErrorMessage("Старый пароль", "empty_val");
        } elseif (!wp_check_password($user_oldpass, $user->user_pass, $user->ID) ) {
            $errors[] = getErrorMessage("Старый пароль", "not_match");
        }
        if (!trim($user_pass)) {
            $errors[] = getErrorMessage("Password", "empty_val");
        }elseif ( strlen($user_pass) < PASS_LEN ) {
            $errors[] = getErrorMessage("Password", "min_size", PASS_LEN);
        }elseif ($user_pass != $user_repass) {
            $errors[] = getErrorMessage("Repeat password", "not_match");
        }

        if (sizeof($errors)) {
            $result['msg'] = alertMessage($errors, 1);
        } elseif( wp_update_user( array('ID' => $user->ID, 'user_pass' => $user_repass) ) ) {
            $result['msg'] = alertMessage("Your password has been successfully changed");
            $result['reset_form'] = 1;
        }else{
            $result['msg'] = alertMessage("Error password change!", 1);
        }
    }
    echo json_encode($result);
    exit();
}

if (!is_admin())
    add_action('init', 'ajax_login_init');

function ajax_login_init() {
    wp_register_script('ajax-login-script', get_stylesheet_directory_uri() . '/js/user.js', array('jquery'), '1.0', true);
    wp_enqueue_script('ajax-login-script');
    wp_localize_script('ajax-login-script', 'ajax_login_object', array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'redirecturl' => site_url() . $_SERVER["REQUEST_URI"],
        'loadingmessage' => __('[:ua]Будь ласка, зачекайте...[:ru]Пожалуйста, подождите...')
    ));
}

add_action('wp_ajax_nopriv_ajaxlogin', 'ajax_login');
function ajax_login() {
    check_ajax_referer( 'ajax-login-nonce', 'security' );
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;
    $result = array();
    $user_signon = wp_signon($info, false);
    if (is_wp_error($user_signon)) {
//        echo "false";
//        exit();
        $result['loggedin'] = false;
        $result['message'] = "<div class='error'><p>".__('[:ua]Введені пошта або пароль невірні. Будь ласка, повторіть спробу.[:ru]Введённая почта или пароль неправильны. Пожалуйста, повторите попытку.')."</p></div>";
    } else {
//        echo "true";
//        exit();
        $result['loggedin'] = true;
        $result['message'] = "<div class='sucecess'><p>".__('[:ua]Будь ласка, зачекайте...[:ru]Пожалуйста, подождите...')."</p></div>";
    }

    echo json_encode($result);
    exit();
}

//user register
add_action('register_form', 'addpass_register_form');

function addpass_register_form() {
    $first_name = ( isset($_POST['password']) ) ? $_POST['password'] : '';
    ?>
    <label for="password">Password</label>
    <input type="password" name="password" id="password" size="20" />
    <?php
}

add_filter('registration_errors', 'addpass_registration_errors', 10, 3);

function addpass_registration_errors($errors, $sanitized_user_login, $user_email) {
    if (empty($_POST['password']))
        $errors->add('password_error', 'Пожалуйста, введите пароль');
    return $errors;
}

add_action('user_register', 'addpass_user_register');

function addpass_user_register($user_id) {
    if (isset($_POST['password']))
        update_user_meta($user_id, 'password', $_POST['password']);
    update_user_meta($user_id, 'show_admin_bar_front', 'false');
}


add_action('wp_ajax_forgot_pass', 'forgot_pass');
add_action('wp_ajax_nopriv_forgot_pass', 'forgot_pass');

function forgot_pass() {
    extract($_POST);

    global $wpdb, $wp_hasher;

    $errors = new WP_Error();

    if ( empty( $user_login ) ) {
        echo __('[:ua]Помилка: Введіть свій email.[:ru]Ошибка: Введите email.');
        die();
    } else if ( strpos( $user_login, '@' ) ) {
        $user_data = get_user_by( 'email', trim( $user_login ) );
        if ( empty( $user_data ) )
        {
            echo __('[:ua]Помилка: Користувача з таким електронним адресом не існує.[:ru]Ошибка: Пользователя с таким электронным адресом не существует.');
            die();
        }
    } else {
        $login = trim( $user_login );
        $user_data = get_user_by('login', $login);
    }

    do_action( 'lostpassword_post', $errors );

    if ( $errors->get_error_code() )
        return $errors;

    if ( !$user_data ) {
        $errors->add('invalidcombo', __('<strong>ERROR</strong>: Invalid username or email.'));
        return $errors;
    }


    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;
    $key = get_password_reset_key( $user_data );
    if ( is_wp_error( $key ) ) {
        return $key;
    }
    $message = __('[:ua]Ви подали заявку на відновлення пароля. Для його скидання, перейдіть за посиланням вказаної нижче.[:ru]Вы подали заявку на восстановления пароля. Для его сброса, перейдите по ссылке указаной ниже.');
    $message .= esc_url( get_site_url() . "/?action=rp&key=$key&login=" . rawurlencode($user_login) ) . "\r\n";
//    $message .= network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' );

    if ( wp_mail( $user_email, wp_specialchars_decode( "Nano Password Reset" ), $message ) )
        echo __('[:ua]Посилання для скидання пароля була відправлена на вашу електронну адресу.[:ru]Ссылка для сброса пароля была отправлена на ваш электронный адрес.');
    else
        echo __('[:ua]Посилання для скидання пароля була відправлена на вашу електронну адресу.[:ru]Ссылка для сброса пароля была отправлена на ваш электронный адрес.');

    if (is_ajax()) {
        exit();
    }
}



add_action( 'wp_ajax_nopriv_reset_pass', 'reset_pass_callback' );
add_action( 'wp_ajax_reset_pass', 'reset_pass_callback' );
/*
 *	@desc	Process reset password
 */
function reset_pass_callback($key = null, $login = null) {

    extract($_POST);

    $errors = new WP_Error();

    $user = check_password_reset_key( $user_key, $user_login );
    if($user->errors):
        foreach ($user->errors as $key => $error) {
            switch($key)
            {
                case "invalid_key": echo "Срок действия ключа истёк.";die();break;
            }
        }
    endif;
//    var_dump($user);die();

    if( empty( $pass1 ) || empty( $pass2 ) )
    {
        echo "Поле 'Пароль' - обязательное";die();
    }

    if ( isset( $pass1 ) && $pass1 != $pass2 )
    {
        echo "Пароли не совпадают";die();
    }

    if ( ( ! $errors->get_error_code() ) && isset( $pass1 ) && !empty( $pass1 ) ) {
        reset_password($user, $pass1);
        echo "Пароль изменён";
    }

    die();
}



add_action('wp_ajax_custom_register', 'custom_register_new_user');
add_action('wp_ajax_nopriv_custom_register', 'custom_register_new_user');

function custom_register_new_user() {
//    die("test");
    $res = validateRegiForm(false, true);
    if (!$res['errors']) {
        $user_data = $res['user_data'];

        $user_id = wp_create_user($user_data['user_login'], $user_data['user_pass'], $user_data['user_email']);
        if ($user_id) {
            wp_update_user(array(
                'ID' => $user_id,
                'first_name' => $user_data['first_name'],
                'display_name' => $user_data['first_name']
            ));
            //$result['user_id'] = $user_id;
            $result['log_in'] = 1;
            $result['email'] = $user_data['user_email'];
            $result['phone'] = $user_data['user_phone'];
            $result['password'] = $user_data['user_pass'];
            $result['security'] = $user_data['reg_security'];
            $result['error'] = alertMessage(__('[:ua]Ви успішно зареєстровані![:ru]Вы успешно зарегистрированы!'));
            update_user_meta($user_id, "phone", $user_data['user_phone']);
            update_user_meta($user_id, "pswrd", $user_data['user_pass']);
            //wp_new_user_notification($user_id, $user_data['user_pass']);

            $userObj = get_user_by('email', $user_data['user_email'] );
            if ( !is_wp_error( $userObj ) )
            {
                wp_clear_auth_cookie();
                wp_set_current_user ( $userObj->ID );
                wp_set_auth_cookie  ( $userObj->ID );
            }
        }
    } else {
        $result['error'] = alertMessage($res['errors'], 1);
    }
    echo json_encode($result);
    exit();
}

function curlApi($url, $fields, $token = false, $origin = false)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    if($fields) curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type: application/json';
    if($origin) $headers[] = 'Origin: '.get_site_url().'1';
    if($token) $headers[] = 'x-access-token: '.$token;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    return $result;
}

function getTokenApi()
{
    $url = 'https://api.moyklass.com/v1/company/auth/getToken';
    $fields = array("apiKey" => "Pfl44knSSg3ABl1GuZwnFMrEYaPHxOaNtFTNL9eICarNvNPonF");
    $fields = json_encode($fields);
    $result = curlApi($url, $fields);
    $result = json_decode($result, true);

    $token = "";
    if(isset($result["accessToken"]) && !empty($result['accessToken'])) {
        return $result["accessToken"];
    }
    else {
        return array('error' => '1');
    }
}

function revokeTokenApi($token)
{
    $url = 'https://api.moyklass.com/v1/company/auth/revokeToken';
    $result = curlApi($url, false, $token);
    $result = json_decode($result, true);
}

function registerUserInMoyKlass($userID)
{

    $token = getTokenApi();
    if(isset($token['error'])) {
        return array('error' => '1');
    }

    $userData = get_userdata($userID);
    $userPhone = get_field('phone',"user_".$userID);
    $balance = get_field('balance', 'user_'.$userID);
    $currency = get_field('currency', 'user_'.$userID);
    $userPass = get_field('pswrd', "user_".$userID);

    $userPhone = preg_replace('/[^0-9\-]/', '', $userPhone);
    $url = 'https://api.moyklass.com/v1/company/users';
    $fields = array(
        "name"  => $userData->display_name,
        "email" => $userData->user_email,
        "phone" => $userPhone,
        'attributes'    =>  array(
                array(
                    'attributeId' => 1646,
                    'value' => $balance." ".$currency
                )
        )
    );





    $fields = json_encode($fields);
    $result = curlApi($url, $fields, $token);
    $result = json_decode($result, true);
//    var_dump($result);die();
    $message = __(
        '[:ua]Ви успішно зареєстровані!
Пароль для входу в кабінет Nano: '.$userPass.'
Для створення паролю в особистому кабінеті MoyKlass, перейдіть за посиланням: nanodev.tvoyklass.com та введіть свою пошту.
                [:ru]Вы успешно зарегистрированы! 
Пароль для входа в кабинет Nano: '.$userPass.'
Для создания пароля в личном кабинете MoyKlass, перейдите по ссылке: nanodev.tvoyklass.com и введите свою почту.'
    );
    wp_mail($userData->user_email, 'New User', $message);


    revokeTokenApi($token);
    return $result;

}

add_action('wp_ajax_update_profile', 'ajax_update_profile');
function ajax_update_profile() {
    $result = array();
    $addMsg = "";
    if (!is_user_logged_in() || !check_ajax_referer('prof_edit_nonce', 'prof_security')) {
        $result['msg'] = alertMessage(" У вас нет достаточных прав для редактирования профиля!", 1);
    } else {
        global $current_user;
        global $wpdb;
        $cur_usr = get_currentuserinfo();
        $ignore_login = $cur_usr->user_email == trim($_POST['user_email']);
        $res = validateRegiForm($ignore_login, true);
        if ($res['errors']) {
            $result['msg'] = alertMessage($res['errors'], 1);
        } else {
            update_user_meta($cur_usr->ID, "first_name", $res['user_data']['first_name']);                                                          // updt user First name
            update_user_meta($cur_usr->ID, "phone", $res['user_data']['user_phone']);                                                               // updt user Phone
            isset($res['user_data']['user_phone_2']) ? update_user_meta($cur_usr->ID, "phone_2", trim($res['user_data']['user_phone_2'])) : false;  // updt user Phone 2

            if (!$ignore_login) {
                $upd_usr_data = array('ID' => $cur_usr->ID, 'user_email' => $res['user_data']['user_email']);                                       // updt user email

                if ($cur_usr->user_login != $res['user_data']['user_email']) {
                    $upd_usr_data['user_nicename'] = sanitize_user($res['user_data']['user_email']);                                                // updt user nicename
                    $wpdb->update($wpdb->users, array('user_login' => $res['user_data']['user_email']), array('ID' => $cur_usr->ID));               // updt user Login
                }
                wp_update_user($upd_usr_data);
                wp_logout();
                $result['reload'] = 1;
                $addMsg = __('[:ua]Можете увійти під новим логіном.[:ru]Можете входить под новым логином.');
            }
            $result['msg'] = alertMessage(__('[:ua]Данні було успішно оновлено [:ru]Данные были успешно обновлены').$addMsg);
        }
    }
    echo json_encode($result);
    exit();
}

function validateRegiForm($ignore_login = false, $ignore_pass = false) {
    extract($_POST);
    $out = array();
    $user_data = $_POST;
    $errors = [];

    $sanitized_user_login = sanitize_user($user_email);
    $user_email = apply_filters('user_registration_email', $user_email);

//    var_dump(username_exists($sanitized_user_login));
    if (!$ignore_login) {
        if ($sanitized_user_login == '') {
            $errors[] = getErrorMessage("E-mail", "empty_val");
        } elseif (!validate_username($sanitized_user_login)) {
            $errors[] = getErrorMessage(__('[:ua]ПІБ[:ru]ФИО'), "illegal_char");
        } elseif (username_exists($sanitized_user_login)) {
            $errors[] = getErrorMessage(__('[:ua]ПІБ[:ru]ФИО'), "already_exists");
        }
        if (!is_email($user_email)) {
            $errors[] = getErrorMessage("E-mail", "invalid_val");
        } elseif (email_exists($user_email)) {
            $errors[] = getErrorMessage("E-mail", "already_exists");
        }
    }
    if (!$ignore_pass) {
        if (!trim($user_pass)) {
            $errors[] = getErrorMessage("Password", "empty_val");
        } elseif ( strlen($user_pass) < PASS_LEN ) {
            $errors[] = getErrorMessage("Password", "min_size", PASS_LEN);
        } elseif ($user_pass != $user_repass) {
            $errors[] = getErrorMessage("Password", "not_match");
        }
    }
    else{
        $user_pass = randomPassword();
    }
    if (!trim($first_name))
        $errors[] = getErrorMessage(__('[:ua]ПІБ[:ru]ФИО'), "empty_val");
    if (isset($user_phone) && !trim($user_phone))
        $errors[] = getErrorMessage("Телефон", "empty_val");

    if (count($errors)) {
        $out['errors'] = $errors;
    } else {
        $out['errors'] = false;
        if (!$ignore_login) {
            $user_data['user_login'] = $sanitized_user_login;
            $user_data['user_email'] = $user_email;
            $user_data['user_phone'] = $user_phone;
        }
        if($ignore_pass)
        {
            $user_data['user_pass'] = $user_pass;
        }
    }
    $out['user_data'] = $user_data;
    return $out;
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function getErrorMessage($field, $errorType, $addvalue = NULL) {
    $errorTag = "<strong>".__('[:ua]Помилка[:ru]Ошибка')."   </strong>:";
    switch ($errorType) {
        case "empty_val" :
            return "$errorTag ".__('[:ua]Будь ласка, заповніть [:ru]Пожалуйста заполните')." поле <b>\"$field\"</b>";
        case "incorrect_val" :
            return "$errorTag ".__('[:ua]Некоректне значення поля: [:ru]Некоректное значение поля:')." <b>\"$field\"</b>";
        case "illegal_char" :
            return "$errorTag ".__('[:ua]Недопустимі символи. Заповніть правильно поле:[:ru]Недопустимые символы. Заполните правильно поле:')." <b>\"$field\"</b>";
        case "already_exists" :
            return "$errorTag ".__('[:ua]Данний[:ru]Этот')." <b>\"$field\"</b> ".__('[:ua]вже зареєстровано. Спробуйте інший.[:ru]уже зарегистрирован. Попробуйте другой.')."";
        case "invalid_val":
            return "$errorTag Поле <b>\"$field\"</b> ".__('[:ua]введено неправильно[:ru]введено неправильно')."";
        case "not_match":
            return "$errorTag The <b>\"$field\"</b> does not match";
        case "min_size":
            return "$errorTag The  <b>\"$field\"</b> must contain at least $addvalue characters";
    }
}

function alertMessage($msgs, $err = false) {
    $class = $err ? "error" : "success";
    $txt = is_array($msgs) ? "<p>" . implode("</p><p>", $msgs) . "</p>" : "<p>$msgs</p>";
    return "<div class='$class'>$txt</div>";
}
//AJAXSIGN