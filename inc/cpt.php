<?php
//Example for Custom Post Type with Taxonimies

/*
    *** You van use dash-icons https://developer.wordpress.org/resource/dashicons/
*/
add_action( 'init', 'register_cpts' );
function register_cpts() {

    //custom taxonomy attached to CPT
    $taxname = 'Category';
    $taxlabels = array(
        'name'                          => $taxname,
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All '.$taxname.'s',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate '.$taxname.'s with commas',
        'add_or_remove_items'           => 'Add or remove '.$taxname.'s',
        'choose_from_most_used'         => 'Choose from most used '.$taxname.'s'
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'rewrite'                       => true,
        'show_admin_column'             => true
    );
    register_taxonomy( 'courses_cat', 'courses', $taxarr );

    register_post_type( 'courses',
        array(
            'labels' => array(
            'name' => 'Courses Price',
            'singular_name' => 'Course Price',
            'menu_name' => 'Courses Price'
        ),
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'rewrite'               => array( 'slug' => 'permalink', 'with_front' => false ),
        'has_archive'           => true,
        'hierarchical'          => true,
        'show_in_nav_menus'     => true,
        'capability_type'       => 'page',
        'query_var'             => true,
        'menu_icon'             => 'dashicons-admin-page',
    ));

    register_post_type( 'promocodes',
        array(
            'labels' => array(
                'name' => 'Courses Promocode',
                'singular_name' => 'Course Promocode',
                'menu_name' => 'Courses Promocode'
            ),
            'public'                => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array('title'),
            'rewrite'               => array(),
            'has_archive'           => false,
            'hierarchical'          => false,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => 'dashicons-admin-page',
        ));

    register_post_type( 'reviews',
        array(
            'labels' => array(
                'name' => 'Reviews',
                'singular_name' => 'Reviews',
                'menu_name' => 'Reviews'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'               => array( 'slug' => 'permalink', 'with_front' => false ),
            'has_archive'           => false,
            'hierarchical'          => true,
            'publicly_queryable'    => false,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => 'dashicons-admin-comments',
        ));

    register_post_type( 'teachers',
        array(
            'labels' => array(
                'name' => 'Teachers',
                'singular_name' => 'Teachers',
                'menu_name' => 'Teachers'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'               => array( 'slug' => 'permalink', 'with_front' => false ),
            'has_archive'           => false,
            'hierarchical'          => true,
            'publicly_queryable'    => false,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => 'dashicons-groups',
        ));

    register_post_type( 'sales',
        array(
            'labels' => array(
                'name' => __('[:ua]Акції[:ru]Акции'),
                'singular_name' => __('[:ua]Акція[:ru]Акция'),
                'menu_name' => 'Sales'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'               => array( 'slug' => 'sales', 'with_front' => false ),
            'has_archive'           => true,
            'hierarchical'          => true,
            'publicly_queryable'    => true,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => 'dashicons-megaphone',
        ));

    register_post_type( 'webinars',
        array(
            'labels' => array(
                'name' => __('[:ua]Вебінари[:ru]Вебинары'),
                'singular_name' => __('[:ua]Вебінари[:ru]Вебинары'),
                'menu_name' => 'Webinars'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'               => array( 'slug' => 'webinars', 'with_front' => false ),
            'has_archive'           => true,
            'hierarchical'          => true,
            'publicly_queryable'    => true,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => 'dashicons-format-video',
        ));

    register_post_type( 'nanotest',
        array(
            'labels' => array(
                'name' => __('nanoTest'),
                'singular_name' => __('nanoTest'),
                'menu_name' => 'nanoTest'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'hierarchical'          => true,
            'rewrite'=> [
                'slug' => 'nanotest',
                "with_front" => false
            ],
            'publicly_queryable'    => true,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => '',
        ));

    //Regular Tests CPT and Taxonomy
    $taxname = 'Test Type';
    $taxlabels = array(
        'name'                          => $taxname,
        'singular_name'                 => $taxname,
        'search_items'                  => 'Search '.$taxname,
        'popular_items'                 => 'Popular '.$taxname,
        'all_items'                     => 'All '.$taxname.'s',
        'parent_item'                   => 'Parent '.$taxname,
        'edit_item'                     => 'Edit '.$taxname,
        'update_item'                   => 'Update '.$taxname,
        'add_new_item'                  => 'Add New '.$taxname,
        'new_item_name'                 => 'New '.$taxname,
        'separate_items_with_commas'    => 'Separate '.$taxname.'s with commas',
        'add_or_remove_items'           => 'Add or remove '.$taxname.'s',
        'choose_from_most_used'         => 'Choose from most used '.$taxname.'s'
    );
    $taxarr = array(
        'label'                         => $taxname,
        'labels'                        => $taxlabels,
        'public'                        => true,
        'hierarchical'                  => true,
        'show_in_nav_menus'             => true,
        'args'                          => array( 'orderby' => 'term_order' ),
        'query_var'                     => true,
        'show_ui'                       => true,
        'show_admin_column'             => true,
        'rewrite'                       => array( 'slug' => 'tests', 'with_front' => false )
    );
    register_taxonomy( 'type_test', 'regular_tests', $taxarr );

    register_post_type( 'regular_tests',
        array(
            'labels' => array(
                'name' => __('Тести'),
                'singular_name' => __('Тести'),
                'menu_name' => 'Regular Tests'
            ),
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'supports'              => array( 'title', 'editor', 'thumbnail' ),
            'rewrite'               => array( 'slug' => 'tests/%type_test%', 'with_front' => false ),
            'has_archive'           => 'tests',
            'hierarchical'          => true,
            'show_in_nav_menus'     => true,
            'capability_type'       => 'page',
            'query_var'             => true,
            'menu_icon'             => '',
        ));

    if( defined('WP_DEBUG') && true !== WP_DEBUG) {
        flush_rewrite_rules();
    }
}
