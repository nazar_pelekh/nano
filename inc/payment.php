<?php

add_action(
    'wp',
    function () {
        if ( is_page( 'prices' ) ) {
            if ( isset( $_POST['data'] ) ) {

                $result = json_decode( base64_decode( $_POST['data'] ) );
//                var_dump($result);die();

                if ( 'success' === $result->status ) {

                    $user = wp_get_current_user();
                    $amount = $result->amount;
                    $currency = $result->currency;
                    update_field('balance', $amount, 'user_'.$user->ID );
                    update_field('currency', $currency, 'user_'.$user->ID );
                    $result = registerUserInMoyKlass($user->ID);
                    $name = $result['name'];
                    echo "<input type='hidden' data-successLiq='true' id='successLiq' data-name='".$name."'>";
//echo "<pre>";
//                    var_dump(get_field('balance', 'user_'.$user->ID));
//                    var_dump( $result->amount );
//                    var_dump( $result );die();
//                    wp_mail( 'mail@gmail.com', 'from checkout', 'Какое-то сообщение checkout . body = ' . $body );
                }
            }
        }
    }
);


if ( ! function_exists( 'is_ajax' ) ) {
    /**
     * is_ajax - Returns true when the page is loaded via ajax.
     * @return bool
     */
    function is_ajax() {
        return defined( 'DOING_AJAX' );
    }
}

add_action( 'wp_ajax_getSignature', 'getSignature' );
add_action( 'wp_ajax_nopriv_getSignature', 'getSignature' );
function getSignature( $courseID = "", $pay_type = "", $currency = "UAH", $promocode = false) {

    extract( $_POST );

    $coursePrice = get_field("price_for_1_course", $courseID);
    $coursesCount = get_field("courses_count", $courseID);
    $finalPrice = $coursePrice*$coursesCount;


    $exCur = file_get_contents("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
    $exCur = json_decode($exCur, true);
    $exUSD = 1;
    $exEUR = 1;
    foreach ($exCur as $cur)
    {
        if($cur['ccy'] == "USD")
            $exUSD = $cur['sale'];
        if($cur['ccy'] == "EUR")
            $exEUR = $cur['sale'];
    }
    switch($currency)
    {
        case "USD": $finalPrice = $finalPrice / $exUSD; break;
        case "EUR": $finalPrice = $finalPrice / $exEUR; break;
    }

    if(empty($pay_type) || !isset($pay_type)) {
        $pay_type = 'card';
    }
    if(empty($finalPrice) || !isset($finalPrice)) {
        return json_encode(array('error' => 'Wrong price!'));
    }
    if(empty($currency) || !isset($currency)) {
        $pay_type = 'UAH';
    }

    if($promocode)
    {
        $promoCheck = check_promo($promocode, true);
        if($promoCheck['success'])
        {
            $discount = $finalPrice/100*$promoCheck['discount'];
            $finalPrice = $finalPrice - $discount;
        }
    }


    $json_string = array(
        "public_key" => "i63207714982",
        "version" => "3",
        "action" => "pay",
        "amount" => $finalPrice,
        "currency" => $currency,
        "paytypes" =>  $pay_type,
        "description" => "test",
        "order_id" => rand(),
        "server_url" => get_permalink(),
        "result_url" => get_permalink(),
    );
//    var_dump($json_string);die();

    $jsondata = $json_string;

    $privateKey = "zaTe58qJV0LnSHonBFRHfrg29k2eHkELQiSIiQ5K";
    $jsonResult = array();
    $data = base64_encode(json_encode($jsondata));
    $signature = $privateKey.$data.$privateKey;
    $signature = base64_encode( sha1( $signature ,1));
    $jsonResult['data']  = $data;
    $jsonResult['signature'] = $signature;

    if ( is_ajax() ) {
        die( json_encode($jsonResult) );
    }


}

add_action('wp_ajax_check_promo', 'check_promo');
add_action('wp_ajax_nopriv_check_promo', 'check_promo');
function check_promo($promocode = false, $faked = false){
    extract($_POST);

    $result = array();
//    var_dump($promocode);
    $posts = new WP_Query(array(
        'post_type' => 'promocodes',
        'post_status'   => 'publish',
        'posts_per_page' => -1,
        'meta_query'    => array(
            'relation' => 'AND',
            array(
                'key' => 'promo_text',
//                            'compare_key'   => 'LIKE',
                'value' => $promocode,
                'compare'   => '=',
            )
        )
    ));
    $promoPost = null;
    $promoPostField = null;
    foreach($posts->posts as $item):
        $promoPostField = get_field("promo_text", $item->ID);
//        var_dump($promoPostField);
//        var_dump($promoPostField);
        if($promoPostField == $promocode)
        {
            $promoPost = $item;
        }
    endforeach;
//    var_dump($posts->posts);

    if($promoPostField == $promocode)
    {
        $discount = get_field('discount_percentage', $promoPost->ID);
        if($discount)
        {
            $result['success'] = true;
            $result['discount'] = $discount;
            $result['msg'] = __('[:ua]Промокод активовано[:ru]Промокод активирован');
        }
        else
        {
            $result['error'] = true;
            $result['msg'] = __('[:ua]Невірний промокод[:ru]Неверный промокод');
        }
    }
    else
    {
        $result['error'] = true;
        $result['msg'] = __('[:ua]Невірний промокод[:ru]Неверный промокод');
    }


    if ( is_ajax() && !$faked) {
        echo( json_encode( $result ) );
        exit();
    }
    return $result;
}


if ( ! function_exists( 'is_ajax' ) ) {
    /**
     * is_ajax - Returns true when the page is loaded via ajax.
     * @return bool
     */
    function is_ajax() {
        return defined( 'DOING_AJAX' );
    }
}
?>