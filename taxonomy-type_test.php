<?php get_header();
$term = get_queried_object();

?>

<div id="breadcrumbs" class="regular-test__breadcrumbs">
    <div class="container">
        <span>
            <span>
                <a href="https://nanoenglish.com/"><?php echo __('[:ua]Головна[:ru]Главная') ?></a> /
                <a href="https://nanoenglish.com/tests"><?php echo __('[:ua]Тести[:ru]Тесты') ?></a> /
                <span class="breadcrumb_last" aria-current="page"><?php echo $term->name ?></span>
            </span>
        </span>
    </div>
</div>

<section class="content container hide_mob">

    <?php if($title = get_field('title_category', $term)) {
        echo '<div class="regular-test__title">'.$title.'</div>';
    } ?>

    <?php
    $args = array(
        'type'          => 'post',
        'orderby'       => 'term_group',
        'hide_empty'    => 0,
        'hierarchical'  => 0,
        'parent'        => 0,
        'taxonomy'      => 'type_test'
    );
    if($top_l = get_categories( $args )) { ?>
        <div class="regular-test__tabber">
            <div class="regular-test__top-categories">
                <?php foreach ($top_l as $top) { ?>
                    <a href="<?php echo get_category_link($top->term_id) ?>" class="regular-test__top-category <?php echo $term->term_id == $top->term_id ? 'is_active' : ''; ?>">
                        <?php echo $top->name ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <?php
    $parent_cat_ID = $term->term_id;
    $args = array(
        'hierarchical' => 1,
        'show_option_none' => '',
        'hide_empty' => 0,
        'parent' => $parent_cat_ID,
        'taxonomy' => 'type_test'
    );
    $subcats = get_categories($args); ?>
    <div class="regular-test__tests">
        <div class="regular-test__tests-cat">
            <?php foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                echo '<a href="javascript:;" data-cat="'.$sc->slug.'">'.$sc->name.'</a>';
            } ?>
        </div>
        <div class="regular-test__tests-res">
            <div class="regular-test__tab tests--default">
                <img src="<?php echo get_template_directory_uri() ?>/images/user_interface_2.svg" alt="">
                <?php the_field('default_text', $term) ?>
            </div>
            <?php foreach ($subcats as $sc) { ?>
                <div class="regular-test__tab tests--posts" data-term="<?php echo $sc->slug ?>">
                    <?php
                    $arg = array(
                        'post_type' => 'regular_tests',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'type_test',
                                'field' => 'term_id',
                                'terms' => $sc->term_id
                            )
                        )
                    );
                    $tests = new WP_Query( $arg );
                    if ( $tests->have_posts() ) { ?>
                        <?php foreach ($tests->posts as $post) {
                            echo '<a href="/tests/'.$term->slug./*'/'.$sc->slug.*/'/'.$post->post_name.'"><span>'.$post->post_title.'</span></a>';
                        } ?>
                    <?php }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>


    <?php if($desc = get_field('description',$term)) { ?>
        <div class="regular-test__description">
            <?php echo $desc; ?>
        </div>
    <?php } ?>

</section>

<section class="content container hide_desk">

    <div class="regular-test__filtering-mob">
        <?php
        $args = array(
            'type'          => 'post',
            'orderby'       => 'term_group',
            'hide_empty'    => 0,
            'hierarchical'  => 0,
            'parent'        => 0,
            'taxonomy'      => 'type_test'
        );
        if($top_l = get_categories( $args )) { ?>
            <p><?php echo __('[:ua]Оберіть розділ[:ru]Выберите раздел') ?></p>
            <select name="" id="test__title">
                <?php foreach ($top_l as $top) { ?>
                    <option data-title="<?php echo $top->slug ?>" data-select="<?php echo $top->name ?>" data-href="<?php echo get_category_link($top->term_id) ?>" class="<?php echo $term->term_id == $top->term_id ? 'is_active' : ''; ?>" value="<?php echo $top->slug ?>">
                        <?php echo $top->name ?>
                    </option>
                <?php } ?>
            </select>
        <?php } ?>

        <?php
        $parent_cat_ID = $term->term_id;
        $args = array(
            'hierarchical' => 1,
            'show_option_none' => '',
            'hide_empty' => 0,
            'parent' => $parent_cat_ID,
            'taxonomy' => 'type_test'
        );
        $subcats = get_categories($args); ?>
        <p><?php echo __('[:ua]Оберіть цікаву тему[:ru]Выберите интересную тему') ?></p>
        <select name="" class="test__category regular-test__filtering-select">
            <option value=""><?php echo __('[:ua]Оберіть тему[:ru]Выберите тему') ?></option>
            <?php $count = 0; foreach ($subcats as $sc) {
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                echo '<option data-title="'.$term->name.'" data-cat="'.$sc->slug.'" value="'.$count++.'">'.$sc->name.'</option>';
            } ?>
        </select>

        <div class="regular-test__mob-posts is_disabled">
            <p><?php echo __('[:ua]Оберіть тип завдання[:ru]Выберите тип задания') ?></p>

            <?php foreach ($subcats as $sc) {
            $arg = array(
                'post_type' => 'regular_tests',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'type_test',
                        'field' => 'term_id',
                        'terms' => $sc->term_id
                    )
                )
            );
            $tests = new WP_Query( $arg );
            if ( $tests->have_posts() ) { ?>
                <div class="regular-test__mob-post hide">
                    <select name="" class="test__post regular-test__filtering-select">
                        <option value=""><?php echo __('[:ua]Оберіть тип[:ru]Выберите тип') ?></option>
                        <?php foreach ($tests->posts as $post) {
                            echo '<option value="/tests/'.$term->slug./*'/'.$sc->slug.*/'/'.$post->post_name.'" >'.$post->post_title.'</option>';
                        } ?>
                    </select>
                </div>
            <?php }
            // Restore original Post Data
            wp_reset_postdata();
        } ?>
        </div>


        <?php

        ?>
        <button class="button regular-test__filtering-submit is_disabled"><?php echo __('[:ua]Почати Тестування[:ru]Начать Тестирование') ?></button>
    </div>


    <?php if($desc = get_field('description',$term)) { ?>
        <div class="regular-test__description">
            <?php echo $desc; ?>
        </div>
    <?php } ?>

</section>

<?php get_footer(); ?>
