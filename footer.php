</div>
<footer>
    <div class="footer_top">
        <div class="container flex__mob">
            <div class="footer_top__logo">
                <a href="/" id="footer_logo">
                    <img src="<?php the_field('logo','option') ?>" alt="">
                </a>
            </div>
            <div class="footer_top__contact">
                <a href="tel:+<?php echo preg_replace('/[^0-9]/', '', get_field("phone","option")); ?>" id="phone_footer"><i class="fas fa-phone-alt"></i><?php the_field("phone","option"); ?></a>
                <a href="mailto:<?php the_field('email','option') ?>"><i class="fas fa-envelope"></i><?php the_field('email','option') ?></a>
            </div>
            <?php if($socials = get_field('links','option')){ ?>
                <div class="footer_top_socials">
                    <?php foreach($socials as $s) { ?>
                        <a href="<?php echo $s['link'] ?>" target="_blank"><?php echo $s['icon'] ?></a>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container flex_start__rwd">
            <div class="footer_menu flex_start">
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location'  => 'footer_menu_1')); ?>
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location'  => 'footer_menu_2')); ?>
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location'  => 'footer_menu_3')); ?>
                <?php wp_nav_menu(array('container' => false, 'items_wrap' => '<ul id="%1$s">%3$s</ul>', 'theme_location'  => 'footer_menu_4')); ?>
            </div>
            <div class="footer_copyright">
                <p>
                    <span>© Copyright <?php bloginfo(); ?></span>
                    <span><?php echo __('[:ua]Усі права захищено '.date('Y').'[:ru]Все права защищены '.date('Y')) ?></span>
                </p>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

 <script type="text/javascript">
             jQuery('input[type="submit"]').click(function() {
                    if((jQuery(this).val()==="Підписатися на розсилку"))  { 
                        jQuery("#thanks_mail h4").text("Дякуємо за ваш вибір!"); 
                        jQuery("#thanks_mail p").text("З нами ви підкорите англійську."); 
                    }
                    if((jQuery(this).val()==="Підписатися"))  { 
                        jQuery("#thanks_mail h4").text("Дякуємо за ваш вибір!"); 
                        jQuery("#thanks_mail p").text("З нами ви підкорите англійську."); 
                    }
                    if((jQuery(this).val()==="Записатися"))  { 
                        jQuery("#thanks_mail h4").text("Вітаємо! Ви зробили перший крок."); 
                        jQuery("#thanks_mail p").text("Ми зв'яжемось з вами протягом 10 хв."); 
                    }
                     if((jQuery(this).val()==="Записатись на пробне заняття"))  { 
                        jQuery("#thanks_mail h4").text("Вітаємо! Ви зробили перший крок."); 
                        jQuery("#thanks_mail p").text("Ми зв'яжемось з вами протягом 10 хв."); 
                    }
                    if((jQuery(this).val()==="Написати нам"))  { 
                        jQuery("#thanks_mail h4").text("Дякуємо за звернення!"); 
                        jQuery("#thanks_mail p").text("Інформація надіслана на вказаний вами e-mail."); 
                    }
                  
                });
         </script>
         
         <?php if( is_page(397) ){?>
        	 <script type="text/javascript">
                     jQuery('input[type="submit"]').click(function() {
                           if((jQuery(this).val()==="Написати нам"))  { 
                                jQuery("#thanks_mail h4").text("Дякуємо за звернення в nanoEnglish!"); 
                                jQuery("#thanks_mail p").text("Ми зв'яжемось з вами протягом 10 хв."); 
                            }
                          
                        });
                 </script>
        <?php }?>
</body>
</html>
<?php if( @!WP_DEBUG) {	ob_end_flush(); } ?>
